package com.omninos.saowaridriver.Activities;

import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.RescheduleAppointmentPojo;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.RescheduleAppointmentViewModel;

public class AppointmentConfirmationActivity extends AppCompatActivity implements View.OnClickListener {

    private Button cancelButton, reshudle;
    private ImageView backButton;
    private TextView address, appointmentDate, appointmentTime, addressData;
    private RescheduleAppointmentViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_confirmation);
        App.getAppPreference().SaveString(ConstantData.TOKEN, "1");
        findIds();


        viewModel = ViewModelProviders.of(this).get(RescheduleAppointmentViewModel.class);

        stopService(new Intent(AppointmentConfirmationActivity.this, MySerives.class));
        Intent intent = new Intent(AppointmentConfirmationActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void findIds() {
        cancelButton = findViewById(R.id.cancel_btn);
        backButton = findViewById(R.id.back_btn);

        //click listeners
        cancelButton.setOnClickListener(this);
        backButton.setOnClickListener(this);


        address = findViewById(R.id.address);
        appointmentDate = findViewById(R.id.appointmentDate);
        appointmentTime = findViewById(R.id.appointmentTime);
        addressData = findViewById(R.id.addressData);
        reshudle = findViewById(R.id.reshudle);

        reshudle.setOnClickListener(this);

        appointmentDate.setText(getIntent().getStringExtra("date"));
        appointmentTime.setText(getIntent().getStringExtra("time"));
//        address.setText(getIntent().getStringExtra("address"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.cancel_btn:
                App.getAppPreference().Logout(AppointmentConfirmationActivity.this);
                Intent intent = new Intent(AppointmentConfirmationActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;

            case R.id.reshudle:
                Reschedule();
                break;
        }
    }

    private void Reschedule() {
        viewModel.rescheduleAppointment(AppointmentConfirmationActivity.this, App.getAppPreference().GetString(ConstantData.USERID)).observe(AppointmentConfirmationActivity.this, new Observer<RescheduleAppointmentPojo>() {
            @Override
            public void onChanged(@Nullable RescheduleAppointmentPojo rescheduleAppointmentPojo) {
                if (rescheduleAppointmentPojo.getSuccess().equalsIgnoreCase("1")) {
                    CommonUtils.showSnackbarAlert(reshudle, "Appointment rescheduled successfully please wait until admin assign new date");
                    finish();
                } else {
                    CommonUtils.showSnackbarAlert(reshudle, rescheduleAppointmentPojo.getMessage());
                }
            }
        });
    }
}
