package com.omninos.saowaridriver.Activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.adapters.VehicleTypeAdapter;
import com.omninos.saowaridriver.model.VehicleTypePojo;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.VehicleBrandsAndModelsVM;

public class SelectVehicleActivity extends AppCompatActivity implements View.OnClickListener {

    private Activity activity;

    private String vehicleType = "";

    private RecyclerView vehicleTypeRC;

    private VehicleBrandsAndModelsVM vehicleTypeVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_vehicle);

        stopService(new Intent(SelectVehicleActivity.this, MySerives.class));
        Intent intent = new Intent(SelectVehicleActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        initViews();

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void initViews() {

        activity = SelectVehicleActivity.this;

        vehicleTypeVM = ViewModelProviders.of(this).get(VehicleBrandsAndModelsVM.class);

//        TextView appbarText = findViewById(R.id.app_bar);
//        appbarText.setText(R.string.select_vehicle_type);

//        ImageView appbarBack = findViewById(R.id.back_btn);
//        appbarBack.setOnClickListener(this);

        vehicleTypeRC = findViewById(R.id.vehicleTypeRC);
        vehicleTypeRC.setLayoutManager(new LinearLayoutManager(activity));

        Button continueBtnSelectVehicle = findViewById(R.id.continueBtnSelectVehicle);
        continueBtnSelectVehicle.setOnClickListener(this);

        getVehicleTypesApi();

    }

    private void getVehicleTypesApi() {
        vehicleTypeVM.vehicleTypes(activity).observe(this, new Observer<VehicleTypePojo>() {
            @Override
            public void onChanged(@Nullable VehicleTypePojo vehicleTypePojo) {
                if (vehicleTypePojo.getSuccess().equalsIgnoreCase("1")) {

                    vehicleTypeRC.setAdapter(new VehicleTypeAdapter(activity, vehicleTypePojo.getDetails(),
                            new VehicleTypeAdapter.VehicleTypeCallback() {
                                @Override
                                public void selectedVehicleType(String id) {
                                    vehicleType = id;
                                }
                            }));

                } else {
                    Toast.makeText(activity, "Vehicle list not found", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.continueBtnSelectVehicle:
                if (vehicleType.isEmpty()) {
                    Toast.makeText(activity, "Please select your vehicle type", Toast.LENGTH_SHORT).show();
                } else {
                    App.getAppPreference().SaveString(ConstantData.VEHICLE_TYPE, vehicleType);
                    Intent i = new Intent(activity, AddVehicleDetailsActivity.class);
                    startActivity(i);
                }
                break;

        }
    }
}
