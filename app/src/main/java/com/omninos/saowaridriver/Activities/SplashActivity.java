package com.omninos.saowaridriver.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

import com.omninos.saowaridriver.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.omninos.saowaridriver.model.CheckDriverJobPojo;
import com.omninos.saowaridriver.model.JobFlowPojo;
import com.omninos.saowaridriver.services.LocationService;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.CheckDriverJobViewModel;
import com.omninos.saowaridriver.viewmodels.JobFlowViewModel;

public class SplashActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private Activity activity;

    private GoogleApiClient googleApiClient;
    private CheckDriverJobViewModel checkDriverJobViewModel;
    private JobFlowViewModel jobFlowViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        activity = SplashActivity.this;

        stopService(new Intent(SplashActivity.this, MySerives.class));
        Intent intent = new Intent(SplashActivity.this, MySerives.class);
        if (!isMyServiceRunning1(intent.getClass())) {
            startService(intent);
        }

        jobFlowViewModel = ViewModelProviders.of(this).get(JobFlowViewModel.class);
        checkDriverJobViewModel = ViewModelProviders.of(this).get(CheckDriverJobViewModel.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
        turnGPSOn();
    }

    private boolean isMyServiceRunning1(Class<? extends Intent> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void turnGPSOn() {


        if (googleApiClient == null) {

            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this).addOnConnectionFailedListener(SplashActivity.this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.
                            permissions();
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(SplashActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            Toast.makeText(SplashActivity.this, "off", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            });
        }
    }

    private void permissions() {
        if (ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
            return;
        } else {
            gotoScreen();
        }
    }

    private void gotoScreen() {
        believe.cht.fadeintextview.TextView textView = findViewById(R.id.textView);


        String text = "<font color=#000000>Way</font><font color=#cc0029> to </font> <font color=#000000>gether</font>";
        textView.setText(Html.fromHtml(text));
        Thread thread = new Thread() {
            @Override
            public void run() {
                super.run();

                try {
                    sleep(3500);
                    Intent locationservice = new Intent(SplashActivity.this, LocationService.class);
                    if (!isMyServiceRunning(locationservice.getClass())) {
                        startService(locationservice);
                    }

                    if (App.getAppPreference().GetString(ConstantData.TOKEN).equalsIgnoreCase("1")) {
                        if (App.getAppPreference().GetString(ConstantData.JOB_ID).isEmpty()) {
                            startActivity(new Intent(activity, HomeActivity.class));
                            finishAffinity();
                        } else {
                            checkDriverJob();
                        }

                    } else {
                        Intent intent = new Intent(SplashActivity.this, SelectLanguageActivity.class);
                        intent.putExtra("Type", "0");
                        startActivity(intent);
                        finishAffinity();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        };

        thread.start();
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                permissions();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Toast.makeText(this, "You have to turn on the Gps. Please restart Aplication", Toast.LENGTH_LONG).show();
            }

            if (requestCode == 1) {
                int camera = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
                int storage = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
                int Writestorage = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                int Vibrate = ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
                int location1 = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
                int location2 = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);


                if (camera != PackageManager.PERMISSION_GRANTED ||
                        storage != PackageManager.PERMISSION_GRANTED ||
                        Writestorage != PackageManager.PERMISSION_GRANTED ||
                        Vibrate != PackageManager.PERMISSION_GRANTED ||
                        location1 != PackageManager.PERMISSION_GRANTED ||
                        location2 != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Mandatory permissions are not allowed", Toast.LENGTH_SHORT).show();
                    rationale();
                } else {
                    SplashActivity.this.finish();
                }

            }

        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1001: {
                int count = 0;
                if (grantResults.length > 0)
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED)
                            count++;
                    }
//
                if (count == grantResults.length) {
                    permissions();
                } else if ((Build.VERSION.SDK_INT > 23 && !shouldShowRequestPermissionRationale(permissions[0])
                        && !shouldShowRequestPermissionRationale(permissions[1])
                        && !shouldShowRequestPermissionRationale(permissions[2])
                        && !shouldShowRequestPermissionRationale(permissions[3])
                        && !shouldShowRequestPermissionRationale(permissions[4])
                        && !shouldShowRequestPermissionRationale(permissions[5]))) {
                    rationale();
                } else {
                    Toast.makeText(this, "Permission Not granted", Toast.LENGTH_SHORT).show();
                    permissions();
                }
            }
//
        }
    }

    void rationale() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Mandatory Permissions")
                .setMessage("Manually allow permissions in App settings")
                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 1);

//                        SplashScreen.this.finish();
                    }
                })
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void OpenSetting() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permission");
        builder.setMessage("Permissions are required");
        builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(SplashActivity.this, "Go to Settings to Grant the Storage Permissions and restart application", Toast.LENGTH_LONG).show();
//                sentToSettings = true;
                Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", SplashActivity.this.getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        })
                .create()
                .show();
    }


    private void checkDriverJob() {

        String jobId = App.getAppPreference().GetString(ConstantData.USERID);

        checkDriverJobViewModel.çheckDriverJob(activity, jobId).observe(this, new Observer<CheckDriverJobPojo>() {
            @Override
            public void onChanged(@Nullable CheckDriverJobPojo checkDriverJobPojo) {
                if (checkDriverJobPojo.getSuccess().equalsIgnoreCase("1")) {
                    String jobStatus = checkDriverJobPojo.getDetails().getJobStatus();

                    App.getAppPreference().SaveString(ConstantData.JOB_CUSTOMER_NAME, checkDriverJobPojo.getDetails().getUserName());
                    App.getAppPreference().SaveString(ConstantData.JOB_CUSTOMER_IMAGE, checkDriverJobPojo.getDetails().getUserImage());
                    App.getAppPreference().SaveString(ConstantData.JOB_CUSTOMER_MOBILE, checkDriverJobPojo.getDetails().getUserPhoneNumber());
                    App.getAppPreference().SaveString(ConstantData.JOB_CUSTOMER_RATING, checkDriverJobPojo.getDetails().getUserRating());

                    App.getAppPreference().SaveString(ConstantData.JOB_STATUS, checkDriverJobPojo.getDetails().getJobStatus());

                    App.getAppPreference().SaveString(ConstantData.JOB_ID, checkDriverJobPojo.getDetails().getJobId());
                    App.getAppPreference().SaveString(ConstantData.JOB_TOTAL_DISTANCE, checkDriverJobPojo.getDetails().getDistance());
                    App.getAppPreference().SaveString(ConstantData.JOB_TOTAL_AMOUNT, checkDriverJobPojo.getDetails().getPayment());
                    App.getAppPreference().SaveString(ConstantData.PAYMENT_TYPE, checkDriverJobPojo.getDetails().getPaymentType());

                    App.getAppPreference().SaveString(ConstantData.JOB_PICK_ADDRESS, checkDriverJobPojo.getDetails().getPicAddress());
                    App.getAppPreference().SaveString(ConstantData.JOB_PICK_LAT, checkDriverJobPojo.getDetails().getPicLat());
                    App.getAppPreference().SaveString(ConstantData.JOB_PICK_LONG, checkDriverJobPojo.getDetails().getPicLong());

                    App.getAppPreference().SaveString(ConstantData.JOB_DROP_ADDRESS, checkDriverJobPojo.getDetails().getDropAddress());
                    App.getAppPreference().SaveString(ConstantData.JOB_DROP_LAT, checkDriverJobPojo.getDetails().getDropLat());
                    App.getAppPreference().SaveString(ConstantData.JOB_DROP_LONG, checkDriverJobPojo.getDetails().getDropLong());

                    if (jobStatus.equalsIgnoreCase("1")) {
                        startActivity(new Intent(activity, JobArrivingActivity.class));
                        finish();
                    } else if (jobStatus.equalsIgnoreCase("2")) {
                        startActivity(new Intent(activity, HomeActivity.class));
                        finish();
                    } else if (jobStatus.equalsIgnoreCase("3")) {
                        startActivity(new Intent(activity, JobStartActivity.class));
                        finish();
                    } else if (jobStatus.equalsIgnoreCase("4")) {
                        startActivity(new Intent(activity, JobDropActivity.class));
                        finish();
                    } else if (jobStatus.equalsIgnoreCase("5")) {
                        startActivity(new Intent(activity, JobRatingActivity.class));
                        finish();
                    } else if (jobStatus.equalsIgnoreCase("0")) {
                        rejectApi(checkDriverJobPojo.getDetails().getJobId());
                    } else {
                        startActivity(new Intent(activity, HomeActivity.class));
                        finish();
                    }
                } else {
                    startActivity(new Intent(activity, HomeActivity.class));
                    finish();
                }
            }
        });

    }

    private void rejectApi(String jobId) {
//        String jobId = App.getAppPreference().getString(AppConstants.JOB_ID);

        jobFlowViewModel.acceptRejectJob(activity, "2", jobId).observe(this,
                new Observer<JobFlowPojo>() {
                    @Override
                    public void onChanged(@Nullable JobFlowPojo jobFlowPojo) {
                        if (jobFlowPojo.getSuccess().equalsIgnoreCase("1")) {
                            startActivity(new Intent(activity, HomeActivity.class));
                            finish();
                        } else {
                            Toast.makeText(activity, jobFlowPojo.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

}
