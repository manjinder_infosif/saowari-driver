package com.omninos.saowaridriver.Activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.HelpAndContactusModel;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.HelpAndContactUsViewModel;

public class HelpActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView backButton;
    private Activity activity;
    private Button sendQueryButton;

    private EditText name, mobileNumber, email, message;
    private String nameString, numberString, emailString, messageString;
    private HelpAndContactUsViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        activity = HelpActivity.this;

        viewModel = ViewModelProviders.of(this).get(HelpAndContactUsViewModel.class);


        stopService(new Intent(HelpActivity.this, MySerives.class));
        Intent intent = new Intent(HelpActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        findIds();
        setUps();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void findIds() {
        backButton = findViewById(R.id.back_btn);
        sendQueryButton = findViewById(R.id.send_query_btn);

        name = findViewById(R.id.name);
        mobileNumber = findViewById(R.id.mobileNumber);
        email = findViewById(R.id.email);
        message = findViewById(R.id.message);

    }

    private void setUps() {
        sendQueryButton.setOnClickListener(this);
        backButton.setOnClickListener(this);

        name.setText(App.getAppPreference().getLoginDetail().getDetails().getDriverName());
        mobileNumber.setText(App.getAppPreference().getLoginDetail().getDetails().getDriverPhoneNumber());
        email.setText(App.getAppPreference().getLoginDetail().getDetails().getDriverEmail());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;

            case R.id.send_query_btn:
                SendContactRequest();
                break;
        }
    }

    private void SendContactRequest() {
        nameString = name.getText().toString();
        emailString = email.getText().toString();
        numberString = mobileNumber.getText().toString();
        messageString = message.getText().toString();

        if (nameString.isEmpty()) {
            CommonUtils.showSnackbarAlert(name, "enter name");
        } else if (emailString.isEmpty()) {
            CommonUtils.showSnackbarAlert(email, "enter email");
        } else if (numberString.isEmpty()) {
            CommonUtils.showSnackbarAlert(mobileNumber, "enter mobile number");
        } else if (messageString.isEmpty()) {
            CommonUtils.showSnackbarAlert(message, "enter query");
        } else {
            viewModel.helpAndContactusModelLiveData(HelpActivity.this, "help", nameString, numberString, emailString, messageString, App.getAppPreference().GetString(ConstantData.USERID)).observe(HelpActivity.this, new Observer<HelpAndContactusModel>() {
                @Override
                public void onChanged(@Nullable HelpAndContactusModel helpAndContactusModel) {
                    if (helpAndContactusModel.getSuccess().equalsIgnoreCase("1")) {
                        CommonUtils.showSnackbarAlert(message, helpAndContactusModel.getMessage());
                        onBackPressed();
                    } else {
                        CommonUtils.showSnackbarAlert(message, helpAndContactusModel.getMessage());
                    }
                }
            });
        }
    }
}
