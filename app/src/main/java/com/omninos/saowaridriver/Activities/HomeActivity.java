package com.omninos.saowaridriver.Activities;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.omninos.saowaridriver.MainActivity;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.CheckDriverJobPojo;
import com.omninos.saowaridriver.model.VersionModel;
import com.omninos.saowaridriver.services.LocationService;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.infideap.drawerbehavior.AdvanceDrawerLayout;
import com.omninos.saowaridriver.viewmodels.CheckDriverJobViewModel;
import com.omninos.saowaridriver.viewmodels.DriverRegisterVM;
import com.omninos.saowaridriver.viewmodels.DriverStatusVM;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback, CompoundButton.OnCheckedChangeListener {

    private AdvanceDrawerLayout advanceDrawerLayout;
    private ImageView menuIcon, profileEdit, editProfileBtnHome;
    private NavigationView navigationView;
    private Activity activity;
    private GoogleMap gMap;
    private Switch aSwitch;
    private TextView tvUserName, tvCarName, tvprofileName, tv_number;
    private DriverStatusVM viewModel;
    private CircleImageView homeImage, navImage;

    LinearLayout navMyRide, navMyBooking, navMyWallet, navChatsAndCalls, navPackages, navSettings, navInviteFriends, navLogout;
    ImageView editProfileButton;
    private DriverRegisterVM viewModel1;
    private RatingBar ratingHome;


    private Timer timer;
    private TimerTask timerTask;
    private Marker driverMarker = null;


    //To calculate distance of to gmap points
    double _radiusEarthMiles = 3959;
    double _radiusEarthKM = 6371;
    double _m2km = 1.60934;
    double _toRad = Math.PI / 180;
    private double _centralAngle;

    private double Lat = 0.0, Lan = 0.0;
    private double Lat200 = 0.0, Lan200 = 0.0;

    private int version;
    private CheckDriverJobViewModel checkDriverJobViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        activity = HomeActivity.this;

        stopService(new Intent(HomeActivity.this, MySerives.class));
        Intent intent = new Intent(HomeActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }
        startLocationService();

        checkDriverJobViewModel = ViewModelProviders.of(this).get(CheckDriverJobViewModel.class);


        checkJobStatus();
        viewModel1 = ViewModelProviders.of(this).get(DriverRegisterVM.class);
        viewModel = ViewModelProviders.of(this).get(DriverStatusVM.class);

        setMap();
        findIds();
        setUps();
        navigationMenu();

    }

    private void checkJobStatus() {

        String jobId = App.getAppPreference().GetString(ConstantData.USERID);

        checkDriverJobViewModel.çheckDriverJob(activity, jobId).observe(this, new Observer<CheckDriverJobPojo>() {
            @Override
            public void onChanged(@Nullable CheckDriverJobPojo checkDriverJobPojo) {

            }
        });
    }


    private void startLocationService() {
        if (!isMyServiceRunning1(LocationService.class)) {
            startService(new Intent(activity, LocationService.class));
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private boolean isMyServiceRunning1(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void findIds() {
        advanceDrawerLayout = findViewById(R.id.advanced_drawer);
        navigationView = findViewById(R.id.nav_view);
        menuIcon = findViewById(R.id.open_menu);
        editProfileBtnHome = findViewById(R.id.editProfileBtnHome);
        aSwitch = findViewById(R.id.switch_online);

        tvUserName = findViewById(R.id.tv_username);
        tvCarName = findViewById(R.id.tv_car);
        tvprofileName = findViewById(R.id.tv_user_name);
        homeImage = findViewById(R.id.homeImage);
        navImage = findViewById(R.id.navImage);

        tv_number = findViewById(R.id.tv_number);

        ratingHome = findViewById(R.id.ratingHome);
    }

    private void setUps() {

        //drawer layout
        advanceDrawerLayout.useCustomBehavior(Gravity.START);
        advanceDrawerLayout.setRadius(Gravity.START, 35);//set end container's corner radius (dimension)
        advanceDrawerLayout.setViewScale(Gravity.START, 0.9f);
        advanceDrawerLayout.setViewElevation(Gravity.START, 20);

        menuIcon.setOnClickListener(this);
        editProfileBtnHome.setOnClickListener(this);
        aSwitch.setOnCheckedChangeListener(this);

        //set Texts


    }

    private void navigationMenu() {


        navMyRide = findViewById(R.id.nav_my_ride);
        navMyBooking = findViewById(R.id.nav_my_booking);
        navMyWallet = findViewById(R.id.nav_my_wallet);
        navChatsAndCalls = findViewById(R.id.nav_chats_calls);
        navPackages = findViewById(R.id.nav_packages);
        navSettings = findViewById(R.id.nav_settings);
        navInviteFriends = findViewById(R.id.nav_invite_friends);
        navLogout = findViewById(R.id.nav_logout);
        editProfileButton = findViewById(R.id.edit_profle);


        navMyRide.setOnClickListener(this);
        navMyBooking.setOnClickListener(this);
        navMyWallet.setOnClickListener(this);
        navChatsAndCalls.setOnClickListener(this);
        navPackages.setOnClickListener(this);
        navSettings.setOnClickListener(this);
        navInviteFriends.setOnClickListener(this);
        navLogout.setOnClickListener(this);

        editProfileButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.open_menu:
                advanceDrawerLayout.openDrawer(Gravity.START);
                break;
            case R.id.edit_profle:
                startActivity(new Intent(activity, ProfileActivity.class));
                break;
            case R.id.editProfileBtnHome:
                startActivity(new Intent(activity, EditProfileActivity.class));
                break;
            case R.id.nav_my_ride:
                startActivity(new Intent(activity, MyRideActivity.class));
                break;
            case R.id.nav_my_booking:
                startActivity(new Intent(activity, MyBookingActivity.class));
                break;
            case R.id.nav_my_wallet:
                startActivity(new Intent(activity, MyWalletActivity.class));
                break;
            case R.id.nav_chats_calls:
                startActivity(new Intent(activity, ChatAndCallActivity.class));
                break;
            case R.id.nav_packages:
                Intent intent = new Intent(activity, SelectLanguageActivity.class);
                intent.putExtra("Type", "1");
                startActivity(intent);
                break;
            case R.id.nav_settings:
                startActivity(new Intent(activity, SettingsActivity.class));
                break;
            case R.id.nav_invite_friends:
                startActivity(new Intent(activity, InviteFriendsActivity.class));
                break;
            case R.id.nav_logout:
                viewModel1.LogOut(activity, App.getAppPreference().GetString(ConstantData.USERID)).observe(HomeActivity.this, new Observer<Map>() {
                    @Override
                    public void onChanged(@Nullable Map map) {
                        if (map.get("success").toString().equalsIgnoreCase("1")) {
                            stopService(new Intent(HomeActivity.this, MySerives.class));
                            App.getAppPreference().Logout(activity);
                            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                            finishAffinity();
                        } else {
                            CommonUtils.showSnackbarAlert(navLogout, "Try Again");
                        }
                    }
                });
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        gMap.setMyLocationEnabled(true);
        if (App.getSinltonPojo().getCurrentLatlng() != null) {
            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(App.getSinltonPojo().getCurrentLatlng().latitude, App.getSinltonPojo().getCurrentLatlng().longitude), 15));

//            if (timer == null) {
//                startTimer();
//            }

        } else {
            stopService(new Intent(HomeActivity.this, LocationService.class));
//            startService(new Intent(activity,MyLocationServices.class));
            Intent intent = new Intent(HomeActivity.this, LocationService.class);
            if (!isMyServiceRunning(intent.getClass())) {
                startService(intent);
//                gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(App.getSinltonPojo().getCurrentLatlng().latitude, App.getSinltonPojo().getCurrentLatlng().longitude), 12));
            }
        }

    }

    private void startTimer() {
        //set a new Timer
        timer = new Timer();
        //initialize the TimerTask's job
        initializeTimerTask();
        //schedule the timer, to wake up every 2 second
        timer.schedule(timerTask, 3500, 3500); //
    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                startLocationService();

                double lat = Double.parseDouble(App.getAppPreference().GetString(ConstantData.CURRENT_LAT));
                double lan = Double.parseDouble(App.getAppPreference().GetString(ConstantData.CURRENT_LONG));

                final LatLng latLngOld = new LatLng(Lat, Lan);
                final LatLng latLngNew = new LatLng(lat, lan);

                if (HasMoved(lat, lan)) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // Your code to run in GUI thread here
//                            animateMarker(latLngOld, latLngNew, false);
                            List<LatLng> latLngsList = new ArrayList<>();
                            latLngsList.add(latLngOld);
                            latLngsList.add(latLngNew);

                            animateCarOnMap(latLngsList);

                        }
                    });

                } else {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (driverMarker != null) {
                                driverMarker.remove();
                            }
                            MarkerOptions markerOptions = new MarkerOptions();
                            // Setting the position for the marker
                            markerOptions.position(latLngOld);
                            if (App.getAppPreference().getLoginDetail().getDetails().getDriverVehicleType().equalsIgnoreCase("1")) {
                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.car));
                            } else {
                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.bike));
                            }


                            // Animating to the touched position
                            gMap.animateCamera(CameraUpdateFactory.newLatLng(latLngOld));
                            // Placing a marker on the touched position
                            driverMarker = gMap.addMarker(markerOptions);
                            driverMarker.setRotation(getBearing(latLngOld, latLngNew));
                        }
                    });
                }
                if (!App.getAppPreference().GetString(ConstantData.JOB_ARRIVING_STATUS).equalsIgnoreCase("1")) {
                    if (!HasMoved200(lat, lan)) {
//                        aboutToArriveApi();
                    }
                }
            }
        };
    }


    public boolean HasMoved(double newLat, double newLan) {
        double significantDistance = 30;
        double currentDistance;

        currentDistance = DistanceMilesSEP(Lat200, Lan200, newLat, newLan);
        currentDistance = currentDistance * _m2km * 1000;

        if (currentDistance < significantDistance) {
            return false;
        } else {
            Lat200 = newLat;
            Lan200 = newLan;
            return true;
        }
    }

    public double DistanceMilesSEP(double Lat1, double Lon1, double Lat2, double Lon2) {
        try {
            double _radLat1 = Lat1 * _toRad;
            double _radLat2 = Lat2 * _toRad;
            double _dLat = (_radLat2 - _radLat1);
            double _dLon = (Lon2 - Lon1) * _toRad;

            double _a = (_dLon) * Math.cos((_radLat1 + _radLat2) / 2);

            // central angle, aka arc segment angular distance
            _centralAngle = Math.sqrt(_a * _a + _dLat * _dLat);

            // great-circle (orthodromic) distance on Earth between 2 points
        } catch (Exception e) {
            e.printStackTrace();
        }
        return _radiusEarthMiles * _centralAngle;
    }

    public boolean HasMoved200(double newLat, double newLan) {
        double significantDistance = 200;
        double currentDistance;

        currentDistance = DistanceMilesSEP200(Lat, Lan, newLat, newLan);
        currentDistance = currentDistance * _m2km * 1000;

        if (currentDistance < significantDistance) {
            return false;
        } else {
            Lat = newLat;
            Lan = newLan;
            return true;
        }
    }

    public double DistanceMilesSEP200(double Lat1, double Lon1, double Lat2, double Lon2) {
        try {
            double _radLat1 = Lat1 * _toRad;
            double _radLat2 = Lat2 * _toRad;
            double _dLat = (_radLat2 - _radLat1);
            double _dLon = (Lon2 - Lon1) * _toRad;

            double _a = (_dLon) * Math.cos((_radLat1 + _radLat2) / 2);

            // central angle, aka arc segment angular distance
            _centralAngle = Math.sqrt(_a * _a + _dLat * _dLat);

            // great-circle (orthodromic) distance on Earth between 2 points
        } catch (Exception e) {
            e.printStackTrace();
        }
        return _radiusEarthMiles * _centralAngle;
    }


    private void animateCarOnMap(final List<LatLng> latLngsList) {

        gMap.animateCamera(CameraUpdateFactory.newCameraPosition
                (new CameraPosition.Builder().target(latLngsList.get(0))
                        .zoom(18.5f).build()));

        if (driverMarker != null) {
            driverMarker.remove();
        }

        if (App.getAppPreference().getLoginDetail().getDetails().getDriverVehicleType().equalsIgnoreCase("1")) {
            driverMarker = gMap.addMarker(new MarkerOptions()
                    .rotation(getBearing(latLngsList.get(0), latLngsList.get(1)))
                    .flat(true)
                    .position(latLngsList.get(0))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
        } else {
            driverMarker = gMap.addMarker(new MarkerOptions()
                    .rotation(getBearing(latLngsList.get(0), latLngsList.get(1)))
                    .flat(true)
                    .position(latLngsList.get(0))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike)));
        }


        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(2500);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Float v = valueAnimator.getAnimatedFraction();
                double lng = v * latLngsList.get(1).longitude + (1 - v)
                        * latLngsList.get(0).longitude;
                double lat = v * latLngsList.get(1).latitude + (1 - v)
                        * latLngsList.get(0).latitude;
                LatLng newPos = new LatLng(lat, lng);
                driverMarker.setPosition(newPos);
                driverMarker.setAnchor(0.5f, 0.5f);
                driverMarker.setRotation(getBearing(latLngsList.get(0), newPos));
                gMap.animateCamera(CameraUpdateFactory.newCameraPosition
                        (new CameraPosition.Builder().target(newPos)
                                .zoom(18.5f).build()));
//                rotateMarker(driverMarker,v);
                driverMarker.setRotation(getBearing(latLngsList.get(0), newPos));


            }
        });
        valueAnimator.start();
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    private void setMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (aSwitch.isChecked()) {
            Toast.makeText(activity, "You're Now Online.", Toast.LENGTH_SHORT).show();
            ChangeStatus("1");
            App.getAppPreference().SaveString(ConstantData.ONLINE_STATUS, "1");
        } else {
            ChangeStatus("0");
            App.getAppPreference().SaveString(ConstantData.ONLINE_STATUS, "0");
            Toast.makeText(activity, "You're Now Offline.", Toast.LENGTH_SHORT).show();
        }


    }

    private void ChangeStatus(String s) {
        viewModel.status(activity, s, App.getAppPreference().GetString(ConstantData.USERID)).observe(HomeActivity.this, new Observer<Map>() {
            @Override
            public void onChanged(@Nullable Map map) {
                if (map.get("success").toString().equalsIgnoreCase("1")) {

                } else {

                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionCode;
            System.out.println("VersionCode : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        checkDriverJobViewModel.modelLiveData(HomeActivity.this).observe(HomeActivity.this, new Observer<VersionModel>() {
            @Override
            public void onChanged(@Nullable VersionModel versionModel) {
                if (versionModel.getSuccess().equalsIgnoreCase("1")) {
                    if (!versionModel.getDetails().getVersion().equalsIgnoreCase(String.valueOf(version))) {
                        OpenWarningBox();
                    }
                }
            }
        });


        if (App.getAppPreference().GetString(ConstantData.ONLINE_STATUS).equalsIgnoreCase("1")) {
            aSwitch.setChecked(true);
        } else {
            aSwitch.setChecked(false);
        }

        if (App.getAppPreference().getLoginDetail() != null) {
            tvUserName.setText(App.getAppPreference().getLoginDetail().getDetails().getDriverName());
//        tvCarName.setText(App.getAppPreference().GetString(ConstantData.VEHICLE_MODEL_NAME) + " " + App.getAppPreference().GetString(ConstantData.VEHICLE_PLATE));
            tvCarName.setText(App.getAppPreference().getLoginDetail().getDetails().getVehicleModel() + " " + App.getAppPreference().getLoginDetail().getDetails().getVehiclePlateNumber());
            tvprofileName.setText(App.getAppPreference().getLoginDetail().getDetails().getDriverName());
            Glide.with(activity).load(App.getAppPreference().getLoginDetail().getDetails().getDriverImage()).into(homeImage);
            Glide.with(activity).load(App.getAppPreference().getLoginDetail().getDetails().getDriverImage()).into(navImage);
            tv_number.setText(App.getAppPreference().getLoginDetail().getDetails().getDriverPhoneNumber());
            if (App.getAppPreference().getLoginDetail().getDetails().getDriverRating() != null) {
                ratingHome.setRating(Float.parseFloat(String.valueOf(App.getAppPreference().getLoginDetail().getDetails().getDriverRating())));
            }
        }

    }

    private void OpenWarningBox() {
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.update_app_pupup, viewGroup, false);
        Button buttonOk = dialogView.findViewById(R.id.buttonOk);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);


        //finally creating the alert dialog and displaying it
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.omninos.saowaridriver&hl=en")));
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
