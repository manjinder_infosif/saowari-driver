package com.omninos.saowaridriver.Activities;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.iid.FirebaseInstanceId;
import com.omninos.saowaridriver.model.CheckDriverPhoneModel;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.services.LocationService;
import com.omninos.saowaridriver.viewmodels.CheckDriverPhoneVM;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;

import android.Manifest;
import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.accountkit.AccountKit;
import com.omninos.saowaridriver.R;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private Button loginButton;
    private LoginActivity activity;
    public static int APP_REQUEST_CODE = 99;
    private CheckDriverPhoneVM checkDriverPhoneVM;
    private String phoneNumber;



    private LocationManager locationManager;
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        getLocation();

        activity = LoginActivity.this;
        checkDriverPhoneVM = ViewModelProviders.of(this).get(CheckDriverPhoneVM.class);
        initView();
        SetUp();

        String token = FirebaseInstanceId.getInstance().getToken();
    }



    private void initView() {
        loginButton = findViewById(R.id.login_btn);
    }

    private void SetUp() {
        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_btn:
                if (App.getSinltonPojo().getCurrentLatlng() != null) {
                    AccountKitLogin();
                } else {
                    CommonUtils.showSnackbarAlert(loginButton, "Please enable Location");
                }

                break;
        }
    }

    private void AccountKitLogin() {
        final Intent intent = new Intent(this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APP_REQUEST_CODE) {
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);

            if (loginResult.getError() != null) {
                Toast.makeText(this, loginResult.getError().getErrorType().getMessage(), Toast.LENGTH_SHORT).show();
            } else if (loginResult.wasCancelled()) {
                Toast.makeText(this, "Login Cancelled", Toast.LENGTH_SHORT).show();
            } else {
                if (loginResult.getAccessToken() != null) {
                    AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                        @Override
                        public void onSuccess(Account account) {
                            phoneNumber = account.getPhoneNumber().toString();
                            System.out.println("MobileNumber: " + phoneNumber);
                            checkDriverPhone(phoneNumber);
                        }

                        @Override
                        public void onError(AccountKitError accountKitError) {

                        }
                    });

                } else {
                    Toast.makeText(this, loginResult.getAuthorizationCode(), Toast.LENGTH_SHORT).show();

                }
            }
        }
    }

    private void startLocationService() {
        if (!isMyServiceRunning(LocationService.class)) {
            startService(new Intent(activity, LocationService.class));
            checkDriverPhone(phoneNumber);
        } else {
            checkDriverPhone(phoneNumber);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        assert manager != null;
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void checkDriverPhone(final String phoneNumber) {
        checkDriverPhoneVM.checkDriverPhone(activity, phoneNumber, App.getAppPreference().GetString(ConstantData.CURRENT_LAT), App.getAppPreference().GetString(ConstantData.CURRENT_LONG), FirebaseInstanceId.getInstance().getToken(), "Android", "normal").observe(this, new Observer<CheckDriverPhoneModel>() {
            @Override
            public void onChanged(@Nullable CheckDriverPhoneModel checkDriverPhoneModel) {
                if (checkDriverPhoneModel.getSuccess().equalsIgnoreCase("1")) {
                    //goto Home screen
//                    if (App.getAppPreference().GetString(ConstantData.TOKEN).equalsIgnoreCase("1"))
//                        startActivity(new Intent(activity, AppointmentConfirmationActivity.class));
                    if (checkDriverPhoneModel.getDetails().getAppointmentStatus().equalsIgnoreCase("0")) {
                        CommonUtils.showSnackbarAlert(loginButton, "Document not verified");
                    } else if (checkDriverPhoneModel.getDetails().getAppointmentStatus().equalsIgnoreCase("1")) {
                        Intent i = new Intent(activity, AppointmentConfirmationActivity.class);
                        i.putExtra("date", checkDriverPhoneModel.getDetails().getAppointmentDate());
                        i.putExtra("time", checkDriverPhoneModel.getDetails().getAppointmentTime());
                        i.putExtra("address", checkDriverPhoneModel.getDetails().getAppointmentAddress());
                        i.putExtra("status", "1");
                        startActivity(i);
                    } else if (checkDriverPhoneModel.getDetails().getAppointmentStatus().equalsIgnoreCase("2")) {
                        App.getAppPreference().saveLoginDetail(checkDriverPhoneModel);
                        App.getAppPreference().SaveString(ConstantData.TOKEN, "1");
                        App.getAppPreference().SaveString(ConstantData.ONLINE_STATUS, checkDriverPhoneModel.getDetails().getOnlineStatus());
                        App.getAppPreference().SaveString(ConstantData.IMAGEPATH, checkDriverPhoneModel.getDetails().getDriverImage());
                        App.getAppPreference().SaveString(ConstantData.USERID, checkDriverPhoneModel.getDetails().getId());
                        App.getAppPreference().SaveString(ConstantData.PHONE_NUMBER, checkDriverPhoneModel.getDetails().getDriverPhoneNumber());
                        App.getAppPreference().SaveString(ConstantData.Name, checkDriverPhoneModel.getDetails().getDriverName());
                        App.getAppPreference().SaveString(ConstantData.VEHICLE_MODEL, checkDriverPhoneModel.getDetails().getVehicleModel());
                        App.getAppPreference().SaveString(ConstantData.VEHICLE_PLATE, checkDriverPhoneModel.getDetails().getVehiclePlateNumber());
                        Toast.makeText(activity, checkDriverPhoneModel.getMessage(), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(activity, HomeActivity.class));
                        finishAffinity();
                    }

                } else if (checkDriverPhoneModel.getSuccess().equalsIgnoreCase("420")) {
                    CommonUtils.showSnackbarAlert(loginButton, checkDriverPhoneModel.getMessage());
                    App.getAppPreference().SaveString(ConstantData.USERID, checkDriverPhoneModel.getDetails().getId());
                    startActivity(new Intent(activity, SelectVehicleActivity.class));
                    finishAffinity();
                } else {
                    //goto Signup Screen
                    startActivity(new Intent(activity, SignUpActivity.class));
                    App.getSinltonPojo().setMobilenumber(phoneNumber);
                    Toast.makeText(activity, checkDriverPhoneModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getLocation() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 500, 0, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {

                    App.getSinltonPojo().setCurrentLatlng(new LatLng(location.getLatitude(), location.getLongitude()));
                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

                    try {
                        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        } else if (locationManager.isProviderEnabled(locationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, 500, 0, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    App.getSinltonPojo().setCurrentLatlng(new LatLng(location.getLatitude(), location.getLongitude()));
                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

                    try {
                        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        }
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }
}



