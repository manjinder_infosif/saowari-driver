package com.omninos.saowaridriver.Activities;

import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.DriverEarningViewModel;
import com.omninos.saowaridriver.model.EarningModel;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;

public class MyRideActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView backButton;
    private TextView earningText, totalTrip, balanceText;
    private DriverEarningViewModel viewModel;
    private RelativeLayout monthlyLayout, weeklyLayout, dailyLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ride);

        viewModel = ViewModelProviders.of(this).get(DriverEarningViewModel.class);


        stopService(new Intent(MyRideActivity.this, MySerives.class));
        Intent intent = new Intent(MyRideActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        findIds();
        setUps();
        getEarning();
    }

    private void getEarning() {
        viewModel.earningModelLiveData(MyRideActivity.this, App.getAppPreference().GetString(ConstantData.USERID)).observe(MyRideActivity.this, new Observer<EarningModel>() {
            @Override
            public void onChanged(@Nullable EarningModel earningModel) {
                if (earningModel.getSuccess().equalsIgnoreCase("1")) {
                    earningText.setText("৳" + earningModel.getDetails().getTotalEarning());
                    totalTrip.setText(earningModel.getDetails().getTotalTrips());
                    balanceText.setText("৳" + earningModel.getDetails().getBalance());
                } else {
                    CommonUtils.showSnackbarAlert(earningText, earningModel.getMessage());
                }
            }
        });
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void findIds() {
        backButton = findViewById(R.id.back_btn);
        earningText = findViewById(R.id.earningText);
        totalTrip = findViewById(R.id.totalTrip);
        balanceText = findViewById(R.id.balanceText);
        monthlyLayout = findViewById(R.id.monthlyLayout);
        weeklyLayout = findViewById(R.id.weeklyLayout);
        dailyLayout = findViewById(R.id.dailyLayout);
    }

    private void setUps() {
        backButton.setOnClickListener(this);
        monthlyLayout.setOnClickListener(this);
        weeklyLayout.setOnClickListener(this);
        dailyLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.monthlyLayout:
                Intent intent = new Intent(MyRideActivity.this, MonthlyStatementActivity.class);
                intent.putExtra("Type", "monthly");
                startActivity(intent);
                break;
            case R.id.weeklyLayout:
                Intent weekly = new Intent(MyRideActivity.this, MonthlyStatementActivity.class);
                weekly.putExtra("Type", "weekly");
                startActivity(weekly);
                break;

            case R.id.dailyLayout:
                Intent daily = new Intent(MyRideActivity.this, MonthlyStatementActivity.class);
                daily.putExtra("Type", "daily");
                startActivity(daily);
                break;


        }

    }
}
