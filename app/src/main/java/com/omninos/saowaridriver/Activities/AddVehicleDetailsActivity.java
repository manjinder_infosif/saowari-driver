package com.omninos.saowaridriver.Activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.adapters.VehicleMakeAdapter;
import com.omninos.saowaridriver.model.DummyPojo;
import com.omninos.saowaridriver.model.VehicleListModel;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.VehicleBrandsAndModelsVM;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddVehicleDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView firstIcon, lastIcon;
    private TextView title;
    private Button continueButton;
    private Spinner spinnerMake, spinnerModel, spinnerYear, spinnerColor;
    private VehicleBrandsAndModelsVM vehicleBrandsAndModelsVM;
    private Activity activity;
    private List<VehicleListModel.BrandDetail> vehicleMakeList;
    private List<VehicleListModel.MoreDetail> vehicleModelList;
    private RelativeLayout rlImage;
    private CircleImageView civVehicle;
    private EditText etPlateNumber;
    private String makeId = "", vehicleYear, vehicleColor, modelId = "", vehiclePlateNumber, modelName;
    private String[] carYear = {"2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009",
            "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021",
            "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030"};
    private String[] carColor = {"Black", "White", "Grey", "Yellow", "Red", "Orange", "Green", "Silver"};
    private Bitmap bitmap;
    private Uri uri;
    private static final int PICK_FROM_GALLERY = 100;
    private String imagepath = "";
    private File photoFile;
    private static final int CAMERA_REQUEST = 121;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicle_details);
        vehicleBrandsAndModelsVM = ViewModelProviders.of(this).get(VehicleBrandsAndModelsVM.class);

        stopService(new Intent(AddVehicleDetailsActivity.this, MySerives.class));
        Intent intent = new Intent(AddVehicleDetailsActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        initView();
        SetUp();
        getMakeApi();
        spinners();
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void initView() {
        firstIcon = findViewById(R.id.firstIcon);
        lastIcon = findViewById(R.id.lastIcon);
        title = findViewById(R.id.title);
        continueButton = findViewById(R.id.continue_btn);
        rlImage = findViewById(R.id.userimg);
        civVehicle = findViewById(R.id.img_vehicle);
        etPlateNumber = findViewById(R.id.et_plate_number);

        //spinners
        spinnerMake = findViewById(R.id.spinner_make);
        spinnerModel = findViewById(R.id.spinner_model);
        spinnerYear = findViewById(R.id.spinner_year);
        spinnerColor = findViewById(R.id.spinner_color);
    }

    private void SetUp() {
        activity = AddVehicleDetailsActivity.this;

        firstIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_left_black_24dp));
        title.setText("Add Vehicle Details");

        continueButton.setOnClickListener(this);
        firstIcon.setOnClickListener(this);
        rlImage.setOnClickListener(this);
    }

    private void spinners() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, carYear);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerYear.setAdapter(spinnerArrayAdapter);
        spinnerYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                vehicleYear = parent.getSelectedItem().toString().trim();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(this, R.layout.spinner_item, carColor);
        spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerColor.setAdapter(spinnerArrayAdapter2);

        spinnerColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                vehicleColor = parent.getSelectedItem().toString().trim();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getMakeApi() {
        vehicleBrandsAndModelsVM.moterCyclerList(activity,App.getAppPreference().GetString(ConstantData.VEHICLE_TYPE)).observe(this, new Observer<VehicleListModel>() {
            @Override
            public void onChanged(@Nullable VehicleListModel vehicleListModel) {
                if (vehicleListModel.getSuccess().equalsIgnoreCase("1")) {
                    vehicleMakeList = vehicleListModel.getBrandDetails();
                    final List<DummyPojo> dummyPojoList = new ArrayList<>();
                    for (int i = 0; i < vehicleMakeList.size(); i++) {
                        dummyPojoList.add(new DummyPojo(vehicleMakeList.get(i).getTitle(),
                                vehicleMakeList.get(i).getId()));
                    }
                    spinnerMake.setAdapter(new VehicleMakeAdapter(activity, dummyPojoList));
                    spinnerMake.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            makeId = vehicleMakeList.get(position).getId();
                            getModelsList(vehicleMakeList.get(position).getMoreDetails());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });

                } else {
                    Toast.makeText(activity, vehicleListModel.getMessage(), Toast.LENGTH_LONG).show();

                }
            }
        });
    }

    private void getModelsList(final List<VehicleListModel.MoreDetail> vehicleModelList) {
        final List<DummyPojo> dummyPojoList = new ArrayList<>();
        for (int i = 0; i < vehicleModelList.size(); i++) {
            dummyPojoList.add(new DummyPojo(vehicleModelList.get(i).getTitle(),
                    vehicleModelList.get(i).getId()));
        }

        spinnerModel.setAdapter(new VehicleMakeAdapter(activity, dummyPojoList));
        spinnerModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                modelId = vehicleModelList.get(position).getId();
                modelName = vehicleModelList.get(position).getTitle();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.firstIcon:
                finish();
                break;
            case R.id.userimg:
                selectImage();
                break;
            case R.id.continue_btn:
                validation();
                break;
        }
    }

    private void validation() {
        vehiclePlateNumber = etPlateNumber.getText().toString();

        if (imagepath.isEmpty()) {
            Toast.makeText(activity, "Please select a vehicle image", Toast.LENGTH_SHORT).show();
        } else if (makeId.isEmpty()) {
            Toast.makeText(activity, "Please select a vehicle brand", Toast.LENGTH_SHORT).show();
        } else if (modelId.isEmpty()) {
            Toast.makeText(activity, "Please select a vehicle model", Toast.LENGTH_SHORT).show();
        } else if (vehicleYear.isEmpty()) {
            Toast.makeText(activity, "Please select a vehicle year", Toast.LENGTH_SHORT).show();
        } else if (vehicleColor.isEmpty()) {
            Toast.makeText(activity, "Please select a vehicle color", Toast.LENGTH_SHORT).show();
        } else if (vehiclePlateNumber.isEmpty()) {
            Toast.makeText(activity, "Please type a valid plate number", Toast.LENGTH_SHORT).show();
        } else {

            App.getAppPreference().SaveString(ConstantData.VEHICLE_IMAGE, imagepath);
            App.getAppPreference().SaveString(ConstantData.VEHICLE_BRAND, makeId);
            App.getAppPreference().SaveString(ConstantData.VEHICLE_MODEL, modelId);
            App.getAppPreference().SaveString(ConstantData.VEHICLE_YEAR, vehicleYear);
            App.getAppPreference().SaveString(ConstantData.VEHICLE_COLOR, vehicleColor);
            App.getAppPreference().SaveString(ConstantData.VEHICLE_PLATE, vehiclePlateNumber);
            App.getAppPreference().SaveString(ConstantData.VEHICLE_MODEL_NAME, modelName);

            startActivity(new Intent(activity, UploadDocumentActivity.class));
        }
    }

    //image path
    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_FROM_GALLERY);
    }

    private void cameraIntent() {


//        Intent pictureIntent = new Intent(
//                MediaStore.ACTION_IMAGE_CAPTURE);
//
//        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
//            //Create a file to store the image
//            photoFile = null;
//            photoFile = ImageUtil.getTemporaryCameraFile();
//            if (photoFile != null) {
//                Uri uri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", photoFile);
//                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
//                startActivityForResult(pictureIntent,
//                        CAMERA_REQUEST);
//            }
//        }
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = this.getPackageManager();
        List<ResolveInfo> listcam = packageManager.queryIntentActivities(intent, 0);
        intent.setPackage(listcam.get(0).activityInfo.packageName);
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                uri = getImageUri(AddVehicleDetailsActivity.this, bitmap);
                imagepath = getRealPathFromUri(uri);
                App.getAppPreference().SaveString(ConstantData.VEHICAL_REGISTRARTION_IMAGE,imagepath);
                civVehicle.setImageBitmap(bitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Uri getImageUri(AddVehicleDetailsActivity youractivity, Bitmap bitmap) {
        String path = MediaStore.Images.Media.insertImage(youractivity.getContentResolver(), bitmap, "Title", null);
        return Uri.parse(path);
    }

    private String getRealPathFromUri(Uri tempUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = this.getContentResolver().query(tempUri, proj, null, null, null);
            assert cursor != null;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FROM_GALLERY)
                onSelectFromGalleryResult(data);
            else if (requestCode == CAMERA_REQUEST)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        try {
            bitmap = (Bitmap) data.getExtras().get("data");
            uri = getImageUri(AddVehicleDetailsActivity.this, bitmap);
            imagepath = getRealPathFromUri(uri);
            App.getAppPreference().SaveString(ConstantData.IMAGEPATH, imagepath);
            App.getAppPreference().SaveString(ConstantData.VEHICAL_REGISTRARTION_IMAGE,imagepath);
            Glide.with(this).load("file://" + imagepath).into(civVehicle);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
