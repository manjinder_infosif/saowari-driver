package com.omninos.saowaridriver.Activities;

import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.PendingAmountModel;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.MyTripsViewModel;

public class MyWalletActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView backButton;
    private TextView tvViewTransactions, pendingAmount;
    private MyTripsViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        viewModel = ViewModelProviders.of(this).get(MyTripsViewModel.class);
        stopService(new Intent(MyWalletActivity.this, MySerives.class));
        Intent intent = new Intent(MyWalletActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }


        findIds();
        setUps();

        getWalletDetail("0");

    }

    private void getWalletDetail(final String status) {
        viewModel.pendingAmountModelLiveData(MyWalletActivity.this, App.getAppPreference().GetString(ConstantData.USERID), status).observe(MyWalletActivity.this, new Observer<PendingAmountModel>() {
            @Override
            public void onChanged(@Nullable PendingAmountModel pendingAmountModel) {
                if (pendingAmountModel.getSuccess().equalsIgnoreCase("1")) {
                    if (status.equalsIgnoreCase("0")) {
                        pendingAmount.setText("Your Pending Amount: ৳" + pendingAmountModel.getDetails());
                    }
                } else {
                    CommonUtils.showSnackbarAlert(pendingAmount, "No Data");
                }
            }
        });
    }

    private void findIds() {
        backButton = findViewById(R.id.back_btn);
        tvViewTransactions = findViewById(R.id.tv_view_transations);

        pendingAmount = findViewById(R.id.pendingAmount);
    }

    private void setUps() {
        backButton.setOnClickListener(this);
        tvViewTransactions.setOnClickListener(this);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;

            case R.id.tv_view_transations:
                getWalletDetail("1");
                break;
        }

    }
}
