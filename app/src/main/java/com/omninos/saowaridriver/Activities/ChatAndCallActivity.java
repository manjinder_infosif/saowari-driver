package com.omninos.saowaridriver.Activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.fragments.CallsFragment;
import com.omninos.saowaridriver.fragments.ChatsFragment;
import com.omninos.saowaridriver.services.MySerives;

import java.util.ArrayList;
import java.util.List;

public class ChatAndCallActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, View.OnClickListener {

    private ImageView backButton;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private String[] string = {"Chats", "Calls"};
    Fragment fragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_and_call);

        stopService(new Intent(ChatAndCallActivity.this, MySerives.class));
        Intent intent = new Intent(ChatAndCallActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        findIds();
        setUps();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void setUps() {
        backButton.setOnClickListener(this);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    private void findIds() {
        backButton = findViewById(R.id.back_btn);
        tabLayout = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.viewpager_chats_calls);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ChatsFragment(), string[0]);
        adapter.addFragment(new CallsFragment(), string[1]);

        viewPager.setAdapter(adapter);
//        for (int i = 0; i < tabLayoutTrips.getTabCount(); i++) {
//            TabLayout.Tab tab = tabLayoutTrips.getTabAt(i);
//            tab.setCustomView(adapter.getTabView(i));
//
//        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmenttitle = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmenttitle.add(title);
        }

//        public View getTabView(int position) {
//            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
//            View v = LayoutInflater.from(getActivity()).inflate(R.layout.layout_custom_tab_view, null);
//            TextView tv_heading = v.findViewById(R.id.tv_heading);
//            tv_heading.setText(string[position]);
//            return v;
//        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return fragmenttitle.get(position);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
        }

    }
}
