package com.omninos.saowaridriver.Activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView backButton, editProfileButton;
    private Activity activity;
    private TextView tv_about, tv_address, tv_email, tv_phone, tv_name,tv_location;
    private CircleImageView driverImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        activity = ProfileActivity.this;

        stopService(new Intent(ProfileActivity.this, MySerives.class));
        Intent intent = new Intent(ProfileActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        findIds();
        setUps();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void setUps() {
        backButton.setOnClickListener(this);
        editProfileButton.setOnClickListener(this);


    }

    private void findIds() {
        backButton = findViewById(R.id.back_btn);
        editProfileButton = findViewById(R.id.edit_profile_btn);
        tv_about = findViewById(R.id.tv_about);
        tv_address = findViewById(R.id.tv_address);
        tv_email = findViewById(R.id.tv_email);
        tv_phone = findViewById(R.id.tv_phone);
        driverImage = findViewById(R.id.driverImage);
        tv_name = findViewById(R.id.tv_name);
        tv_location=findViewById(R.id.tv_location);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;

            case R.id.edit_profile_btn:
                startActivity(new Intent(activity, EditProfileActivity.class));
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Glide.with(ProfileActivity.this).load(App.getAppPreference().getLoginDetail().getDetails().getDriverImage()).into(driverImage);
        tv_name.setText(App.getAppPreference().getLoginDetail().getDetails().getDriverName());
        tv_phone.setText(App.getAppPreference().getLoginDetail().getDetails().getDriverPhoneNumber());
        tv_email.setText(App.getAppPreference().getLoginDetail().getDetails().getDriverEmail());

        tv_address.setText(App.getAppPreference().getLoginDetail().getDetails().getAddress());
        tv_location.setText(App.getAppPreference().getLoginDetail().getDetails().getAddress());
//        try {
//            Geocoder geocoder;
//            List<Address> addresses;
//            geocoder = new Geocoder(this, Locale.getDefault());
//
//            addresses = geocoder.getFromLocation(Double.parseDouble(App.getAppPreference().getLoginDetail().getDetails().getLatitude()), Double.parseDouble(App.getAppPreference().getLoginDetail().getDetails().getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//
//            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//            String city = addresses.get(0).getLocality();
//            String state = addresses.get(0).getAdminArea();
//            String country = addresses.get(0).getCountryName();
//            String postalCode = addresses.get(0).getPostalCode();
//            String knownName = addresses.get(0).getFeatureName();
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
}
