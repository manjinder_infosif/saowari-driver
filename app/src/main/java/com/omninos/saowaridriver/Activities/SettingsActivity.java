package com.omninos.saowaridriver.Activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView backButton;
    private CardView cvHelp, cvContactUs;
    private Activity activity;
    private CircleImageView userImage;
    private TextView driverName, driverAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        activity = SettingsActivity.this;

        stopService(new Intent(SettingsActivity.this, MySerives.class));
        Intent intent = new Intent(SettingsActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        findIds();
        setUps();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void findIds() {
        backButton = findViewById(R.id.back_btn);
        cvHelp = findViewById(R.id.cv_help);
        cvContactUs = findViewById(R.id.cv_contact_us);

        userImage = findViewById(R.id.userImage);
        driverName = findViewById(R.id.driverName);
        driverAddress = findViewById(R.id.driverAddress);
    }

    private void setUps() {
        backButton.setOnClickListener(this);
        cvHelp.setOnClickListener(this);
        cvContactUs.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;

            case R.id.cv_help:
                startActivity(new Intent(activity, HelpActivity.class));
                break;

            case R.id.cv_contact_us:
                startActivity(new Intent(activity, ContactUsActivity.class));
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Glide.with(activity).load(App.getAppPreference().getLoginDetail().getDetails().getDriverImage()).into(userImage);
        driverName.setText(App.getAppPreference().getLoginDetail().getDetails().getDriverName());
        driverAddress.setText(App.getAppPreference().getLoginDetail().getDetails().getAddress());
//        if (!App.getAppPreference().getLoginDetail().getDetails().getLatitude().isEmpty()){
//            try {
//                Geocoder geocoder;
//                List<Address> addresses;
//                geocoder = new Geocoder(this, Locale.getDefault());
//
//                addresses = geocoder.getFromLocation(Double.parseDouble(App.getAppPreference().getLoginDetail().getDetails().getLatitude()), Double.parseDouble(App.getAppPreference().getLoginDetail().getDetails().getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//
//                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                String city = addresses.get(0).getLocality();
//                String state = addresses.get(0).getAdminArea();
//                String country = addresses.get(0).getCountryName();
//                String postalCode = addresses.get(0).getPostalCode();
//                String knownName = addresses.get(0).getFeatureName();
//
//                driverAddress.setText(address);
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }


    }
}
