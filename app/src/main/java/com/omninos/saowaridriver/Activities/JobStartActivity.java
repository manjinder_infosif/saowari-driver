package com.omninos.saowaridriver.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.JobFlowPojo;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.JobFlowViewModel;

public class JobStartActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    private Activity activity;

    private GoogleMap map;

    private ImageView gpsStartJob, imageStartJob, callStartJob;
    private TextView nameStartJob;
    private RatingBar ratingStartJob;
    private Button startJobBtn;

    private String mobileNo;
    private int change = 0;
    private LatLng oldLatlng;

    private JobFlowViewModel jobFlowViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_start);

        findIds();

        setDriverDetails();

    }

    private void findIds() {

        activity = JobStartActivity.this;

        jobFlowViewModel = ViewModelProviders.of(this).get(JobFlowViewModel.class);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapStartJob);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        gpsStartJob = findViewById(R.id.gpsStartJob);
        gpsStartJob.setOnClickListener(this);

        imageStartJob = findViewById(R.id.imageStartJob);
        nameStartJob = findViewById(R.id.nameStartJob);
        ratingStartJob = findViewById(R.id.ratingStartJob);

        callStartJob = findViewById(R.id.callStartJob);
        callStartJob.setOnClickListener(this);

        startJobBtn = findViewById(R.id.startJobBtn);
        startJobBtn.setOnClickListener(this);

    }

    private void setDriverDetails() {

        String rating = App.getAppPreference().GetString(ConstantData.JOB_CUSTOMER_RATING);
        String name = App.getAppPreference().GetString(ConstantData.JOB_CUSTOMER_NAME);
        String image = App.getAppPreference().GetString(ConstantData.JOB_CUSTOMER_IMAGE);

        mobileNo = App.getAppPreference().GetString(ConstantData.JOB_CUSTOMER_MOBILE);

        Glide.with(activity).load(image).into(imageStartJob);
        nameStartJob.setText(name);
        ratingStartJob.setRating(Float.parseFloat(rating));

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.gpsStartJob:
                zoomCurrentLocation();
                break;

            case R.id.callStartJob:
                if (isPermissionGranted()) {
                    call_action();
                }
                break;

            case R.id.startJobBtn:
                startJobDialog();
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;

        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (change == 1) {
            Double lat = Double.valueOf(App.getAppPreference().GetString(ConstantData.CURRENT_LAT));
            Double lon = Double.valueOf(App.getAppPreference().GetString(ConstantData.CURRENT_LONG));

            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 14));

            LatLng currentLatLng = new LatLng(lat, lon);

            MarkerOptions markerOptions = new MarkerOptions();
            // Setting the position for the marker
            markerOptions.position(currentLatLng);
            if (App.getAppPreference().getLoginDetail().getDetails().getDriverVehicleType().equalsIgnoreCase("1")){
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.car));
            }else {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.bike));
            }

            // Animating to the touched position
            // Placing a marker on the touched position
            Marker driverMarker = map.addMarker(markerOptions);

            driverMarker.setRotation(getBearing(oldLatlng, currentLatLng));

        } else {
            Double lat = Double.valueOf(App.getAppPreference().GetString(ConstantData.CURRENT_LAT));
            Double lon = Double.valueOf(App.getAppPreference().GetString(ConstantData.CURRENT_LONG));

            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 14));

            change = 1;

            oldLatlng = new LatLng(lat,lon);
        }


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        map.setMyLocationEnabled(false);

    }

    private void zoomCurrentLocation() {

        Double lat = Double.valueOf(App.getAppPreference().GetString(ConstantData.CURRENT_LAT));
        Double lon = Double.valueOf(App.getAppPreference().GetString(ConstantData.CURRENT_LONG));

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 14));

    }

    private void startJobDialog() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
        builder1.setMessage("Starting Job?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        arriveApi("4");
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    private void arriveApi(final String jobStatus) {

        String jobId = App.getAppPreference().GetString(ConstantData.JOB_ID);

        jobFlowViewModel.acceptRejectJob(activity, jobStatus, jobId).observe(this,
                new Observer<JobFlowPojo>() {
                    @Override
                    public void onChanged(@Nullable JobFlowPojo jobFlowPojo) {
                        if (jobFlowPojo.getSuccess().equalsIgnoreCase("1")) {
                            startActivity(new Intent(activity, JobDropActivity.class));
                            finish();
                        } else {
                            Toast.makeText(activity, jobFlowPojo.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    public void call_action() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + mobileNo));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {
                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},
                        100);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 100: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    call_action();
                } else {
//                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }
}
