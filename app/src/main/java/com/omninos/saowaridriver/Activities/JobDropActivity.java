package com.omninos.saowaridriver.Activities;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.DirectionPojo;
import com.omninos.saowaridriver.model.JobFlowPojo;
import com.omninos.saowaridriver.retrofit.Api;
import com.omninos.saowaridriver.retrofit.ApiClient;
import com.omninos.saowaridriver.services.LocationService;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.JobFlowViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobDropActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    private Activity activity;

    private GoogleMap map;

    private ImageView imageDropJob;
    private TextView nameDropJob;
    private RatingBar ratingDropJob;

    private JobFlowViewModel jobFlowViewModel;

    private TimerTask timerTask;
    private Timer timer;

    //To calculate distance of to gmap points
    double _radiusEarthMiles = 3959;
    double _radiusEarthKM = 6371;
    double _m2km = 1.60934;
    double _toRad = Math.PI / 180;
    private double _centralAngle;

    private double Lat = 0.0, Lan = 0.0;

    private Marker driverMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_drop);

        findIds();

        setDriverDetails();

    }

    private void findIds() {

        activity = JobDropActivity.this;

        jobFlowViewModel = ViewModelProviders.of(this).get(JobFlowViewModel.class);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapDropJob);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        ImageView gpsDropJob = findViewById(R.id.gpsDropJob);
        gpsDropJob.setOnClickListener(this);

        imageDropJob = findViewById(R.id.imageDropJob);
        nameDropJob = findViewById(R.id.nameDropJob);
        ratingDropJob = findViewById(R.id.ratingDropJob);

        Button dropJobBtn = findViewById(R.id.dropJobBtn);
        dropJobBtn.setOnClickListener(this);

    }

    private void setDriverDetails() {

        String rating = App.getAppPreference().GetString(ConstantData.JOB_CUSTOMER_RATING);
        String name = App.getAppPreference().GetString(ConstantData.JOB_CUSTOMER_NAME);
        String image = App.getAppPreference().GetString(ConstantData.JOB_CUSTOMER_IMAGE);

        Glide.with(activity).load(image).into(imageDropJob);
        nameDropJob.setText(name);
        ratingDropJob.setRating(Float.parseFloat(rating));

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.gpsDropJob:
                zoomCurrentLocation();
                break;

            case R.id.dropJobBtn:
                dropDialog();
                break;
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;

        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        double lat = Double.valueOf(App.getAppPreference().GetString(ConstantData.CURRENT_LAT));
        double lon = Double.valueOf(App.getAppPreference().GetString(ConstantData.CURRENT_LONG));

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 14));

        if (timer == null) {
            startTimer();
        }

        makeRouteAndMarkers();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        map.setMyLocationEnabled(false);

    }

    private void makeRouteAndMarkers() {


        Double lat = Double.valueOf(App.getAppPreference().GetString(ConstantData.CURRENT_LAT));
        Double lon = Double.valueOf(App.getAppPreference().GetString(ConstantData.CURRENT_LONG));

        Double pickLat = Double.valueOf(App.getAppPreference().GetString(ConstantData.JOB_DROP_LAT));
        Double pickLong = Double.valueOf(App.getAppPreference().GetString(ConstantData.JOB_DROP_LONG));

        List<LatLng> latLngList = new ArrayList<>();

        LatLng latLngCurrent = new LatLng(lat, lon);
        latLngList.add(latLngCurrent);

        LatLng latLngPickup = new LatLng(pickLat, pickLong);
        latLngList.add(latLngPickup);

        MarkerOptions markerOptionCurrent = new MarkerOptions();
        markerOptionCurrent.position(latLngCurrent);
        markerOptionCurrent.icon(BitmapDescriptorFactory.fromResource(R.drawable.source_marker));
        Marker current = map.addMarker(markerOptionCurrent);

        MarkerOptions markerOptionPickup = new MarkerOptions();
        markerOptionPickup.position(latLngPickup);
        markerOptionPickup.icon(BitmapDescriptorFactory.fromResource(R.drawable.destination_marker));
        Marker pickup = map.addMarker(markerOptionPickup);

        showRouteApi(lat, lon, pickLat, pickLong);

        final LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latlng : latLngList) {
            builder.include(latlng);
        }

        int width = getResources().getDisplayMetrics().widthPixels;
        int heights = getResources().getDisplayMetrics().widthPixels;
        int padding = (int) (width * 0.12); // offset from edges of the map 12% of screen

        map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), width, heights, padding));

    }

    private void zoomCurrentLocation() {

        double lat = Double.valueOf(App.getAppPreference().GetString(ConstantData.CURRENT_LAT));
        double lon = Double.valueOf(App.getAppPreference().GetString(ConstantData.CURRENT_LONG));

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 14));

    }

    private void dropDialog() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
        builder1.setMessage("Dropping Customer?");
        builder1.setCancelable(true);

        builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                dropJobApi("5");
            }
        });

        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    private void dropJobApi(final String jobStatus) {

        String jobId = App.getAppPreference().GetString(ConstantData.JOB_ID);

        jobFlowViewModel.acceptRejectJob(activity, jobStatus, jobId).observe(this,
                new Observer<JobFlowPojo>() {
                    @Override
                    public void onChanged(@Nullable JobFlowPojo jobFlowPojo) {
                        if (jobFlowPojo.getSuccess().equalsIgnoreCase("1")) {
                            startActivity(new Intent(activity, JobRatingActivity.class));
                            finish();
                        } else {
                            Toast.makeText(activity, jobFlowPojo.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();
        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 2 second
        timer.schedule(timerTask, 2500, 2500); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                startLocationService();

                double lat = Double.parseDouble(App.getAppPreference().GetString(ConstantData.CURRENT_LAT));
                double lan = Double.parseDouble(App.getAppPreference().GetString(ConstantData.CURRENT_LONG));

                final LatLng latLngOld = new LatLng(Lat, Lan);
                final LatLng latLngNew = new LatLng(lat, lan);

                if (HasMoved(lat, lan)) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            animateMarker(latLngOld, latLngNew, false);
                            List<LatLng> latLngsList = new ArrayList<>();
                            latLngsList.add(latLngOld);
                            latLngsList.add(latLngNew);

                            animateCarOnMap(latLngsList);

                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (driverMarker != null) {
                                driverMarker.remove();
                            }
                            MarkerOptions markerOptions = new MarkerOptions();
                            // Setting the position for the marker
                            markerOptions.position(latLngOld);
                            if (App.getAppPreference().getLoginDetail().getDetails().getDriverVehicleType().equalsIgnoreCase("1")){
                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.car));
                            }else {
                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.bike));
                            }

                            // Animating to the touched position
                            map.animateCamera(CameraUpdateFactory.newLatLng(latLngOld));
                            // Placing a marker on the touched position
                            driverMarker = map.addMarker(markerOptions);
                            driverMarker.setRotation(getBearing(latLngOld, latLngNew));
                        }
                    });
                }
            }
        };
    }

    /**
     * not needed
     */
    public void stopTimerTask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public boolean HasMoved(double newLat, double newLan) {
        double significantDistance = 30;
        double currentDistance;

        currentDistance = DistanceMilesSEP(Lat, Lan, newLat, newLan);
        currentDistance = currentDistance * _m2km * 1000;

        if (currentDistance < significantDistance) {
            return false;
        } else {
            Lat = newLat;
            Lan = newLan;
            return true;
        }
    }

    public double DistanceMilesSEP(double Lat1, double Lon1, double Lat2, double Lon2) {
        try {
            double _radLat1 = Lat1 * _toRad;
            double _radLat2 = Lat2 * _toRad;
            double _dLat = (_radLat2 - _radLat1);
            double _dLon = (Lon2 - Lon1) * _toRad;

            double _a = (_dLon) * Math.cos((_radLat1 + _radLat2) / 2);

            // central angle, aka arc segment angular distance
            _centralAngle = Math.sqrt(_a * _a + _dLat * _dLat);

            // great-circle (orthodromic) distance on Earth between 2 points
        } catch (Exception e) {
            e.printStackTrace();
        }
        return _radiusEarthMiles * _centralAngle;
    }


    private void animateCarOnMap(final List<LatLng> latLngsList) {
//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
//        for (LatLng latLng : latLngs) {
//            builder.include(latLng);
//        }
//        LatLngBounds bounds = builder.build();
//        CameraUpdate mCameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 2);
        map.animateCamera(CameraUpdateFactory.newCameraPosition
                (new CameraPosition.Builder().target(latLngsList.get(0))
                        .zoom(18.5f).build()));

        if (driverMarker != null) {
            driverMarker.remove();
        }

        if (App.getAppPreference().getLoginDetail().getDetails().getDriverVehicleType().equalsIgnoreCase("1")){
            driverMarker = map.addMarker(new MarkerOptions()
                    .rotation(getBearing(latLngsList.get(0), latLngsList.get(1)))
                    .flat(true)
                    .position(latLngsList.get(0))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
        }else {
            driverMarker = map.addMarker(new MarkerOptions()
                    .rotation(getBearing(latLngsList.get(0), latLngsList.get(1)))
                    .flat(true)
                    .position(latLngsList.get(0))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike)));
        }



        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(2500);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Float v = valueAnimator.getAnimatedFraction();
                double lng = v * latLngsList.get(1).longitude + (1 - v)
                        * latLngsList.get(0).longitude;
                double lat = v * latLngsList.get(1).latitude + (1 - v)
                        * latLngsList.get(0).latitude;
                LatLng newPos = new LatLng(lat, lng);
                driverMarker.setPosition(newPos);
                driverMarker.setAnchor(0.5f, 0.5f);
                driverMarker.setRotation(getBearing(latLngsList.get(0), newPos));
                map.animateCamera(CameraUpdateFactory.newCameraPosition
                        (new CameraPosition.Builder().target(newPos)
                                .zoom(18.5f).build()));
//                rotateMarker(driverMarker,v);
                driverMarker.setRotation(getBearing(latLngsList.get(0), newPos));


            }
        });
        valueAnimator.start();
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    private void showRouteApi(final Double srcLat, final Double srcLong, Double desLat, Double desLong) {

        Map<String, String> data = new HashMap<>();

        data.put("origin", srcLat + "," + srcLong);
        data.put("destination", desLat + "," + desLong);
        data.put("key", activity.getResources().getString(R.string.map_key));

        if (CommonUtils.isNetworkConnected(activity)) {

            Api apiInterface = ApiClient.getClientRoute().create(Api.class);

            apiInterface.getPolyLine(data).enqueue(new Callback<DirectionPojo>() {
                @Override
                public void onResponse(@NonNull Call<DirectionPojo> call, @NonNull Response<DirectionPojo> response) {

                    List<DirectionPojo.Route> routeList = response.body().getRoutes();
                    PolylineOptions polylineOptions = new PolylineOptions();
                    polylineOptions.width(10).color(getResources().getColor(R.color.red)).geodesic(true);

                    for (int i = 0; i < routeList.size(); i++) {
                        List<DirectionPojo.Leg> legList = routeList.get(0).getLegs();
                        for (int j = 0; j < legList.size(); j++) {
                            List<DirectionPojo.Step> stepList = legList.get(0).getSteps();
                            for (int k = 0; k < stepList.size(); k++) {
                                String polyline = stepList.get(k).getPolyline().getPoints();
                                List<LatLng> latlngList = decodePolyline(polyline);
                                for (int z = 0; z < latlngList.size(); z++) {
                                    LatLng point = latlngList.get(z);
                                    polylineOptions.add(point);

                                }
                            }
                        }
                    }

                    Polyline polyline = map.addPolyline(polylineOptions);

                }

                @Override
                public void onFailure(@NonNull Call<DirectionPojo> call, @NonNull Throwable t) {
                    Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private List<LatLng> decodePolyline(String polyline) {

        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = polyline.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = polyline.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = polyline.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (timer == null) {
            startTimer();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopTimerTask();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopTimerTask();
    }


    private void startLocationService() {
        if (!isMyServiceRunning(LocationService.class)) {
            startService(new Intent(activity, LocationService.class));
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        assert manager != null;
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
