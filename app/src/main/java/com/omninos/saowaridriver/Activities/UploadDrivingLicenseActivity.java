package com.omninos.saowaridriver.Activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.util.DateDifferentiate;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class UploadDrivingLicenseActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView backButton;
    private Button continueButton;
    private TextView tvIssuedDate, tvExpirationDate;
    private String issuedDateLicenseS = "", expiryDateLicenseS = "", licenseS = "", validVehicleTypeLicenseS = "";
    private Activity activity;
    private EditText etLicenseNumber, etVehicleType;
    private CircleImageView civLicense;
    private RelativeLayout rlImage;

    private Bitmap bitmap;
    private Uri uri;
    private static final int PICK_FROM_GALLERY = 100;
    private String imagepath = "";
    private File photoFile;
    private static final int CAMERA_REQUEST = 121;
    private Spinner spinnerVehicle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_driving_license);

        stopService(new Intent(UploadDrivingLicenseActivity.this, MySerives.class));
        Intent intent = new Intent(UploadDrivingLicenseActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        findIds();
        setUps();
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void setUps() {
        activity = UploadDrivingLicenseActivity.this;
        backButton.setOnClickListener(this);
        continueButton.setOnClickListener(this);
        tvIssuedDate.setOnClickListener(this);
        tvExpirationDate.setOnClickListener(this);
        rlImage.setOnClickListener(this);

        if (App.getAppPreference().GetString(ConstantData.DRIVER_LICENSE_DETAILS).equalsIgnoreCase("1")) {

            issuedDateLicenseS = App.getAppPreference().GetString(ConstantData.DRIVER_LICENSE_ISSUE_DATE);
            expiryDateLicenseS = App.getAppPreference().GetString(ConstantData.DRIVER_LICENSE_EXPIRY_DATE);
            licenseS = App.getAppPreference().GetString(ConstantData.DRIVER_LICENSE_NUMBER);
            validVehicleTypeLicenseS = App.getAppPreference().GetString(ConstantData.DRIVER_VALID_VEHICLE_TYPE);

            tvIssuedDate.setText(issuedDateLicenseS);
            tvExpirationDate.setText(expiryDateLicenseS);
            etLicenseNumber.setText(licenseS);
            etVehicleType.setText(validVehicleTypeLicenseS);

            imagepath = App.getAppPreference().GetString(ConstantData.DRIVER_LICENSE_IMAGE);

            File image = new File(imagepath);
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
            civLicense.setImageBitmap(bitmap);

        }
    }

    private void findIds() {
        backButton = findViewById(R.id.back_btn);
        continueButton = findViewById(R.id.continue_btn);

        tvIssuedDate = findViewById(R.id.tv_issued_date);
        tvExpirationDate = findViewById(R.id.tv_expiration_date);
        etLicenseNumber = findViewById(R.id.et_license_number);
        etVehicleType = findViewById(R.id.et_vehicle_type);

        civLicense = findViewById(R.id.civ_image);
        rlImage = findViewById(R.id.userimg);

        spinnerVehicle = findViewById(R.id.spinnerVehicle);


        spinnerVehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                Typeface typeface = ResourcesCompat.getFont(UploadDrivingLicenseActivity.this, R.font.poppins);
                ((TextView) view).setTypeface(typeface);
                String data = parent.getItemAtPosition(position).toString();
                if (data.equalsIgnoreCase("2 Wheeler")){
                    validVehicleTypeLicenseS="2";
                }else if (data.equalsIgnoreCase("4 Wheeler")){
                    validVehicleTypeLicenseS="1";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.userimg:
                selectImage();
                break;
            case R.id.continue_btn:
                validation();
                break;
            case R.id.tv_issued_date:
                datePicker(0);
                break;
            case R.id.tv_expiration_date:
                datePicker(1);
                break;
        }
    }

    private void validation() {

        licenseS = etLicenseNumber.getText().toString().trim();
//        validVehicleTypeLicenseS = etVehicleType.getText().toString();

        if (imagepath.isEmpty()) {
            Toast.makeText(activity, "Please select driving license image", Toast.LENGTH_SHORT).show();
        } else if (licenseS.isEmpty()) {
            Toast.makeText(activity, "Please enter driving license number", Toast.LENGTH_SHORT).show();
        } else if (validVehicleTypeLicenseS.isEmpty()) {
            Toast.makeText(activity, "Please enter driving license type", Toast.LENGTH_SHORT).show();
        } else if (issuedDateLicenseS.isEmpty()) {
            Toast.makeText(activity, "Please enter driving license issue date", Toast.LENGTH_SHORT).show();
        } else if (expiryDateLicenseS.isEmpty()) {
            Toast.makeText(activity, "Please enter driving license expiry date", Toast.LENGTH_SHORT).show();
        } else if (validVehicleTypeLicenseS.equalsIgnoreCase("-Select-")) {
            CommonUtils.showSnackbarAlert(spinnerVehicle, "select vehicle type");
        } else {

            App.getAppPreference().SaveString(ConstantData.DRIVER_LICENSE_IMAGE, imagepath);
            App.getAppPreference().SaveString(ConstantData.DRIVER_LICENSE_NUMBER, licenseS);
            App.getAppPreference().SaveString(ConstantData.DRIVER_VALID_VEHICLE_TYPE, validVehicleTypeLicenseS);
            App.getAppPreference().SaveString(ConstantData.DRIVER_LICENSE_ISSUE_DATE, issuedDateLicenseS);
            App.getAppPreference().SaveString(ConstantData.DRIVER_LICENSE_EXPIRY_DATE, expiryDateLicenseS);
            App.getAppPreference().SaveString(ConstantData.DRIVER_LICENSE_DETAILS, "1");

            finish();
        }
    }

    private void datePicker(final int dateType) {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Dialog,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String outputDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat mdformat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
                        String currentDate = mdformat.format(calendar.getTime());

                        if (dateType == 0) {
                            if (DateDifferentiate.compareDates(currentDate, outputDate) == 1) {
                                tvIssuedDate.setText(outputDate);
                                issuedDateLicenseS = outputDate;
                            } else {
                                Toast.makeText(activity, "Please select valid date", Toast.LENGTH_SHORT).show();
                            }
                        } else if (dateType == 1) {
                            if (DateDifferentiate.compareDates(currentDate, outputDate) == 2) {
                                tvExpirationDate.setText(outputDate);
                                expiryDateLicenseS = outputDate;
                            } else {
                                Toast.makeText(activity, "Please select valid date", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    //image path
    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_FROM_GALLERY);
    }

    private void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = this.getPackageManager();
        List<ResolveInfo> listcam = packageManager.queryIntentActivities(intent, 0);
        intent.setPackage(listcam.get(0).activityInfo.packageName);
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                uri = getImageUri(UploadDrivingLicenseActivity.this, bitmap);
                imagepath = getRealPathFromUri(uri);
                civLicense.setImageBitmap(bitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private Uri getImageUri(UploadDrivingLicenseActivity youractivity, Bitmap bitmap) {
        String path = MediaStore.Images.Media.insertImage(youractivity.getContentResolver(), bitmap, "Title", null);
        return Uri.parse(path);
    }

    private String getRealPathFromUri(Uri tempUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = this.getContentResolver().query(tempUri, proj, null, null, null);
            assert cursor != null;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FROM_GALLERY)
                onSelectFromGalleryResult(data);
            else if (requestCode == CAMERA_REQUEST)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        try {
            bitmap = (Bitmap) data.getExtras().get("data");
            uri = getImageUri(UploadDrivingLicenseActivity.this, bitmap);
            imagepath = getRealPathFromUri(uri);
            App.getAppPreference().SaveString(ConstantData.IMAGEPATH, imagepath);
            Glide.with(this).load("file://" + imagepath).into(civLicense);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
