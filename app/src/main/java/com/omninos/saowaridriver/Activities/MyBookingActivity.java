package com.omninos.saowaridriver.Activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.adapters.MyBookingAdapter;
import com.omninos.saowaridriver.model.MyTripsModel;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.MyTripsViewModel;

import java.util.ArrayList;
import java.util.List;

public class MyBookingActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerViewBooking;
    private Activity activity;
    private ImageView backButton;

    private MyTripsViewModel viewModel;
    private List<MyTripsModel.Detail> list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking);
        activity = MyBookingActivity.this;

        viewModel = ViewModelProviders.of(this).get(MyTripsViewModel.class);
        stopService(new Intent(MyBookingActivity.this, MySerives.class));
        Intent intent = new Intent(MyBookingActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        findIds();
        getList();
    }

    private void getList() {
        viewModel.myTrips(activity, App.getAppPreference().GetString(ConstantData.USERID)).observe(MyBookingActivity.this, new Observer<MyTripsModel>() {
            @Override
            public void onChanged(@Nullable MyTripsModel myTripsModel) {
                if (myTripsModel.getSuccess().equalsIgnoreCase("1")) {
                    list = myTripsModel.getDetails();
                    recyclerViewBooking.setAdapter(new MyBookingAdapter(activity,list));
                } else {
                    CommonUtils.showSnackbarAlert(backButton, myTripsModel.getMessage());
                }
            }
        });
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void findIds() {
        recyclerViewBooking = findViewById(R.id.recyclerview_mybooking);
        recyclerViewBooking.setLayoutManager(new LinearLayoutManager(activity));


        backButton = findViewById(R.id.back_btn);
        backButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
        }
    }
}
