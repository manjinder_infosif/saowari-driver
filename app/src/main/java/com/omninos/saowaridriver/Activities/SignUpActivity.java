package com.omninos.saowaridriver.Activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.CheckDriverPhoneModel;
import com.omninos.saowaridriver.model.DriverRegisterModel;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.services.LocationService;
import com.omninos.saowaridriver.viewmodels.DriverRegisterVM;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private TextView title;
    private Button signUp;
    private Activity activity;
    private EditText etUserName, etUserEmail;
    private TextView tvBirthDate;
    private RelativeLayout userImage;
    private static final int CAMERA_REQUEST = 200;
    private static final int PICK_FROM_GALLERY = 100;
    private Bitmap bitmap;
    private Uri uri;
    private String imagepath = "", path, strName, strEmail, strDateOfBirth, strGender;
    private CircleImageView civProfilePic;
    private RadioGroup radioGroupGender;
    private DriverRegisterVM driverRegisterVM;
    private String Gender = "", JobTime = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        driverRegisterVM = ViewModelProviders.of(this).get(DriverRegisterVM.class);
        initView();
        SetUp();
    }

    private void initView() {
        title = findViewById(R.id.title);
        signUp = findViewById(R.id.signUp);
        userImage = findViewById(R.id.userimg);
        civProfilePic = findViewById(R.id.iv_profilepic);
        activity = SignUpActivity.this;

        //edit texts
        etUserName = findViewById(R.id.et_username);
        etUserEmail = findViewById(R.id.et_useremail);

        tvBirthDate = findViewById(R.id.dateOfBirthData);

        radioGroupGender = findViewById(R.id.radio_grp_gender);
    }

    private void SetUp() {
        title.setText("Sign Up");
        signUp.setOnClickListener(this);
        tvBirthDate.setOnClickListener(this);
        userImage.setOnClickListener(this);

        radioGroupGender.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUp:
                validate();
                break;
            case R.id.dateOfBirthData:
                PickDate();
                break;
            case R.id.userimg:

                showPopup();
                break;
        }
    }

    private void validate() {
        strName = etUserName.getText().toString();
        strEmail = etUserEmail.getText().toString();
        strDateOfBirth = tvBirthDate.getText().toString();


        if (TextUtils.isEmpty(strName) || TextUtils.isEmpty(strEmail) || TextUtils.isEmpty(strDateOfBirth) || TextUtils.isEmpty(strGender)) {
            CommonUtils.showSnackbarAlert(etUserName, ConstantData.Fill_DETAILS);
        } else if (imagepath.isEmpty()) {
            CommonUtils.showSnackbarAlert(etUserName, "Please select a profile picture");
        } else {
            App.getAppPreference().SaveString(ConstantData.Name, strName);
            startLocationService();
        }

    }

    private void startLocationService() {
        if (!isMyServiceRunning(LocationService.class)) {
            startService(new Intent(activity, LocationService.class));
            getDriverRegisterApi();
        } else {
            getDriverRegisterApi();
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        assert manager != null;
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void getDriverRegisterApi() {
        RequestBody userName = RequestBody.create(MediaType.parse("Text/plain"), strName);
        RequestBody userEmail = RequestBody.create(MediaType.parse("Text/plain"), strEmail);
        RequestBody userBirthDate = RequestBody.create(MediaType.parse("Text/plain"), strDateOfBirth);
        RequestBody phoneNumber = RequestBody.create(MediaType.parse("Text/plain"), App.getSinltonPojo().getMobilenumber());
        RequestBody gender = RequestBody.create(MediaType.parse("Text/plain"), Gender);
        RequestBody deviceType = RequestBody.create(MediaType.parse("Text/plain"), "Android");
        RequestBody regId = RequestBody.create(MediaType.parse("Text/plain"), FirebaseInstanceId.getInstance().getToken());
        RequestBody jobTypeBody = RequestBody.create(MediaType.parse("Text/plain"), JobTime);
        RequestBody latitude = RequestBody.create(MediaType.parse("Text/plain"), String.valueOf(App.getSinltonPojo().getCurrentLatlng().latitude));
        RequestBody longitude = RequestBody.create(MediaType.parse("Text/plain"), String.valueOf(App.getSinltonPojo().getCurrentLatlng().longitude));
        RequestBody loginType = RequestBody.create(MediaType.parse("Text/plain"), "normal");
        File fileLicense = new File(imagepath);

        final RequestBody userImage = RequestBody.create(MediaType.parse("multipart/form-data"), fileLicense);
        MultipartBody.Part userProfile = MultipartBody.Part.createFormData("driverImage", fileLicense.getName(), userImage);
        driverRegisterVM.driverRegisterModel(activity, userName, phoneNumber, userEmail, userBirthDate, gender, deviceType, regId, latitude, longitude, loginType, userProfile, jobTypeBody).observe(this, new Observer<CheckDriverPhoneModel>() {
            @Override
            public void onChanged(@Nullable CheckDriverPhoneModel driverRegisterModel) {
                if (driverRegisterModel.getSuccess().equalsIgnoreCase("1")) {
                    App.getAppPreference().saveLoginDetail(driverRegisterModel);
                    App.getAppPreference().SaveString(ConstantData.USERID, driverRegisterModel.getDetails().getId());
                    Toast.makeText(activity, driverRegisterModel.getMessage(), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(SignUpActivity.this, SelectVehicleActivity.class));
                    finish();
                } else {
                    Toast.makeText(activity, driverRegisterModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void showPopup() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo"))
                    cameraIntent();
                else if (items[item].equals("Choose from Library"))
                    galleryIntent();
                else if (items[item].equals("Cancel"))
                    dialog.dismiss();
            }
        });
        builder.show();

    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = this.getPackageManager();
        List<ResolveInfo> listcam = packageManager.queryIntentActivities(intent, 0);
        intent.setPackage(listcam.get(0).activityInfo.packageName);
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FROM_GALLERY)
                onSelectFromGalleryResult(data);
            else if (requestCode == CAMERA_REQUEST)
                onCaptureImageResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        uri = getImageUri(this, bitmap);


        imagepath = getRealPathFromUri(uri);
        App.getAppPreference().SaveString(ConstantData.IMAGEPATH, imagepath);
        Glide.with(this).load("file://" + imagepath).into(civProfilePic);
    }

    private void onCaptureImageResult(Intent data) {
        try {
            bitmap = (Bitmap) data.getExtras().get("data");
            uri = getImageUri(SignUpActivity.this, bitmap);
            imagepath = getRealPathFromUri(uri);
            App.getAppPreference().SaveString(ConstantData.IMAGEPATH, imagepath);
            Glide.with(this).load("file://" + imagepath).into(civProfilePic);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private Uri getImageUri(SignUpActivity signUpActivity, Bitmap bitmap) {
        path = MediaStore.Images.Media.insertImage(signUpActivity.getContentResolver(), bitmap, "Title", null);
        return Uri.parse(path);
    }

    private String getRealPathFromUri(Uri tempUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = this.getContentResolver().query(tempUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void PickDate() {
        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(
                SignUpActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker,
                                  int selectedyear, int selectedmonth,
                                  int selectedday) {

                mcurrentDate.set(Calendar.YEAR, selectedyear);
                mcurrentDate.set(Calendar.MONTH, selectedmonth);
                mcurrentDate.set(Calendar.DAY_OF_MONTH,
                        selectedday);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                tvBirthDate.setText(sdf.format(mcurrentDate
                        .getTime()));
            }
        }, mYear, mMonth, mDay);

        mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDatePicker.setTitle("Date of Birth");
        mDatePicker.show();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        // This will get the radiobutton that has changed in its check state
        RadioButton checkedRadioButton = group.findViewById(checkedId);
        // This puts the value (true/false) into the variable
        boolean isChecked = checkedRadioButton.isChecked();
        // If the radiobutton that has changed in check state is now checked...
        if (isChecked) {
            // Changes the textview's text to "Checked: example radiobutton text"
            strGender = checkedRadioButton.toString();
        }
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.maleRadio:
                if (checked)
                    Gender = "Male";
                break;
            case R.id.femaleRadio:
                if (checked)
                    Gender = "Female";
                break;

            case R.id.otherRadio:
                if (checked) {
                    Gender = "Other";
                }
                break;
        }

    }

    public void SelectJobType(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.halfTime:
                if (checked)
                    JobTime = "0";
                break;
            case R.id.fullTime:
                if (checked)
                    JobTime = "1";
                break;
        }
    }
}
