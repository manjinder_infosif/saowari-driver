package com.omninos.saowaridriver.Activities;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.JobFlowViewModel;

import java.util.Map;

public class JobRatingActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    Activity activity;

    private GoogleMap map;

    private ImageView imageRating;
    private TextView ratingTextName, ratingAmount;
    private RatingBar ratingCustomer;
    private EditText ratingReviewET;

    private String ratingReviewS = "1";

    private JobFlowViewModel jobFlowViewModel;

    String commentS = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_rating);

        findId();

    }

    private void findId() {

        activity = JobRatingActivity.this;

        jobFlowViewModel = ViewModelProviders.of(this).get(JobFlowViewModel.class);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapRatingJob);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        imageRating = findViewById(R.id.imageRating);
        ratingTextName = findViewById(R.id.ratingTextName);
        ratingReviewET = findViewById(R.id.ratingReviewET);

        ratingAmount = findViewById(R.id.ratingAmount);

        ratingCustomer = findViewById(R.id.ratingCustomer);

        Button submitRating = findViewById(R.id.submitRating);
        submitRating.setOnClickListener(this);

        setPassengerDetails();

    }

    private void setPassengerDetails() {

        String name = App.getAppPreference().GetString(ConstantData.JOB_CUSTOMER_NAME);
        String image = App.getAppPreference().GetString(ConstantData.JOB_CUSTOMER_IMAGE);
        ratingAmount.setText("Total Amount ৳" + App.getAppPreference().GetString(ConstantData.JOB_TOTAL_AMOUNT));

        Glide.with(activity).load(image).into(imageRating);
        ratingTextName.setText("How was trip with " + name);

        ratingCustomer.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingReviewS = String.valueOf((int) Math.ceil(ratingBar.getRating()));
            }
        });
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.submitRating:
                ratingToUserApi();
                break;
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;

        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        double lat = Double.parseDouble(App.getAppPreference().GetString(ConstantData.CURRENT_LAT));
        double lon = Double.valueOf(App.getAppPreference().GetString(ConstantData.CURRENT_LONG));

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 14));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        map.setMyLocationEnabled(false);

    }


    private void ratingToUserApi() {

        String jobId = App.getAppPreference().GetString(ConstantData.JOB_ID);
        String driverId = App.getAppPreference().GetString(ConstantData.USERID);
        String userId = App.getAppPreference().GetString(ConstantData.JOB_CUSTOMER_ID);

        commentS = ratingReviewET.getText().toString().trim();

        jobFlowViewModel.ratingToUser(activity, jobId, driverId, userId, commentS, ratingReviewS).observe(this, new Observer<Map>() {
            @Override
            public void onChanged(@Nullable Map driverRatingToUserPojo) {
                if (driverRatingToUserPojo.get("success").toString().equalsIgnoreCase("1")) {
                    startActivity(new Intent(activity, HomeActivity.class));
                    CommonUtils.clearJobDetails();
                    finishAffinity();
                } else {
                    Toast.makeText(activity, "Unable to Rate Driver", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

}
