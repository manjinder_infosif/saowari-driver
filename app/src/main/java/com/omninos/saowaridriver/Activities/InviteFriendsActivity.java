package com.omninos.saowaridriver.Activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;

public class InviteFriendsActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView backButton;
    private Button inviteButton;
    private TextView code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);


        stopService(new Intent(InviteFriendsActivity.this, MySerives.class));
        Intent intent = new Intent(InviteFriendsActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        findIds();
        setUps();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void findIds() {
        backButton = findViewById(R.id.back_btn);
        inviteButton = findViewById(R.id.invite_btn);
        code = findViewById(R.id.code);
    }

    private void setUps() {
        backButton.setOnClickListener(this);
        inviteButton.setOnClickListener(this);
        code.setText(App.getAppPreference().getLoginDetail().getDetails().getReferralCode());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;

            case R.id.invite_btn:
                break;
        }

    }
}
