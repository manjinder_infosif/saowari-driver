package com.omninos.saowaridriver.Activities;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.JobFlowPojo;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.JobFlowViewModel;

public class RideRequestActivity extends AppCompatActivity implements View.OnClickListener {

    private Activity activity;

    private ImageView imageRideRequest;
    private TextView textTimer, nameRideRequest, pickAddressRideRequest, dropAddressRideRequest, distanceRideRequest,
            amountRideRequest;
    private ProgressBar barTimer;
    private RatingBar ratingRideRequest;

    private JobFlowViewModel jobFlowViewModel;

    MediaPlayer mp;
    private boolean data = false;


    private int autoCancelJob = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_request);

        mp = MediaPlayer.create(this, R.raw.notification);
        mp.start();
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                if (data) {
                    mp.stop();
                } else {
                    mp.start();
                }
            }
        });

        findIds();
    }

    @Override
    protected void onStop() {
        super.onStop();
        data = true;
        mp.stop();
    }

    private void findIds() {

        activity = RideRequestActivity.this;

        jobFlowViewModel = ViewModelProviders.of(this).get(JobFlowViewModel.class);

//        ImageView appbarBack = findViewById(R.id.appbarBack);
//        appbarBack.setVisibility(View.GONE);
//
//        TextView appbarText = findViewById(R.id.appbarText);
//        appbarText.setText(R.string.ride_request);

        barTimer = findViewById(R.id.barTimer);
        textTimer = findViewById(R.id.textTimer);

        ratingRideRequest = findViewById(R.id.ratingRideRequest);
        imageRideRequest = findViewById(R.id.imageRideRequest);
        nameRideRequest = findViewById(R.id.nameRideRequest);

        pickAddressRideRequest = findViewById(R.id.pickAddressRideRequest);
        dropAddressRideRequest = findViewById(R.id.dropAddressRideRequest);

        distanceRideRequest = findViewById(R.id.distanceRideRequest);
        amountRideRequest = findViewById(R.id.amountRideRequest);

        Button cancelJobRideRequest = findViewById(R.id.cancelJobRideRequest);
        cancelJobRideRequest.setOnClickListener(this);

        Button confirmJobRideRequest = findViewById(R.id.confirmJobRideRequest);
        confirmJobRideRequest.setOnClickListener(this);

        setJobDetails();

        startTimer();

    }

    private void startTimer() {

        new CountDownTimer(30 * 1000, 500) {
            @Override
            public void onTick(long leftTimeInMilliseconds) {
                long seconds = leftTimeInMilliseconds / 1000;
                barTimer.setProgress((int) seconds);
                textTimer.setText(String.format("%02d", seconds / 60) + ":" + String.format("%02d", seconds % 60));
                // format the textview to show the easily readable format
            }

            @Override
            public void onFinish() {
                if (textTimer.getText().equals("00:00")) {
                    if (autoCancelJob == 1) {
                        acceptJobApi("2");
                    }
                } else {
                    textTimer.setText("00:30");
                    barTimer.setProgress(100);
                }
            }
        }.start();
    }

    private void setJobDetails() {

        String name = App.getAppPreference().GetString(ConstantData.JOB_CUSTOMER_NAME);
        String image = App.getAppPreference().GetString(ConstantData.JOB_CUSTOMER_IMAGE);
        String rating = App.getAppPreference().GetString(ConstantData.JOB_CUSTOMER_RATING);

        String pickupAddress = App.getAppPreference().GetString(ConstantData.JOB_PICK_ADDRESS);
        String dropAddress = App.getAppPreference().GetString(ConstantData.JOB_DROP_ADDRESS);

        String distance = App.getAppPreference().GetString(ConstantData.JOB_TOTAL_DISTANCE);
        String amount = App.getAppPreference().GetString(ConstantData.JOB_TOTAL_AMOUNT);

        if (!name.isEmpty()) {
            nameRideRequest.setText(name);
        }
        if (!rating.isEmpty()) {
            ratingRideRequest.setRating(Float.parseFloat(rating));
        }
        if (!pickupAddress.isEmpty()) {
            pickAddressRideRequest.setText(pickupAddress);
        }
        if (!dropAddress.isEmpty()) {
            dropAddressRideRequest.setText(dropAddress);
        }
        if (!distance.isEmpty()) {
            distanceRideRequest.setText(distance);
        }
        if (!amount.isEmpty()) {
            amountRideRequest.setText("৳ " + amount);
        }
        if (!image.isEmpty()) {
            Glide.with(activity).load(image).into(imageRideRequest);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.cancelJobRideRequest:
                acceptJobApi("2");
                break;

            case R.id.confirmJobRideRequest:
                acceptJobApi("1");
                break;
        }

    }

    private void acceptJobApi(final String jobStatus) {

        String jobId = App.getAppPreference().GetString(ConstantData.JOB_ID);

        jobFlowViewModel.acceptRejectJob(activity, jobStatus, jobId).observe(this,
                new Observer<JobFlowPojo>() {
                    @Override
                    public void onChanged(@Nullable JobFlowPojo jobFlowPojo) {
                        if (jobFlowPojo.getSuccess().equalsIgnoreCase("1")) {
                            if (jobStatus.equalsIgnoreCase("2")) {
                                CommonUtils.clearJobDetails();
                                startActivity(new Intent(activity, HomeActivity.class));
                                finish();
                                autoCancelJob = 0;
                            } else if (jobStatus.equalsIgnoreCase("1")) {
                                autoCancelJob = 0;
                                startActivity(new Intent(activity, JobArrivingActivity.class));
                                finish();
                            }
                        } else {
                            Toast.makeText(activity, jobFlowPojo.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {

    }
}
