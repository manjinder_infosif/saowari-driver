package com.omninos.saowaridriver.Activities;

import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.adapters.MonthlyStatementAdapter;
import com.omninos.saowaridriver.model.DriverEarningViewModel;
import com.omninos.saowaridriver.model.StatementModelClass;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;

public class MonthlyStatementActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private MonthlyStatementAdapter adapter;
    private ImageView firstIcon;
    private TextView title;
    private DriverEarningViewModel viewModel;
    private String Type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthly_statement);

        viewModel = ViewModelProviders.of(this).get(DriverEarningViewModel.class);

        Type = getIntent().getStringExtra("Type");

        stopService(new Intent(MonthlyStatementActivity.this, MySerives.class));
        Intent intent = new Intent(MonthlyStatementActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }
        initView();
        SetUp();
        getList();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void getList() {
        viewModel.statementModelClassLiveData(MonthlyStatementActivity.this, Type, App.getAppPreference().GetString(ConstantData.USERID)).observe(MonthlyStatementActivity.this, new Observer<StatementModelClass>() {
            @Override
            public void onChanged(@Nullable StatementModelClass statementModelClass) {
                if (statementModelClass.getSuccess().equalsIgnoreCase("1")) {
                    if (statementModelClass.getDetails() != null) {
                        adapter = new MonthlyStatementAdapter(MonthlyStatementActivity.this, statementModelClass.getDetails());
                        recyclerView.setAdapter(adapter);
                    }
                } else {
                    CommonUtils.showSnackbarAlert(recyclerView, statementModelClass.getMessage());
                }
            }
        });
    }

    private void initView() {
        recyclerView = findViewById(R.id.monthlyStatementRC);
        title = findViewById(R.id.title);
        firstIcon = findViewById(R.id.firstIcon);
    }

    private void SetUp() {
        if (Type.equalsIgnoreCase("monthly")){
            title.setText("Monthly Statements");
        }else if (Type.equalsIgnoreCase("weekly")){
            title.setText("Weekly Statements");
        }else if (Type.equalsIgnoreCase("daily")){
            title.setText("Daily Statements");
        }

        firstIcon.setImageDrawable(getDrawable(R.drawable.ic_back));
        firstIcon.setOnClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MonthlyStatementActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.firstIcon:
                onBackPressed();
                break;
        }
    }
}
