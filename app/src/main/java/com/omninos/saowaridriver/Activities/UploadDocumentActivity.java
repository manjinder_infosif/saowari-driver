package com.omninos.saowaridriver.Activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.VehicleRegisterModel;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.VehicleRegisterVM;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UploadDocumentActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView backButton;
    private Button continueButton;
    private Activity activity;
    private VehicleRegisterVM vehicleRegisterVM;

    private ImageView okCheckDriverLicense, okCheckVehicleInsurance, okCheckVehiclePermit, okCheckVehicleRegistration;
    private RelativeLayout licenseBtn, vehicleInsuranceBtn, vehiclePermitBtn, vehicleRegistrationBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_document);
        activity = UploadDocumentActivity.this;


        stopService(new Intent(UploadDocumentActivity.this, MySerives.class));
        Intent intent = new Intent(UploadDocumentActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        vehicleRegisterVM = ViewModelProviders.of(this).get(VehicleRegisterVM.class);
        findIds();
        setUps();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void findIds() {
        continueButton = findViewById(R.id.continue_btn);

        //imageviews
        backButton = findViewById(R.id.back_btn);
        okCheckDriverLicense = findViewById(R.id.okCheckDriverLicense);
        okCheckVehicleInsurance = findViewById(R.id.okCheckVehicleInsurance);
        okCheckVehiclePermit = findViewById(R.id.okCheckVehiclePermit);
        okCheckVehicleRegistration = findViewById(R.id.okCheckVehicleRegistration);

        //relative layouts
        licenseBtn = findViewById(R.id.licenseBtn);
        vehicleInsuranceBtn = findViewById(R.id.vehicleInsuranceBtn);
        vehiclePermitBtn = findViewById(R.id.vehiclePermitBtn);
        vehicleRegistrationBtn = findViewById(R.id.vehicleRegistrationBtn);

    }

    private void setUps() {
        backButton.setOnClickListener(this);
        continueButton.setOnClickListener(this);

        licenseBtn.setOnClickListener(this);
        vehicleInsuranceBtn.setOnClickListener(this);
        vehiclePermitBtn.setOnClickListener(this);
        vehicleRegistrationBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continue_btn:
                validation();
                break;

            case R.id.back_btn:
                finish();
                break;

            case R.id.licenseBtn:
                startActivity(new Intent(activity, UploadDrivingLicenseActivity.class));
                break;

            case R.id.vehicleInsuranceBtn:
                startActivity(new Intent(activity, VehicleInsuranceActivity.class));
                break;

            case R.id.vehiclePermitBtn:
                startActivity(new Intent(activity, VehiclePermitActivity.class));
                break;

            case R.id.vehicleRegistrationBtn:
                startActivity(new Intent(activity, VehicleRegistrationActivity.class));
                break;
        }
    }

    private void validation() {
        String driverLicenseDetails = App.getAppPreference().GetString(ConstantData.DRIVER_LICENSE_DETAILS);
        String driverVehicleInsuranceDetails = App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_INSURANCE_DETAILS);
        String driverVehiclePermitDetails = App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_PERMIT_DETAILS);
        String driverVehicleRegistrationDetails = App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_REGISTRATION_DETAILS);

        if (!driverLicenseDetails.equalsIgnoreCase("1")) {
            Toast.makeText(activity, "Please enter license details", Toast.LENGTH_SHORT).show();
        } else if (!driverVehicleInsuranceDetails.equalsIgnoreCase("1")) {
            Toast.makeText(activity, "Please enter vehicle insurance details", Toast.LENGTH_SHORT).show();
        } else if (!driverVehiclePermitDetails.equalsIgnoreCase("1")) {
            Toast.makeText(activity, "Please enter vehicle permit details", Toast.LENGTH_SHORT).show();
        } else if (!driverVehicleRegistrationDetails.equalsIgnoreCase("1")) {
            Toast.makeText(activity, "Please enter vehicle registration details", Toast.LENGTH_SHORT).show();
        } else {

            RequestBody driverIdR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.USERID));
            RequestBody vehicleBrandR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.VEHICLE_BRAND));
            RequestBody vehicleModelR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.VEHICLE_MODEL));
            RequestBody vehicalYearR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.VEHICLE_YEAR));
            RequestBody vehicleColorR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.VEHICLE_COLOR));
            RequestBody vehiclePlateNumberR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.VEHICLE_PLATE));
            RequestBody driverLicenseNumberR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.DRIVER_LICENSE_NUMBER));
            RequestBody driverVehicleTypeR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.DRIVER_VALID_VEHICLE_TYPE));
            RequestBody driverLicenceStartDateR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.DRIVER_LICENSE_ISSUE_DATE));
            RequestBody driverLicenseExpiryR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.DRIVER_LICENSE_EXPIRY_DATE));
            RequestBody vehicleInsurenceNumberR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_INSURANCE_NUMBER));
            RequestBody vehicleInsurenceStartDateR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_INSURANCE_ISSUE_DATE));
            RequestBody vehicleInsurenceExpieryR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_INSURANCE_EXPIRY_DATE));
            RequestBody vehiclePermitNumberR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_PERMIT_NUMBER));
            RequestBody vehiclePermitStartDateR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_PERMIT_ISSUE_DATE));
            RequestBody vehiclePermitExpiryDateR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_PERMIT_EXPIRY_DATE));
            RequestBody vehicleRegistartionNumberR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_REGISTRATION_NUMBER));
            RequestBody vehicleRegistartionDateR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_REGISTRATION_DATE));
            RequestBody vehicleRegistartionValidDateR = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_REGISTRATION_VALID_DATE));
            RequestBody typeBody = RequestBody.create(MediaType.parse("Text/plain"), App.getAppPreference().GetString(ConstantData.VEHICLE_TYPE));

            File fileLicense = new File(App.getAppPreference().GetString(ConstantData.DRIVER_LICENSE_IMAGE));
            final RequestBody driverLicenseImageR = RequestBody.create(MediaType.parse("multipart/form-data"), fileLicense);
            MultipartBody.Part driverLicenseImage = MultipartBody.Part.createFormData("driverLicenseImage", fileLicense.getName(), driverLicenseImageR);

            File fileRegistration = new File(App.getAppPreference().GetString(ConstantData.VEHICALE_NEW_REGISTER_IMAGE));
            final RequestBody vehicleRegistartionImageR = RequestBody.create(MediaType.parse("multipart/form-data"), fileRegistration);
            MultipartBody.Part vehicleRegistartionImage = MultipartBody.Part.createFormData("vehicleRegistartionImage", fileRegistration.getName(), vehicleRegistartionImageR);

            File fileVehicle = new File(App.getAppPreference().GetString(ConstantData.VEHICAL_REGISTRARTION_IMAGE));
            final RequestBody vehicleImageR = RequestBody.create(MediaType.parse("multipart/form-data"), fileVehicle);
            MultipartBody.Part vehicleImage = MultipartBody.Part.createFormData("vehicleImage", fileVehicle.getName(), vehicleImageR);

            File fileInsurance = new File(App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_INSURANCE_IMAGE));
            final RequestBody vehicleInsurenceImageR = RequestBody.create(MediaType.parse("multipart/form-data"), fileInsurance);
            MultipartBody.Part vehicleInsurenceImage = MultipartBody.Part.createFormData("vehicleInsurenceImage", fileInsurance.getName(), vehicleInsurenceImageR);

            File filePermit = new File(App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_PERMIT_IMAGE));
            final RequestBody vehiclePermitImageR = RequestBody.create(MediaType.parse("multipart/form-data"), filePermit);
            MultipartBody.Part vehiclePermitImage = MultipartBody.Part.createFormData("vehiclePermitImage", filePermit.getName(), vehiclePermitImageR);


            vehicleRegisterVM.vehicleRegister(activity, vehicleBrandR, vehicleModelR, vehicalYearR,
                    vehicleColorR, vehiclePlateNumberR, driverLicenseNumberR, driverVehicleTypeR, driverLicenceStartDateR,
                    driverLicenseExpiryR, vehicleInsurenceNumberR, vehicleInsurenceStartDateR,
                    vehicleInsurenceExpieryR, vehiclePermitNumberR, vehiclePermitStartDateR,
                    vehiclePermitExpiryDateR, vehicleRegistartionNumberR, vehicleRegistartionDateR,
                    vehicleRegistartionValidDateR, driverLicenseImage, vehicleRegistartionImage,
                    vehicleImage, vehicleInsurenceImage, vehiclePermitImage, driverIdR, typeBody).observe(this, new Observer<VehicleRegisterModel>() {
                @Override
                public void onChanged(@Nullable VehicleRegisterModel vehicleRegisterModel) {
                    if (vehicleRegisterModel.getSuccess().equalsIgnoreCase("1")) {
                        Intent i = new Intent(activity, AppointmentConfirmationActivity.class);
                        i.putExtra("date", vehicleRegisterModel.getDetails().getAppointmentDate());
                        i.putExtra("time", vehicleRegisterModel.getDetails().getAppointmentTime());
                        i.putExtra("address", vehicleRegisterModel.getDetails().getAppointmentAddress());
                        i.putExtra("status", "1");
                        startActivity(i);
//                        startActivity(new Intent(activity, AppointmentConfirmationActivity.class));
                    } else {
                        Toast.makeText(activity, vehicleRegisterModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        String driverLicenseDetails = App.getAppPreference().GetString(ConstantData.DRIVER_LICENSE_DETAILS);
        String driverVehicleInsuranceDetails = App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_INSURANCE_DETAILS);
        String driverVehiclePermitDetails = App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_PERMIT_DETAILS);
        String driverVehicleRegistrationDetails = App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_REGISTRATION_DETAILS);

        if (driverLicenseDetails.equalsIgnoreCase("1")) {
            okCheckDriverLicense.setImageResource(R.drawable.ic_tick_app_color);
        } else {
            okCheckDriverLicense.setImageResource(R.drawable.ccp_down_arrow);
        }
        if (driverVehicleInsuranceDetails.equalsIgnoreCase("1")) {
            okCheckVehicleInsurance.setImageResource(R.drawable.ic_tick_app_color);
        } else {
            okCheckVehicleInsurance.setImageResource(R.drawable.ccp_down_arrow);
        }
        if (driverVehiclePermitDetails.equalsIgnoreCase("1")) {
            okCheckVehiclePermit.setImageResource(R.drawable.ic_tick_app_color);
        } else {
            okCheckVehiclePermit.setImageResource(R.drawable.ccp_down_arrow);
        }
        if (driverVehicleRegistrationDetails.equalsIgnoreCase("1")) {
            okCheckVehicleRegistration.setImageResource(R.drawable.ic_tick_app_color);
        } else {
            okCheckVehicleRegistration.setImageResource(R.drawable.ccp_down_arrow);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        App.getAppPreference().SaveString(ConstantData.DRIVER_LICENSE_DETAILS, "0");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_INSURANCE_DETAILS, "0");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_PERMIT_DETAILS, "0");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_REGISTRATION_DETAILS, "0");
        App.getAppPreference().SaveString(ConstantData.DRIVER_LICENSE_IMAGE, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_LICENSE_NUMBER, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VALID_VEHICLE_TYPE, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_LICENSE_ISSUE_DATE, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_LICENSE_EXPIRY_DATE, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_INSURANCE_IMAGE, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_INSURANCE_NUMBER, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_INSURANCE_ISSUE_DATE, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_INSURANCE_EXPIRY_DATE, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_PERMIT_IMAGE, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_PERMIT_NUMBER, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_PERMIT_ISSUE_DATE, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_PERMIT_EXPIRY_DATE, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_REGISTRATION_IMAGE, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_REGISTRATION_NUMBER, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_REGISTRATION_DATE, "");
        App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_REGISTRATION_VALID_DATE, "");
        App.getAppPreference().SaveString(ConstantData.VEHICALE_NEW_REGISTER_IMAGE, "");
        App.getAppPreference().SaveString(ConstantData.VEHICAL_REGISTRARTION_IMAGE, "");

    }
}

