package com.omninos.saowaridriver.Activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.omninos.saowaridriver.BuildConfig;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.CheckDriverPhoneModel;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.util.ImageUtil;
import com.omninos.saowaridriver.viewmodels.DriverRegisterVM;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView backButton, editAbout, editAddress, editPhone, editEmail;
    private EditText etAbout, etAddress, etPhone, etEmail, et_name;
    private String name, address, number, email;
    private Button saveButton;
    private CircleImageView driverEditImage;
    private EditProfileActivity activity;

    private File photoFile;
    private static final int GALLERY_REQUEST = 101;

    private static final int CAMERA_REQUEST = 102;

    private Uri uri;
    private String UserimagePath = "";
    private DriverRegisterVM viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        activity = EditProfileActivity.this;

        viewModel = ViewModelProviders.of(this).get(DriverRegisterVM.class);

        stopService(new Intent(EditProfileActivity.this, MySerives.class));
        Intent intent = new Intent(EditProfileActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }
        findIds();
        setUps();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void setUps() {
        backButton.setOnClickListener(this);
        editAbout.setOnClickListener(this);
        editAddress.setOnClickListener(this);
        editPhone.setOnClickListener(this);
        editEmail.setOnClickListener(this);
        saveButton.setOnClickListener(this);

        driverEditImage.setOnClickListener(this);

        Glide.with(activity).load(App.getAppPreference().getLoginDetail().getDetails().getDriverImage()).into(driverEditImage);
        et_name.setText(App.getAppPreference().getLoginDetail().getDetails().getDriverName());
        etAddress.setText(App.getAppPreference().getLoginDetail().getDetails().getAddress());
        etEmail.setText(App.getAppPreference().getLoginDetail().getDetails().getDriverEmail());
        etPhone.setText(App.getAppPreference().getLoginDetail().getDetails().getDriverPhoneNumber());


    }

    private void findIds() {
        //imageviews
        backButton = findViewById(R.id.back_btn);
        editAbout = findViewById(R.id.edit_about);
        editAddress = findViewById(R.id.edit_address);
        editPhone = findViewById(R.id.edit_phone);
        editEmail = findViewById(R.id.edit_email);
        //edit texts
        etAbout = findViewById(R.id.et_about);
        etAddress = findViewById(R.id.et_address);
        etPhone = findViewById(R.id.et_phone);
        etEmail = findViewById(R.id.et_email);
        et_name = findViewById(R.id.et_name);
        driverEditImage = findViewById(R.id.driverEditImage);

        //buttons
        saveButton = findViewById(R.id.save_btn);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.edit_about:
                break;
            case R.id.edit_address:
                break;
            case R.id.edit_phone:
                break;
            case R.id.edit_email:
                break;
            case R.id.save_btn:
                SaveProfile();
                break;
            case R.id.driverEditImage:
                OpenImageData();
                break;
        }
    }

    private void OpenImageData() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void cameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            photoFile = null;
            photoFile = ImageUtil.getTemporaryCameraFile();
            if (photoFile != null) {
                Uri uri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private void galleryIntent() {

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == CAMERA_REQUEST) {
                UserimagePath = ImageUtil.compressImage(photoFile.getAbsolutePath());
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                Bitmap mBitmapInsurance = BitmapFactory.decodeFile(photoFile.getAbsolutePath(), options);
//                userImage.setImageBitmap(mBitmapInsurance);
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                Bitmap rotated = Bitmap.createBitmap(mBitmapInsurance, 0, 0, mBitmapInsurance.getWidth(), mBitmapInsurance.getHeight(),
                        matrix, true);

                if (Build.VERSION.SDK_INT > 25 && Build.VERSION.SDK_INT < 27) {
                    // Do something for lollipop and above versions
                    driverEditImage.setImageBitmap(rotated);
                } else {
                    // do something for phones running an SDK before lollipop
                    driverEditImage.setImageBitmap(mBitmapInsurance);
                }
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            uri = data.getData();
            if (uri != null) {
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                int column_index_data = cursor.getColumnIndex(projection[0]);
                cursor.moveToFirst();
                UserimagePath = cursor.getString(column_index_data);
                Glide.with(activity).load("file://" + UserimagePath).into(driverEditImage);
            }

        }
    }

    private void SaveProfile() {
        MultipartBody.Part user_image;
        name = et_name.getText().toString();
        address = etAddress.getText().toString();
        number = etPhone.getText().toString();
        email = etEmail.getText().toString();
        if (name.isEmpty()) {
            CommonUtils.showSnackbarAlert(et_name, "enter name");
        } else if (address.isEmpty()) {
            CommonUtils.showSnackbarAlert(etAddress, "enter address");
        } else if (number.isEmpty()) {
            CommonUtils.showSnackbarAlert(etPhone, "enter mobile number");
        } else if (email.isEmpty()) {
            CommonUtils.showSnackbarAlert(etEmail, "enter email");
        } else {

            RequestBody nameBody = RequestBody.create(MediaType.parse("text/plain"), name);
            RequestBody addressBody = RequestBody.create(MediaType.parse("text/plain"), address);
            RequestBody numberBody = RequestBody.create(MediaType.parse("text/plain"), number);
            RequestBody emailBody = RequestBody.create(MediaType.parse("text/palin"), email);
            RequestBody driverIdBody = RequestBody.create(MediaType.parse("text/plain"), App.getAppPreference().GetString(ConstantData.USERID));


            if (!UserimagePath.isEmpty()) {
                File file = new File(UserimagePath);
                final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                user_image = MultipartBody.Part.createFormData("driverImage", file.getName(), requestFile);

            } else {
                final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                user_image = MultipartBody.Part.createFormData("driverImage", "", requestFile);

            }

            viewModel.updatedriverProfile(activity, nameBody, addressBody, numberBody, emailBody, driverIdBody, user_image).observe(activity, new Observer<CheckDriverPhoneModel>() {
                @Override
                public void onChanged(@Nullable CheckDriverPhoneModel checkDriverPhoneModel) {
                    if (checkDriverPhoneModel.getSuccess().equalsIgnoreCase("1")) {
                        CommonUtils.showSnackbarAlert(et_name, checkDriverPhoneModel.getMessage());
                        App.getAppPreference().saveLoginDetail(checkDriverPhoneModel);
                        onBackPressed();
                    } else {
                        CommonUtils.showSnackbarAlert(et_name, checkDriverPhoneModel.getMessage());
                    }
                }
            });
        }
    }
}
