package com.omninos.saowaridriver.Activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.adapters.PersonalChatAdapter;
import com.omninos.saowaridriver.model.ChatModelClass;
import com.omninos.saowaridriver.model.GetConversionModel;
import com.omninos.saowaridriver.model.SendMessageModel;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.MessegeViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView backButton, sendButton, cameraButton, attachButton;
    private Activity activity;
    private RecyclerView recyclerViewChat;
    private TimerTask timerTask;
    private Timer timer;
    private String driverId, userId, Status;
    private MessegeViewModel viewModel;
    private TextView userNameData;
    private EditText et_message;
    private RelativeLayout messageRL;
    private List<ChatModelClass> list = new ArrayList<>();
    private PersonalChatAdapter adapter;
    private ProgressBar progress_circular;

    private TextView noData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);


        activity = ChatActivity.this;

        viewModel = ViewModelProviders.of(this).get(MessegeViewModel.class);

        stopService(new Intent(ChatActivity.this, MySerives.class));
        Intent intent = new Intent(ChatActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        userId = getIntent().getStringExtra("userId");
        driverId = App.getAppPreference().GetString(ConstantData.USERID);
        Status = getIntent().getStringExtra("status");
        findIds();
        setUps();

        startTimer();

    }

    private void startTimer() {

        //set a new Timer
        timer = new Timer();
        //initialize the TimerTask's job
        initializeTimerTask();
        //schedule the timer, to wake up every 2 second
        timer.schedule(timerTask, 2000, 5000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                getConversationApi();

            }
        };

    }

    private void getConversationApi() {
        viewModel.getConversionModelLiveData(ChatActivity.this, driverId, userId).observe(ChatActivity.this, new Observer<GetConversionModel>() {
            @Override
            public void onChanged(@Nullable GetConversionModel getConversionModel) {
                if (getConversionModel.getSuccess().equalsIgnoreCase("1")) {
                    progress_circular.setVisibility(View.GONE);

                    noData.setVisibility(View.GONE);
                    recyclerViewChat.setVisibility(View.VISIBLE);
                    if (getConversionModel.getMessageDetails() != null) {
                        adapter = new PersonalChatAdapter(activity, getConversionModel.getMessageDetails());
                        recyclerViewChat.setAdapter(adapter);
                        int newMsgPosition = getConversionModel.getMessageDetails().size() - 1;
                        adapter.notifyItemInserted(newMsgPosition);
                        recyclerViewChat.scrollToPosition(newMsgPosition);
                    } else {

                        noData.setVisibility(View.VISIBLE);
                        recyclerViewChat.setVisibility(View.GONE);
                        progress_circular.setVisibility(View.GONE);
//                        CommonUtils.showSnackbarAlert(backButton, "No conversation");
                    }
                } else {
                    noData.setVisibility(View.VISIBLE);
                    recyclerViewChat.setVisibility(View.GONE);
                    progress_circular.setVisibility(View.GONE);
//                    CommonUtils.showSnackbarAlert(backButton, getConversionModel.getMessage());
                }
            }
        });
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void setUps() {
        backButton.setOnClickListener(this);
        sendButton.setOnClickListener(this);
        cameraButton.setOnClickListener(this);
        attachButton.setOnClickListener(this);

        userNameData.setText(getIntent().getStringExtra("Title"));
        recyclerViewChat.setLayoutManager(new LinearLayoutManager(activity));


    }

    private void findIds() {
        backButton = findViewById(R.id.back_btn);
        sendButton = findViewById(R.id.sendInbox);
        cameraButton = findViewById(R.id.cameraInbox);
        attachButton = findViewById(R.id.attachmentInbox);
        recyclerViewChat = findViewById(R.id.rv_PersonalChat);
        userNameData = findViewById(R.id.userNameData);
        et_message = findViewById(R.id.et_message);
        messageRL = findViewById(R.id.messageRL);
        noData = findViewById(R.id.noData);

        progress_circular = findViewById(R.id.progress_circular);

        if (Status.equalsIgnoreCase("6")) {
            messageRL.setVisibility(View.INVISIBLE);
        } else {
            messageRL.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                onBackPressed();
                break;
            case R.id.sendInbox:
                sendMessage();
                break;
            case R.id.cameraInbox:
                break;
            case R.id.attachmentInbox:
                break;
        }
    }

    private void sendMessage() {
        String data = et_message.getText().toString();
        if (data.isEmpty()) {
            CommonUtils.showSnackbarAlert(et_message, "enter message");
        } else {
            viewModel.message(ChatActivity.this, driverId, userId, data).observe(ChatActivity.this, new Observer<SendMessageModel>() {
                @Override
                public void onChanged(@Nullable SendMessageModel sendMessageModel) {
                    if (sendMessageModel.getSuccess().equalsIgnoreCase("1")) {
                        et_message.setText("");
                    } else {
                        CommonUtils.showSnackbarAlert(et_message, sendMessageModel.getMessage());
                    }
                }
            });

        }
    }
}
