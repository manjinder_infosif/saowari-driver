package com.omninos.saowaridriver.Activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.omninos.saowaridriver.R;

public class SelectLanguageActivity extends AppCompatActivity implements View.OnClickListener {


    private Button nextButton;
    private RelativeLayout banglaLng, englishLng, nepaliLng;
    private TextView hintText;
    private ImageView banglaImg, englishImg, nepaliImg;
    private String Type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);



        initView();
        SetUp();
    }



    private void initView() {
        nextButton = findViewById(R.id.nextButton);
        banglaLng = findViewById(R.id.banglaLng);
        englishLng = findViewById(R.id.englishLng);
        nepaliLng = findViewById(R.id.nepaliLng);

        hintText = findViewById(R.id.hintText);

        banglaImg = findViewById(R.id.banglaImg);
        englishImg = findViewById(R.id.englishImg);
        nepaliImg = findViewById(R.id.nepaliImg);

        Type = getIntent().getStringExtra("Type");
    }

    private void SetUp() {
        nextButton.setOnClickListener(this);
        banglaLng.setOnClickListener(this);
        englishLng.setOnClickListener(this);
        nepaliLng.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nextButton:
                if (Type.equalsIgnoreCase("1")) {
                    onBackPressed();
                } else {
                    startActivity(new Intent(SelectLanguageActivity.this, LoginActivity.class));
                    finish();
                }
                break;

            case R.id.banglaLng:
                nextButton.setVisibility(View.VISIBLE);
                banglaImg.setVisibility(View.VISIBLE);
                englishImg.setVisibility(View.GONE);
                nepaliImg.setVisibility(View.GONE);
                nextButton.setText("পরবর্তী");
                break;

            case R.id.englishLng:
                nextButton.setVisibility(View.VISIBLE);
                banglaImg.setVisibility(View.GONE);
                englishImg.setVisibility(View.VISIBLE);
                nepaliImg.setVisibility(View.GONE);
                nextButton.setText("Next");
                break;

            case R.id.nepaliLng:
                nextButton.setVisibility(View.VISIBLE);
                banglaImg.setVisibility(View.GONE);
                englishImg.setVisibility(View.GONE);
                nepaliImg.setVisibility(View.VISIBLE);
                nextButton.setText("अर्को");
                break;
        }
    }
}
