package com.omninos.saowaridriver.Activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.services.MySerives;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.util.DateDifferentiate;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class VehiclePermitActivity extends AppCompatActivity implements View.OnClickListener {

    private Activity activity;
    private ImageView backButton;
    private Button continueButton;
    private EditText etVehiclePermitNumber;
    private TextView tvIssuedDate, tvExpiryDate;
    private CircleImageView civPermit;
    private RelativeLayout rlImage;

    private String vehiclePermitS = "", issuedDateVehiclePermitS = "", expiryDateVehiclePermitS = "";

    private Bitmap bitmap;
    private Uri uri;
    private static final int PICK_FROM_GALLERY = 100;
    private String imagepath = "";
    private File photoFile;
    private static final int CAMERA_REQUEST = 121;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_permit);

        stopService(new Intent(VehiclePermitActivity.this, MySerives.class));
        Intent intent = new Intent(VehiclePermitActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        findIds();
        setUps();


    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void setUps() {
        backButton.setOnClickListener(this);
        continueButton.setOnClickListener(this);
        rlImage.setOnClickListener(this);

        tvIssuedDate.setOnClickListener(this);
        tvExpiryDate.setOnClickListener(this);


        if (App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_PERMIT_DETAILS).equalsIgnoreCase("1")) {

            issuedDateVehiclePermitS = App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_PERMIT_ISSUE_DATE);
            expiryDateVehiclePermitS = App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_PERMIT_EXPIRY_DATE);
            vehiclePermitS = App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_PERMIT_NUMBER);

            tvIssuedDate.setText(issuedDateVehiclePermitS);
            tvExpiryDate.setText(expiryDateVehiclePermitS);
            etVehiclePermitNumber.setText(vehiclePermitS);

            imagepath = App.getAppPreference().GetString(ConstantData.DRIVER_VEHICLE_PERMIT_IMAGE);

            File image = new File(imagepath);
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
            civPermit.setImageBitmap(bitmap);

        }

    }

    private void findIds() {
        activity = VehiclePermitActivity.this;
        backButton = findViewById(R.id.back_btn);

        etVehiclePermitNumber = findViewById(R.id.et_vehicle_tag);
        tvIssuedDate = findViewById(R.id.tv_issued_date);
        tvExpiryDate = findViewById(R.id.tv_expiration_date);

        continueButton = findViewById(R.id.continue_btn);
        rlImage = findViewById(R.id.userimg);
        civPermit = findViewById(R.id.civ_image);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.userimg:
                selectImage();
                break;
            case R.id.continue_btn:
                validation();
                break;
            case R.id.tv_issued_date:
                datePicker(0);
                break;
            case R.id.tv_expiration_date:
                datePicker(1);
                break;
        }
    }

    private void validation() {

        vehiclePermitS = etVehiclePermitNumber.getText().toString().trim();

        if (imagepath.isEmpty()) {
            Toast.makeText(activity, "Please select vehicle permit image", Toast.LENGTH_SHORT).show();
        } else if (vehiclePermitS.isEmpty()) {
            Toast.makeText(activity, "Please enter vehicle permit number", Toast.LENGTH_SHORT).show();
        } else if (issuedDateVehiclePermitS.isEmpty()) {
            Toast.makeText(activity, "Please enter vehicle permit issue date", Toast.LENGTH_SHORT).show();
        } else if (expiryDateVehiclePermitS.isEmpty()) {
            Toast.makeText(activity, "Please enter vehicle permit expiry date", Toast.LENGTH_SHORT).show();
        } else {

            App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_PERMIT_IMAGE, imagepath);
            App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_PERMIT_NUMBER, vehiclePermitS);
            App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_PERMIT_ISSUE_DATE, issuedDateVehiclePermitS);
            App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_PERMIT_EXPIRY_DATE, expiryDateVehiclePermitS);
            App.getAppPreference().SaveString(ConstantData.DRIVER_VEHICLE_PERMIT_DETAILS, "1");

            finish();
        }
    }

    private void datePicker(final int dateType) {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Dialog,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String outputDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat mdformat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
                        String currentDate = mdformat.format(calendar.getTime());

                        if (dateType == 0) {
                            if (DateDifferentiate.compareDates(currentDate, outputDate) == 1) {
                                tvIssuedDate.setText(outputDate);
                                issuedDateVehiclePermitS = outputDate;
                            } else {
                                Toast.makeText(activity, "Please select valid date", Toast.LENGTH_SHORT).show();
                            }
                        } else if (dateType == 1) {
                            if (DateDifferentiate.compareDates(currentDate, outputDate) == 2) {
                                tvExpiryDate.setText(outputDate);
                                expiryDateVehiclePermitS = outputDate;
                            } else {
                                Toast.makeText(activity, "Please select valid date", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    //image path
    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_FROM_GALLERY);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = this.getPackageManager();
        List<ResolveInfo> listcam = packageManager.queryIntentActivities(intent, 0);
        intent.setPackage(listcam.get(0).activityInfo.packageName);
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                uri = getImageUri(VehiclePermitActivity.this, bitmap);
                imagepath = getRealPathFromUri(uri);
                civPermit.setImageBitmap(bitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private Uri getImageUri(VehiclePermitActivity youractivity, Bitmap bitmap) {
        String path = MediaStore.Images.Media.insertImage(youractivity.getContentResolver(), bitmap, "Title", null);
        return Uri.parse(path);
    }

    private String getRealPathFromUri(Uri tempUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = this.getContentResolver().query(tempUri, proj, null, null, null);
            assert cursor != null;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FROM_GALLERY)
                onSelectFromGalleryResult(data);
            else if (requestCode == CAMERA_REQUEST)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        try {
            bitmap = (Bitmap) data.getExtras().get("data");
            uri = getImageUri(VehiclePermitActivity.this, bitmap);
            imagepath = getRealPathFromUri(uri);
            App.getAppPreference().SaveString(ConstantData.IMAGEPATH, imagepath);
            Glide.with(this).load("file://" + imagepath).into(civPermit);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
