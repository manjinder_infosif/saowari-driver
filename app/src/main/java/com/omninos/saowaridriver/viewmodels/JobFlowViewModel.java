package com.omninos.saowaridriver.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.omninos.saowaridriver.model.JobFlowPojo;
import com.omninos.saowaridriver.retrofit.Api;
import com.omninos.saowaridriver.retrofit.ApiClient;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobFlowViewModel extends ViewModel {

    MutableLiveData<JobFlowPojo> acceptRejectJobMLD;

    public LiveData<JobFlowPojo> acceptRejectJob(final Activity activity, String jobStatus, String jobId) {
        acceptRejectJobMLD = new MutableLiveData<>();

        Api apiInterface = ApiClient.getApiClient().create(Api.class);

        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, "");
            apiInterface.driverAcceptRejectJob(jobStatus, jobId).enqueue(new Callback<JobFlowPojo>() {
                @Override
                public void onResponse(@NonNull Call<JobFlowPojo> call, @NonNull Response<JobFlowPojo> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        acceptRejectJobMLD.setValue(response.body());
                    } else {
                        JobFlowPojo jobFlowPojo = new JobFlowPojo();
                        jobFlowPojo.setMessage("Server Error");
                        jobFlowPojo.setSuccess("0");
                        acceptRejectJobMLD.setValue(jobFlowPojo);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<JobFlowPojo> call, @NonNull Throwable t) {
                    CommonUtils.dismissProgress();

                    JobFlowPojo jobFlowPojo = new JobFlowPojo();
                    jobFlowPojo.setMessage("Server Error");
                    jobFlowPojo.setSuccess("0");
                    acceptRejectJobMLD.setValue(jobFlowPojo);
                }
            });
        } else {

            JobFlowPojo jobFlowPojo = new JobFlowPojo();
            jobFlowPojo.setMessage("Check Internet Connection");
            jobFlowPojo.setSuccess("0");
            acceptRejectJobMLD.setValue(jobFlowPojo);
        }
        return acceptRejectJobMLD;
    }

    MutableLiveData<Map> ratingToUserMLD;

    public LiveData<Map> ratingToUser(final Activity activity, String jobId, String driverId, String userId, String comment, String rating) {
        ratingToUserMLD = new MutableLiveData<>();

        Api apiInterface = ApiClient.getApiClient().create(Api.class);

            CommonUtils.showProgress(activity,"");
            apiInterface.rating(jobId,driverId,userId,comment,rating).enqueue(new Callback<Map>() {
                @Override
                public void onResponse(@NonNull Call<Map> call, @NonNull Response<Map> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        ratingToUserMLD.setValue(response.body());
                    } else {

                    }
                }

                @Override
                public void onFailure(@NonNull Call<Map> call, @NonNull Throwable t) {
                    CommonUtils.dismissProgress();
                    Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        return ratingToUserMLD;
    }




}
