package com.omninos.saowaridriver.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.omninos.saowaridriver.model.RescheduleAppointmentPojo;
import com.omninos.saowaridriver.retrofit.Api;
import com.omninos.saowaridriver.retrofit.ApiClient;
import com.omninos.saowaridriver.util.CommonUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RescheduleAppointmentViewModel extends ViewModel {

    MutableLiveData<RescheduleAppointmentPojo> rescheduleAppointment;

    public LiveData<RescheduleAppointmentPojo> rescheduleAppointment(final Activity activity, String id) {
        rescheduleAppointment = new MutableLiveData<>();

        Api apiInterface = ApiClient.getApiClient().create(Api.class);

        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, "");

            apiInterface.rescheduleAppointment(id).enqueue(new Callback<RescheduleAppointmentPojo>() {
                @Override
                public void onResponse(@NonNull Call<RescheduleAppointmentPojo> call, @NonNull Response<RescheduleAppointmentPojo> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        rescheduleAppointment.setValue(response.body());
                    } else {
                        RescheduleAppointmentPojo rescheduleAppointmentPojo = new RescheduleAppointmentPojo();
                        rescheduleAppointmentPojo.setMessage("Server Error");
                        rescheduleAppointmentPojo.setSuccess("1");
                        rescheduleAppointment.setValue(rescheduleAppointmentPojo);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RescheduleAppointmentPojo> call, @NonNull Throwable t) {
                    CommonUtils.dismissProgress();
                    RescheduleAppointmentPojo rescheduleAppointmentPojo = new RescheduleAppointmentPojo();
                    rescheduleAppointmentPojo.setMessage("Server Error");
                    rescheduleAppointmentPojo.setSuccess("1");
                    rescheduleAppointment.setValue(rescheduleAppointmentPojo);
                }
            });

        } else {
            RescheduleAppointmentPojo rescheduleAppointmentPojo = new RescheduleAppointmentPojo();
            rescheduleAppointmentPojo.setMessage("Please Check Internet Connection");
            rescheduleAppointmentPojo.setSuccess("1");
            rescheduleAppointment.setValue(rescheduleAppointmentPojo);
        }

        return rescheduleAppointment;
    }


}
