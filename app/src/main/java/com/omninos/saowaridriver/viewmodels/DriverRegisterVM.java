package com.omninos.saowaridriver.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.omninos.saowaridriver.model.CheckDriverPhoneModel;
import com.omninos.saowaridriver.model.DriverRegisterModel;
import com.omninos.saowaridriver.retrofit.ApiClient;
import com.omninos.saowaridriver.retrofit.Api;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverRegisterVM extends ViewModel {
    private MutableLiveData<CheckDriverPhoneModel> driverRegisterModelMutableLiveData;
    private MutableLiveData<CheckDriverPhoneModel> updateDriver;

    private MutableLiveData<Map> userLogout;

    public LiveData<CheckDriverPhoneModel> driverRegisterModel(final Activity activity, RequestBody driverName,
                                                               RequestBody driverPhoneNumber, RequestBody driverEmail,
                                                               RequestBody dateOfBirth, RequestBody gender,
                                                               RequestBody device_type, RequestBody reg_id,
                                                               RequestBody latitude, RequestBody longitude,
                                                               RequestBody login_type, MultipartBody.Part driverImage, RequestBody driverJob) {
        driverRegisterModelMutableLiveData = new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);

        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            api.driverRegister(driverName, driverPhoneNumber, driverEmail, dateOfBirth, gender, device_type, reg_id, latitude, longitude, login_type, driverImage, driverJob).enqueue(new Callback<CheckDriverPhoneModel>() {
                @Override
                public void onResponse(Call<CheckDriverPhoneModel> call, Response<CheckDriverPhoneModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        driverRegisterModelMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<CheckDriverPhoneModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    CheckDriverPhoneModel driverRegisterModel = new CheckDriverPhoneModel();
                    driverRegisterModel.setSuccess("0");
                    driverRegisterModel.setMessage("Server Error");
                    driverRegisterModelMutableLiveData.setValue(driverRegisterModel);

                }
            });
        } else {
            CheckDriverPhoneModel driverRegisterModel = new CheckDriverPhoneModel();
            driverRegisterModel.setSuccess("0");
            driverRegisterModel.setMessage("Network Error");
            driverRegisterModelMutableLiveData.setValue(driverRegisterModel);
        }
        return driverRegisterModelMutableLiveData;
    }


    public LiveData<CheckDriverPhoneModel> updatedriverProfile(Activity activity, RequestBody driverName, RequestBody address, RequestBody driverPhoneNumber, RequestBody driverEmail, RequestBody driverId, MultipartBody.Part driverImage) {
        updateDriver = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {

            CommonUtils.showProgress(activity, "");
            Api api = ApiClient.getApiClient().create(Api.class);

            api.updateProfile(driverName, address, driverPhoneNumber, driverEmail, driverId, driverImage).enqueue(new Callback<CheckDriverPhoneModel>() {
                @Override
                public void onResponse(Call<CheckDriverPhoneModel> call, Response<CheckDriverPhoneModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        updateDriver.setValue(response.body());
                    } else {
                        CheckDriverPhoneModel checkDriverPhoneModel = new CheckDriverPhoneModel();
                        checkDriverPhoneModel.setSuccess("0");
                        checkDriverPhoneModel.setMessage("Server Error");
                        updateDriver.setValue(checkDriverPhoneModel);
                    }
                }

                @Override
                public void onFailure(Call<CheckDriverPhoneModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    CheckDriverPhoneModel checkDriverPhoneModel = new CheckDriverPhoneModel();
                    checkDriverPhoneModel.setSuccess("0");
                    checkDriverPhoneModel.setMessage("Server Error");
                    updateDriver.setValue(checkDriverPhoneModel);
                }
            });

        } else {

            CheckDriverPhoneModel checkDriverPhoneModel = new CheckDriverPhoneModel();
            checkDriverPhoneModel.setSuccess("0");
            checkDriverPhoneModel.setMessage("Please Check Internet Connection");
            updateDriver.setValue(checkDriverPhoneModel);
        }

        return updateDriver;
    }

    public LiveData<Map> LogOut(Activity activity, String userId) {
        userLogout = new MutableLiveData<>();
        CommonUtils.showProgress(activity, "");
        Api api = ApiClient.getApiClient().create(Api.class);
        api.logOut(userId).enqueue(new Callback<Map>() {
            @Override
            public void onResponse(Call<Map> call, Response<Map> response) {
                CommonUtils.dismissProgress();
                if (response.body() != null) {
                    userLogout.setValue(response.body());
                }
            }
            @Override
            public void onFailure(Call<Map> call, Throwable t) {
                CommonUtils.dismissProgress();

            }
        });
        return userLogout;
    }
}
