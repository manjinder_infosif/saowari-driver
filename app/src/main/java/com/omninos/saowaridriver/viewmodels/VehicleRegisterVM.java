package com.omninos.saowaridriver.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.widget.Toast;

import com.omninos.saowaridriver.model.VehicleRegisterModel;
import com.omninos.saowaridriver.retrofit.ApiClient;
import com.omninos.saowaridriver.retrofit.Api;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleRegisterVM extends ViewModel {

    private MutableLiveData<VehicleRegisterModel> vehicleRegisterModelMutableLiveData;

    public LiveData<VehicleRegisterModel> vehicleRegister(final Activity activity, RequestBody vehicleBrand, RequestBody vehicleModel, RequestBody vehicalYear, RequestBody vehicleColor, RequestBody vehiclePlateNumber, RequestBody driverLicenseNumber, RequestBody driverVehicleType,
                                                          RequestBody driverLicenceStartDate, RequestBody driverLicenseExpiry,
                                                          RequestBody vehicleInsurenceNumber,
                                                          RequestBody vehicleInsurenceStartDate,
                                                          RequestBody vehicleInsurenceExpiery, RequestBody vehiclePermitNumber,
                                                          RequestBody vehiclePermitStartDate, RequestBody vehiclePermitExpiryDate,
                                                          RequestBody vehicleRegistartionNumber, RequestBody vehicleRegistartionDate,
                                                          RequestBody vehicleRegistartionValidDate, MultipartBody.Part driverLicenseImage,
                                                          MultipartBody.Part vehicleRegistartionImage, MultipartBody.Part vehicleImage,
                                                          MultipartBody.Part vehicleInsurenceImage, MultipartBody.Part vehiclePermitImage,
                                                          RequestBody driverId,
                                                          RequestBody type) {

        vehicleRegisterModelMutableLiveData = new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);

        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            api.vehcileRegister(vehicleBrand, vehicleModel, vehicalYear, vehicleColor,vehiclePlateNumber,
                    driverLicenseNumber, driverVehicleType, driverLicenceStartDate,
                    driverLicenseExpiry, vehicleInsurenceNumber, vehicleInsurenceStartDate,
                    vehicleInsurenceExpiery, vehiclePermitStartDate, vehiclePermitExpiryDate,
                    vehiclePermitNumber, vehicleRegistartionNumber, vehicleRegistartionDate,
                    vehicleRegistartionValidDate, driverId, driverLicenseImage,
                    vehicleRegistartionImage, vehicleImage, vehicleInsurenceImage,
                    vehiclePermitImage,type).enqueue(new Callback<VehicleRegisterModel>() {
                @Override
                public void onResponse(Call<VehicleRegisterModel> call, Response<VehicleRegisterModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        vehicleRegisterModelMutableLiveData.setValue(response.body());
                    } else {
                        Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<VehicleRegisterModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    VehicleRegisterModel vehicleRegisterModel = new VehicleRegisterModel();
                    vehicleRegisterModel.setSuccess("0");
                    vehicleRegisterModel.setMessage("Server Error");
                    vehicleRegisterModelMutableLiveData.setValue(vehicleRegisterModel);
                }
            });

        } else {
            VehicleRegisterModel vehicleRegisterModel = new VehicleRegisterModel();
            vehicleRegisterModel.setSuccess("0");
            vehicleRegisterModel.setMessage("Network Issue");
            vehicleRegisterModelMutableLiveData.setValue(vehicleRegisterModel);
        }
        return vehicleRegisterModelMutableLiveData;
    }
}
