package com.omninos.saowaridriver.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.omninos.saowaridriver.model.VehicleListModel;
import com.omninos.saowaridriver.model.VehicleTypePojo;
import com.omninos.saowaridriver.retrofit.ApiClient;
import com.omninos.saowaridriver.retrofit.Api;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleBrandsAndModelsVM extends ViewModel {
    //getMotorCycle Brand List
    private MutableLiveData<VehicleListModel> moterCycleListModelMutableLiveData;

    public LiveData<VehicleListModel> moterCyclerList(final Activity activity,String type) {

        moterCycleListModelMutableLiveData = new MutableLiveData<>();

        Api api = ApiClient.getApiClient().create(Api.class);

        if (CommonUtils.isNetworkConnected(activity)) {

            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            api.getMakeAndModelList(type).enqueue(new Callback<VehicleListModel>() {
                @Override
                public void onResponse(@NonNull Call<VehicleListModel> call, @NonNull Response<VehicleListModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        moterCycleListModelMutableLiveData.setValue(response.body());
                    } else {
                        Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<VehicleListModel> call, @NonNull Throwable t) {
                    CommonUtils.dismissProgress();
                    Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }
        return moterCycleListModelMutableLiveData;
    }


    MutableLiveData<VehicleTypePojo> vehicleType;

    public LiveData<VehicleTypePojo> vehicleTypes(final Activity activity) {

        vehicleType = new MutableLiveData<>();

        Api api = ApiClient.getApiClient().create(Api.class);

        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, "Please wait...");
            api.vehicleDetails().enqueue(new Callback<VehicleTypePojo>() {
                @Override
                public void onResponse(@NonNull Call<VehicleTypePojo> call, @NonNull Response<VehicleTypePojo> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        vehicleType.setValue(response.body());
                    } else {
//                        Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<VehicleTypePojo> call, @NonNull Throwable t) {
                    CommonUtils.dismissProgress();
                    Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }

        return vehicleType;
    }


}
