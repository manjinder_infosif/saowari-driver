package com.omninos.saowaridriver.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;


import com.omninos.saowaridriver.model.HelpAndContactusModel;
import com.omninos.saowaridriver.retrofit.Api;
import com.omninos.saowaridriver.retrofit.ApiClient;
import com.omninos.saowaridriver.util.CommonUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HelpAndContactUsViewModel extends ViewModel {

    private MutableLiveData<HelpAndContactusModel> helpAndContactusModelMutableLiveData;

    public LiveData<HelpAndContactusModel> helpAndContactusModelLiveData(Activity activity, String type, String name, String phone, String email, String message, String driverId) {
        helpAndContactusModelMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {

            CommonUtils.showProgress(activity,"");

            Api api = ApiClient.getApiClient().create(Api.class);
            api.HelpContact(type, name, phone, email, message, driverId).enqueue(new Callback<HelpAndContactusModel>() {
                @Override
                public void onResponse(Call<HelpAndContactusModel> call, Response<HelpAndContactusModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        helpAndContactusModelMutableLiveData.setValue(response.body());
                    } else {
                        HelpAndContactusModel helpAndContactusModel = new HelpAndContactusModel();
                        helpAndContactusModel.setMessage("Server Error");
                        helpAndContactusModel.setSuccess("0");
                        helpAndContactusModelMutableLiveData.setValue(helpAndContactusModel);
                    }
                }

                @Override
                public void onFailure(Call<HelpAndContactusModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    HelpAndContactusModel helpAndContactusModel = new HelpAndContactusModel();
                    helpAndContactusModel.setMessage("Server Error");
                    helpAndContactusModel.setSuccess("0");
                    helpAndContactusModelMutableLiveData.setValue(helpAndContactusModel);
                }
            });

        } else {
            HelpAndContactusModel helpAndContactusModel = new HelpAndContactusModel();
            helpAndContactusModel.setMessage("Please Check Internet Connection");
            helpAndContactusModel.setSuccess("0");
            helpAndContactusModelMutableLiveData.setValue(helpAndContactusModel);
        }

        return helpAndContactusModelMutableLiveData;
    }
}
