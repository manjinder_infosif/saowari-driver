package com.omninos.saowaridriver.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.omninos.saowaridriver.model.MyTripsModel;
import com.omninos.saowaridriver.model.PendingAmountModel;
import com.omninos.saowaridriver.retrofit.Api;
import com.omninos.saowaridriver.retrofit.ApiClient;
import com.omninos.saowaridriver.util.CommonUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyTripsViewModel extends ViewModel {

    MutableLiveData<MyTripsModel> myTripsPojoMLD;

    MutableLiveData<PendingAmountModel> pendingAmountModelMutableLiveData;


    public LiveData<MyTripsModel> myTrips(final Activity activity, String id) {
        myTripsPojoMLD = new MutableLiveData<>();

        Api apiInterface = ApiClient.getApiClient().create(Api.class);

        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, "");
            apiInterface.myTrips(id).enqueue(new Callback<MyTripsModel>() {
                @Override
                public void onResponse(@NonNull Call<MyTripsModel> call, @NonNull Response<MyTripsModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        myTripsPojoMLD.setValue(response.body());
                    } else {
                        MyTripsModel myTripsModel = new MyTripsModel();
                        myTripsModel.setMessage("Server Error");
                        myTripsModel.setSuccess("1");
                        myTripsPojoMLD.setValue(myTripsModel);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MyTripsModel> call, @NonNull Throwable t) {
                    CommonUtils.dismissProgress();
                    MyTripsModel myTripsModel = new MyTripsModel();
                    myTripsModel.setMessage("Server Error");
                    myTripsModel.setSuccess("1");
                    myTripsPojoMLD.setValue(myTripsModel);
                }
            });
        } else {
            MyTripsModel myTripsModel = new MyTripsModel();
            myTripsModel.setMessage("Please Check Internet Connection");
            myTripsModel.setSuccess("1");
            myTripsPojoMLD.setValue(myTripsModel);
        }
        return myTripsPojoMLD;
    }

    public LiveData<PendingAmountModel> pendingAmountModelLiveData(Activity activity, String driverId, String status) {
        pendingAmountModelMutableLiveData = new MutableLiveData<>();

        CommonUtils.showProgress(activity, "");
        Api api = ApiClient.getApiClient().create(Api.class);

        api.pending(driverId, status).enqueue(new Callback<PendingAmountModel>() {
            @Override
            public void onResponse(Call<PendingAmountModel> call, Response<PendingAmountModel> response) {
                CommonUtils.dismissProgress();
                if (response.body() != null) {
                    pendingAmountModelMutableLiveData.setValue(response.body());
                } else {
                    PendingAmountModel pendingAmountModel = new PendingAmountModel();
                    pendingAmountModel.setSuccess("0");
                    pendingAmountModel.setMessage("Server Error");
                    pendingAmountModelMutableLiveData.setValue(pendingAmountModel);
                }
            }

            @Override
            public void onFailure(Call<PendingAmountModel> call, Throwable t) {
                CommonUtils.dismissProgress();
                PendingAmountModel pendingAmountModel = new PendingAmountModel();
                pendingAmountModel.setSuccess("0");
                pendingAmountModel.setMessage("Server Error");
                pendingAmountModelMutableLiveData.setValue(pendingAmountModel);
            }
        });

        return pendingAmountModelMutableLiveData;
    }
}
