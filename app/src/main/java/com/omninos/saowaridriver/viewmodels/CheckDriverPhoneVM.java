package com.omninos.saowaridriver.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.omninos.saowaridriver.model.CheckDriverPhoneModel;
import com.omninos.saowaridriver.retrofit.ApiClient;
import com.omninos.saowaridriver.retrofit.Api;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckDriverPhoneVM extends ViewModel {
    private MutableLiveData<CheckDriverPhoneModel> checkDriverPhoneModelMutableLiveData;

    public LiveData<CheckDriverPhoneModel> checkDriverPhone(final Activity activity, String driverPhoneNumber, String latitude, String longitude, String reg_id, String device_type, String login_type) {
        checkDriverPhoneModelMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            Api api = ApiClient.getApiClient().create(Api.class);
            api.checkphone(driverPhoneNumber, latitude, longitude, reg_id, device_type, login_type).enqueue(new Callback<CheckDriverPhoneModel>() {
                @Override
                public void onResponse(Call<CheckDriverPhoneModel> call, Response<CheckDriverPhoneModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        checkDriverPhoneModelMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<CheckDriverPhoneModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    CheckDriverPhoneModel checkDriverPhoneModel = new CheckDriverPhoneModel();
                    checkDriverPhoneModel.setSuccess("2");
                    checkDriverPhoneModel.setMessage("Server Error");
                    checkDriverPhoneModelMutableLiveData.setValue(checkDriverPhoneModel);
                }
            });
        } else {
            CheckDriverPhoneModel checkDriverPhoneModel = new CheckDriverPhoneModel();
            checkDriverPhoneModel.setSuccess("2");
            checkDriverPhoneModel.setMessage("Network Issue");
            checkDriverPhoneModelMutableLiveData.setValue(checkDriverPhoneModel);
        }
        return checkDriverPhoneModelMutableLiveData;
    }
}
