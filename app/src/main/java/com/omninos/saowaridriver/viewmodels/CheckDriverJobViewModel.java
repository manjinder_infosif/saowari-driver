package com.omninos.saowaridriver.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.omninos.saowaridriver.model.CheckDriverJobPojo;
import com.omninos.saowaridriver.model.VersionModel;
import com.omninos.saowaridriver.retrofit.Api;
import com.omninos.saowaridriver.retrofit.ApiClient;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckDriverJobViewModel extends ViewModel {

    MutableLiveData<CheckDriverJobPojo> checkDriverJobMLD;

    private MutableLiveData<VersionModel> versionModelMutableLiveData;


    public LiveData<CheckDriverJobPojo> çheckDriverJob(final Activity activity, String jobId) {

        checkDriverJobMLD = new MutableLiveData<>();

        Api apiInterface = ApiClient.getApiClient().create(Api.class);

        if (CommonUtils.isNetworkConnected(activity)) {
//            CommonUtils.showProgress(activity, "");
            apiInterface.checkDriverJob(jobId).enqueue(new Callback<CheckDriverJobPojo>() {
                @Override
                public void onResponse(@NonNull Call<CheckDriverJobPojo> call, @NonNull Response<CheckDriverJobPojo> response) {
//                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        checkDriverJobMLD.setValue(response.body());
                    } else {
                        CheckDriverJobPojo checkDriverJobPojo = new CheckDriverJobPojo();
                        checkDriverJobPojo.setMessage("Server Error");
                        checkDriverJobPojo.setSuccess("0");
                        checkDriverJobMLD.setValue(checkDriverJobPojo);
//                        Toast.makeText(activity, ConstantData.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CheckDriverJobPojo> call, @NonNull Throwable t) {
//                    CommonUtils.dismissProgress();
                    CheckDriverJobPojo checkDriverJobPojo = new CheckDriverJobPojo();
                    checkDriverJobPojo.setMessage("Server Error");
                    checkDriverJobPojo.setSuccess("0");
                    checkDriverJobMLD.setValue(checkDriverJobPojo);
                }
            });
        } else {
            CheckDriverJobPojo checkDriverJobPojo = new CheckDriverJobPojo();
            checkDriverJobPojo.setMessage("Please Check Internet Connection");
            checkDriverJobPojo.setSuccess("0");
            checkDriverJobMLD.setValue(checkDriverJobPojo);
        }
        return checkDriverJobMLD;
    }


    public LiveData<VersionModel> modelLiveData(Activity activity) {

        versionModelMutableLiveData = new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);

        api.version("driver").enqueue(new Callback<VersionModel>() {
            @Override
            public void onResponse(Call<VersionModel> call, Response<VersionModel> response) {
                if (response.body() != null) {
                    versionModelMutableLiveData.setValue(response.body());
                } else {

                }
            }

            @Override
            public void onFailure(Call<VersionModel> call, Throwable t) {

            }
        });

        return versionModelMutableLiveData;
    }

}