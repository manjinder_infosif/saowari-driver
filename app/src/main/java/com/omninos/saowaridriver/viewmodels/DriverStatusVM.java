package com.omninos.saowaridriver.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.widget.Toast;

import com.omninos.saowaridriver.retrofit.Api;
import com.omninos.saowaridriver.retrofit.ApiClient;
import com.omninos.saowaridriver.util.CommonUtils;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverStatusVM extends ViewModel {

    private MutableLiveData<Map> onlineOfflineStatus;

    public LiveData<Map> status(final Activity activity, String onlineStatus, String driverId) {

        onlineOfflineStatus = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {

//            CommonUtils.showProgress(activity, "");

            Api api = ApiClient.getApiClient().create(Api.class);
            api.onlineOfflineData(onlineStatus, driverId).enqueue(new Callback<Map>() {
                @Override
                public void onResponse(Call<Map> call, Response<Map> response) {
//                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        onlineOfflineStatus.setValue(response.body());
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<Map> call, Throwable t) {
//                    CommonUtils.dismissProgress();
                    Toast.makeText(activity, "Server Issue", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }

        return onlineOfflineStatus;
    }
}
