package com.omninos.saowaridriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverRegisterModel {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private Details details;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public DriverRegisterModel withSuccess(String success) {
        this.success = success;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DriverRegisterModel withMessage(String message) {
        this.message = message;
        return this;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public DriverRegisterModel withDetails(Details details) {
        this.details = details;
        return this;
    }

    public class Details {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("walletBalance")
        @Expose
        private String walletBalance;
        @SerializedName("onlineStatus")
        @Expose
        private String onlineStatus;
        @SerializedName("social_id")
        @Expose
        private String socialId;
        @SerializedName("appointmentStatus")
        @Expose
        private String appointmentStatus;
        @SerializedName("appointmentDate")
        @Expose
        private String appointmentDate;
        @SerializedName("appointmentTime")
        @Expose
        private String appointmentTime;
        @SerializedName("appointmentAddress")
        @Expose
        private String appointmentAddress;
        @SerializedName("otp")
        @Expose
        private String otp;
        @SerializedName("phoneVerifyStatus")
        @Expose
        private String phoneVerifyStatus;
        @SerializedName("driverName")
        @Expose
        private String driverName;
        @SerializedName("driverEmail")
        @Expose
        private String driverEmail;
        @SerializedName("driverPhoneNumber")
        @Expose
        private String driverPhoneNumber;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("driverPassword")
        @Expose
        private String driverPassword;
        @SerializedName("driverImage")
        @Expose
        private String driverImage;
        @SerializedName("driverLicenseNumber")
        @Expose
        private String driverLicenseNumber;
        @SerializedName("driverVehicleType")
        @Expose
        private String driverVehicleType;
        @SerializedName("driverLicenceStartDate")
        @Expose
        private String driverLicenceStartDate;
        @SerializedName("driverLicenseExpiry")
        @Expose
        private String driverLicenseExpiry;
        @SerializedName("driverLicenseImage")
        @Expose
        private String driverLicenseImage;
        @SerializedName("vehicleImage")
        @Expose
        private String vehicleImage;
        @SerializedName("vehicleBrand")
        @Expose
        private String vehicleBrand;
        @SerializedName("vehicleModel")
        @Expose
        private String vehicleModel;
        @SerializedName("vehicalYear")
        @Expose
        private String vehicalYear;
        @SerializedName("vehicleColor")
        @Expose
        private String vehicleColor;
        @SerializedName("vehicleInsurenceImage")
        @Expose
        private String vehicleInsurenceImage;
        @SerializedName("vehicleInsurenceNumber")
        @Expose
        private String vehicleInsurenceNumber;
        @SerializedName("vehicleInsurenceStartDate")
        @Expose
        private String vehicleInsurenceStartDate;
        @SerializedName("vehicleInsurenceExpiery")
        @Expose
        private String vehicleInsurenceExpiery;
        @SerializedName("vehicleCertificateImage")
        @Expose
        private String vehicleCertificateImage;
        @SerializedName("vehiclePermitImage")
        @Expose
        private String vehiclePermitImage;
        @SerializedName("vehiclePermitNumber")
        @Expose
        private String vehiclePermitNumber;
        @SerializedName("vehiclePermitStartDate")
        @Expose
        private String vehiclePermitStartDate;
        @SerializedName("vehiclePermitExpiryDate")
        @Expose
        private String vehiclePermitExpiryDate;
        @SerializedName("region")
        @Expose
        private String region;
        @SerializedName("about")
        @Expose
        private String about;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("category")
        @Expose
        private String category;
        @SerializedName("digit")
        @Expose
        private String digit;
        @SerializedName("reg_id")
        @Expose
        private String regId;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("previousLongitude")
        @Expose
        private String previousLongitude;
        @SerializedName("previousLatitude")
        @Expose
        private String previousLatitude;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("login_type")
        @Expose
        private String loginType;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("vehicleRegistartionImage")
        @Expose
        private String vehicleRegistartionImage;
        @SerializedName("vehicleRegistartionNumber")
        @Expose
        private String vehicleRegistartionNumber;
        @SerializedName("vehicleRegistartionDate")
        @Expose
        private String vehicleRegistartionDate;
        @SerializedName("vehicleRegistartionValidDate")
        @Expose
        private String vehicleRegistartionValidDate;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Details withId(String id) {
            this.id = id;
            return this;
        }

        public String getWalletBalance() {
            return walletBalance;
        }

        public void setWalletBalance(String walletBalance) {
            this.walletBalance = walletBalance;
        }

        public Details withWalletBalance(String walletBalance) {
            this.walletBalance = walletBalance;
            return this;
        }

        public String getOnlineStatus() {
            return onlineStatus;
        }

        public void setOnlineStatus(String onlineStatus) {
            this.onlineStatus = onlineStatus;
        }

        public Details withOnlineStatus(String onlineStatus) {
            this.onlineStatus = onlineStatus;
            return this;
        }

        public String getSocialId() {
            return socialId;
        }

        public void setSocialId(String socialId) {
            this.socialId = socialId;
        }

        public Details withSocialId(String socialId) {
            this.socialId = socialId;
            return this;
        }

        public String getAppointmentStatus() {
            return appointmentStatus;
        }

        public void setAppointmentStatus(String appointmentStatus) {
            this.appointmentStatus = appointmentStatus;
        }

        public Details withAppointmentStatus(String appointmentStatus) {
            this.appointmentStatus = appointmentStatus;
            return this;
        }

        public String getAppointmentDate() {
            return appointmentDate;
        }

        public void setAppointmentDate(String appointmentDate) {
            this.appointmentDate = appointmentDate;
        }

        public Details withAppointmentDate(String appointmentDate) {
            this.appointmentDate = appointmentDate;
            return this;
        }

        public String getAppointmentTime() {
            return appointmentTime;
        }

        public void setAppointmentTime(String appointmentTime) {
            this.appointmentTime = appointmentTime;
        }

        public Details withAppointmentTime(String appointmentTime) {
            this.appointmentTime = appointmentTime;
            return this;
        }

        public String getAppointmentAddress() {
            return appointmentAddress;
        }

        public void setAppointmentAddress(String appointmentAddress) {
            this.appointmentAddress = appointmentAddress;
        }

        public Details withAppointmentAddress(String appointmentAddress) {
            this.appointmentAddress = appointmentAddress;
            return this;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public Details withOtp(String otp) {
            this.otp = otp;
            return this;
        }

        public String getPhoneVerifyStatus() {
            return phoneVerifyStatus;
        }

        public void setPhoneVerifyStatus(String phoneVerifyStatus) {
            this.phoneVerifyStatus = phoneVerifyStatus;
        }

        public Details withPhoneVerifyStatus(String phoneVerifyStatus) {
            this.phoneVerifyStatus = phoneVerifyStatus;
            return this;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }

        public Details withDriverName(String driverName) {
            this.driverName = driverName;
            return this;
        }

        public String getDriverEmail() {
            return driverEmail;
        }

        public void setDriverEmail(String driverEmail) {
            this.driverEmail = driverEmail;
        }

        public Details withDriverEmail(String driverEmail) {
            this.driverEmail = driverEmail;
            return this;
        }

        public String getDriverPhoneNumber() {
            return driverPhoneNumber;
        }

        public void setDriverPhoneNumber(String driverPhoneNumber) {
            this.driverPhoneNumber = driverPhoneNumber;
        }

        public Details withDriverPhoneNumber(String driverPhoneNumber) {
            this.driverPhoneNumber = driverPhoneNumber;
            return this;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Details withType(String type) {
            this.type = type;
            return this;
        }

        public String getDriverPassword() {
            return driverPassword;
        }

        public void setDriverPassword(String driverPassword) {
            this.driverPassword = driverPassword;
        }

        public Details withDriverPassword(String driverPassword) {
            this.driverPassword = driverPassword;
            return this;
        }

        public String getDriverImage() {
            return driverImage;
        }

        public void setDriverImage(String driverImage) {
            this.driverImage = driverImage;
        }

        public Details withDriverImage(String driverImage) {
            this.driverImage = driverImage;
            return this;
        }

        public String getDriverLicenseNumber() {
            return driverLicenseNumber;
        }

        public void setDriverLicenseNumber(String driverLicenseNumber) {
            this.driverLicenseNumber = driverLicenseNumber;
        }

        public Details withDriverLicenseNumber(String driverLicenseNumber) {
            this.driverLicenseNumber = driverLicenseNumber;
            return this;
        }

        public String getDriverVehicleType() {
            return driverVehicleType;
        }

        public void setDriverVehicleType(String driverVehicleType) {
            this.driverVehicleType = driverVehicleType;
        }

        public Details withDriverVehicleType(String driverVehicleType) {
            this.driverVehicleType = driverVehicleType;
            return this;
        }

        public String getDriverLicenceStartDate() {
            return driverLicenceStartDate;
        }

        public void setDriverLicenceStartDate(String driverLicenceStartDate) {
            this.driverLicenceStartDate = driverLicenceStartDate;
        }

        public Details withDriverLicenceStartDate(String driverLicenceStartDate) {
            this.driverLicenceStartDate = driverLicenceStartDate;
            return this;
        }

        public String getDriverLicenseExpiry() {
            return driverLicenseExpiry;
        }

        public void setDriverLicenseExpiry(String driverLicenseExpiry) {
            this.driverLicenseExpiry = driverLicenseExpiry;
        }

        public Details withDriverLicenseExpiry(String driverLicenseExpiry) {
            this.driverLicenseExpiry = driverLicenseExpiry;
            return this;
        }

        public String getDriverLicenseImage() {
            return driverLicenseImage;
        }

        public void setDriverLicenseImage(String driverLicenseImage) {
            this.driverLicenseImage = driverLicenseImage;
        }

        public Details withDriverLicenseImage(String driverLicenseImage) {
            this.driverLicenseImage = driverLicenseImage;
            return this;
        }

        public String getVehicleImage() {
            return vehicleImage;
        }

        public void setVehicleImage(String vehicleImage) {
            this.vehicleImage = vehicleImage;
        }

        public Details withVehicleImage(String vehicleImage) {
            this.vehicleImage = vehicleImage;
            return this;
        }

        public String getVehicleBrand() {
            return vehicleBrand;
        }

        public void setVehicleBrand(String vehicleBrand) {
            this.vehicleBrand = vehicleBrand;
        }

        public Details withVehicleBrand(String vehicleBrand) {
            this.vehicleBrand = vehicleBrand;
            return this;
        }

        public String getVehicleModel() {
            return vehicleModel;
        }

        public void setVehicleModel(String vehicleModel) {
            this.vehicleModel = vehicleModel;
        }

        public Details withVehicleModel(String vehicleModel) {
            this.vehicleModel = vehicleModel;
            return this;
        }

        public String getVehicalYear() {
            return vehicalYear;
        }

        public void setVehicalYear(String vehicalYear) {
            this.vehicalYear = vehicalYear;
        }

        public Details withVehicalYear(String vehicalYear) {
            this.vehicalYear = vehicalYear;
            return this;
        }

        public String getVehicleColor() {
            return vehicleColor;
        }

        public void setVehicleColor(String vehicleColor) {
            this.vehicleColor = vehicleColor;
        }

        public Details withVehicleColor(String vehicleColor) {
            this.vehicleColor = vehicleColor;
            return this;
        }

        public String getVehicleInsurenceImage() {
            return vehicleInsurenceImage;
        }

        public void setVehicleInsurenceImage(String vehicleInsurenceImage) {
            this.vehicleInsurenceImage = vehicleInsurenceImage;
        }

        public Details withVehicleInsurenceImage(String vehicleInsurenceImage) {
            this.vehicleInsurenceImage = vehicleInsurenceImage;
            return this;
        }

        public String getVehicleInsurenceNumber() {
            return vehicleInsurenceNumber;
        }

        public void setVehicleInsurenceNumber(String vehicleInsurenceNumber) {
            this.vehicleInsurenceNumber = vehicleInsurenceNumber;
        }

        public Details withVehicleInsurenceNumber(String vehicleInsurenceNumber) {
            this.vehicleInsurenceNumber = vehicleInsurenceNumber;
            return this;
        }

        public String getVehicleInsurenceStartDate() {
            return vehicleInsurenceStartDate;
        }

        public void setVehicleInsurenceStartDate(String vehicleInsurenceStartDate) {
            this.vehicleInsurenceStartDate = vehicleInsurenceStartDate;
        }

        public Details withVehicleInsurenceStartDate(String vehicleInsurenceStartDate) {
            this.vehicleInsurenceStartDate = vehicleInsurenceStartDate;
            return this;
        }

        public String getVehicleInsurenceExpiery() {
            return vehicleInsurenceExpiery;
        }

        public void setVehicleInsurenceExpiery(String vehicleInsurenceExpiery) {
            this.vehicleInsurenceExpiery = vehicleInsurenceExpiery;
        }

        public Details withVehicleInsurenceExpiery(String vehicleInsurenceExpiery) {
            this.vehicleInsurenceExpiery = vehicleInsurenceExpiery;
            return this;
        }

        public String getVehicleCertificateImage() {
            return vehicleCertificateImage;
        }

        public void setVehicleCertificateImage(String vehicleCertificateImage) {
            this.vehicleCertificateImage = vehicleCertificateImage;
        }

        public Details withVehicleCertificateImage(String vehicleCertificateImage) {
            this.vehicleCertificateImage = vehicleCertificateImage;
            return this;
        }

        public String getVehiclePermitImage() {
            return vehiclePermitImage;
        }

        public void setVehiclePermitImage(String vehiclePermitImage) {
            this.vehiclePermitImage = vehiclePermitImage;
        }

        public Details withVehiclePermitImage(String vehiclePermitImage) {
            this.vehiclePermitImage = vehiclePermitImage;
            return this;
        }

        public String getVehiclePermitNumber() {
            return vehiclePermitNumber;
        }

        public void setVehiclePermitNumber(String vehiclePermitNumber) {
            this.vehiclePermitNumber = vehiclePermitNumber;
        }

        public Details withVehiclePermitNumber(String vehiclePermitNumber) {
            this.vehiclePermitNumber = vehiclePermitNumber;
            return this;
        }

        public String getVehiclePermitStartDate() {
            return vehiclePermitStartDate;
        }

        public void setVehiclePermitStartDate(String vehiclePermitStartDate) {
            this.vehiclePermitStartDate = vehiclePermitStartDate;
        }

        public Details withVehiclePermitStartDate(String vehiclePermitStartDate) {
            this.vehiclePermitStartDate = vehiclePermitStartDate;
            return this;
        }

        public String getVehiclePermitExpiryDate() {
            return vehiclePermitExpiryDate;
        }

        public void setVehiclePermitExpiryDate(String vehiclePermitExpiryDate) {
            this.vehiclePermitExpiryDate = vehiclePermitExpiryDate;
        }

        public Details withVehiclePermitExpiryDate(String vehiclePermitExpiryDate) {
            this.vehiclePermitExpiryDate = vehiclePermitExpiryDate;
            return this;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public Details withRegion(String region) {
            this.region = region;
            return this;
        }

        public String getAbout() {
            return about;
        }

        public void setAbout(String about) {
            this.about = about;
        }

        public Details withAbout(String about) {
            this.about = about;
            return this;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Details withAddress(String address) {
            this.address = address;
            return this;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public Details withCategory(String category) {
            this.category = category;
            return this;
        }

        public String getDigit() {
            return digit;
        }

        public void setDigit(String digit) {
            this.digit = digit;
        }

        public Details withDigit(String digit) {
            this.digit = digit;
            return this;
        }

        public String getRegId() {
            return regId;
        }

        public void setRegId(String regId) {
            this.regId = regId;
        }

        public Details withRegId(String regId) {
            this.regId = regId;
            return this;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public Details withDeviceType(String deviceType) {
            this.deviceType = deviceType;
            return this;
        }

        public String getPreviousLongitude() {
            return previousLongitude;
        }

        public void setPreviousLongitude(String previousLongitude) {
            this.previousLongitude = previousLongitude;
        }

        public Details withPreviousLongitude(String previousLongitude) {
            this.previousLongitude = previousLongitude;
            return this;
        }

        public String getPreviousLatitude() {
            return previousLatitude;
        }

        public void setPreviousLatitude(String previousLatitude) {
            this.previousLatitude = previousLatitude;
        }

        public Details withPreviousLatitude(String previousLatitude) {
            this.previousLatitude = previousLatitude;
            return this;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public Details withLatitude(String latitude) {
            this.latitude = latitude;
            return this;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public Details withLongitude(String longitude) {
            this.longitude = longitude;
            return this;
        }

        public String getLoginType() {
            return loginType;
        }

        public void setLoginType(String loginType) {
            this.loginType = loginType;
        }

        public Details withLoginType(String loginType) {
            this.loginType = loginType;
            return this;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public Details withCreated(String created) {
            this.created = created;
            return this;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public Details withUpdated(String updated) {
            this.updated = updated;
            return this;
        }

        public String getVehicleRegistartionImage() {
            return vehicleRegistartionImage;
        }

        public void setVehicleRegistartionImage(String vehicleRegistartionImage) {
            this.vehicleRegistartionImage = vehicleRegistartionImage;
        }

        public Details withVehicleRegistartionImage(String vehicleRegistartionImage) {
            this.vehicleRegistartionImage = vehicleRegistartionImage;
            return this;
        }

        public String getVehicleRegistartionNumber() {
            return vehicleRegistartionNumber;
        }

        public void setVehicleRegistartionNumber(String vehicleRegistartionNumber) {
            this.vehicleRegistartionNumber = vehicleRegistartionNumber;
        }

        public Details withVehicleRegistartionNumber(String vehicleRegistartionNumber) {
            this.vehicleRegistartionNumber = vehicleRegistartionNumber;
            return this;
        }

        public String getVehicleRegistartionDate() {
            return vehicleRegistartionDate;
        }

        public void setVehicleRegistartionDate(String vehicleRegistartionDate) {
            this.vehicleRegistartionDate = vehicleRegistartionDate;
        }

        public Details withVehicleRegistartionDate(String vehicleRegistartionDate) {
            this.vehicleRegistartionDate = vehicleRegistartionDate;
            return this;
        }

        public String getVehicleRegistartionValidDate() {
            return vehicleRegistartionValidDate;
        }

        public void setVehicleRegistartionValidDate(String vehicleRegistartionValidDate) {
            this.vehicleRegistartionValidDate = vehicleRegistartionValidDate;
        }

        public Details withVehicleRegistartionValidDate(String vehicleRegistartionValidDate) {
            this.vehicleRegistartionValidDate = vehicleRegistartionValidDate;
            return this;
        }

    }


}
