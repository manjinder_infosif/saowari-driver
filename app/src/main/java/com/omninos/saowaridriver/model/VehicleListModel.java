package com.omninos.saowaridriver.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleListModel {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;
    @SerializedName("brandDetails")
    @Expose
    private List<BrandDetail> brandDetails = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public List<BrandDetail> getBrandDetails() {
        return brandDetails;
    }

    public void setBrandDetails(List<BrandDetail> brandDetails) {
        this.brandDetails = brandDetails;
    }


    public class BrandDetail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("moreDetails")
        @Expose
        private List<MoreDetail> moreDetails = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<MoreDetail> getMoreDetails() {
            return moreDetails;
        }

        public void setMoreDetails(List<MoreDetail> moreDetails) {
            this.moreDetails = moreDetails;
        }

    }

    public class Detail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("basePrice")
        @Expose
        private String basePrice;
        @SerializedName("baseDistance")
        @Expose
        private String baseDistance;
        @SerializedName("unitTimePricing")
        @Expose
        private String unitTimePricing;
        @SerializedName("unitDistancePrice")
        @Expose
        private String unitDistancePrice;
        @SerializedName("seatCapacity")
        @Expose
        private String seatCapacity;
        @SerializedName("pricingLogic")
        @Expose
        private String pricingLogic;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("serviceImage1")
        @Expose
        private String serviceImage1;
        @SerializedName("serviceImage2")
        @Expose
        private String serviceImage2;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBasePrice() {
            return basePrice;
        }

        public void setBasePrice(String basePrice) {
            this.basePrice = basePrice;
        }

        public String getBaseDistance() {
            return baseDistance;
        }

        public void setBaseDistance(String baseDistance) {
            this.baseDistance = baseDistance;
        }

        public String getUnitTimePricing() {
            return unitTimePricing;
        }

        public void setUnitTimePricing(String unitTimePricing) {
            this.unitTimePricing = unitTimePricing;
        }

        public String getUnitDistancePrice() {
            return unitDistancePrice;
        }

        public void setUnitDistancePrice(String unitDistancePrice) {
            this.unitDistancePrice = unitDistancePrice;
        }

        public String getSeatCapacity() {
            return seatCapacity;
        }

        public void setSeatCapacity(String seatCapacity) {
            this.seatCapacity = seatCapacity;
        }

        public String getPricingLogic() {
            return pricingLogic;
        }

        public void setPricingLogic(String pricingLogic) {
            this.pricingLogic = pricingLogic;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getServiceImage1() {
            return serviceImage1;
        }

        public void setServiceImage1(String serviceImage1) {
            this.serviceImage1 = serviceImage1;
        }

        public String getServiceImage2() {
            return serviceImage2;
        }

        public void setServiceImage2(String serviceImage2) {
            this.serviceImage2 = serviceImage2;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

    }

    public class MoreDetail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("make_id")
        @Expose
        private String makeId;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("title")
        @Expose
        private String title;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMakeId() {
            return makeId;
        }

        public void setMakeId(String makeId) {
            this.makeId = makeId;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

    }

}
