package com.omninos.saowaridriver.model;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.omninos.saowaridriver.retrofit.Api;
import com.omninos.saowaridriver.retrofit.ApiClient;
import com.omninos.saowaridriver.util.CommonUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverEarningViewModel extends ViewModel {
    private MutableLiveData<EarningModel> getEarnig;

    public LiveData<EarningModel> earningModelLiveData(Activity activity, String driverId) {
        getEarnig = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {

            CommonUtils.showProgress(activity, "");
            Api api = ApiClient.getApiClient().create(Api.class);
            api.earning(driverId).enqueue(new Callback<EarningModel>() {
                @Override
                public void onResponse(Call<EarningModel> call, Response<EarningModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        getEarnig.setValue(response.body());
                    } else {
                        EarningModel earningModel = new EarningModel();
                        earningModel.setSuccess("0");
                        earningModel.setMessage("Server Error");
                        getEarnig.setValue(earningModel);
                    }
                }

                @Override
                public void onFailure(Call<EarningModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    EarningModel earningModel = new EarningModel();
                    earningModel.setSuccess("0");
                    earningModel.setMessage("Server Error");
                    getEarnig.setValue(earningModel);
                }
            });
        } else {

            EarningModel earningModel = new EarningModel();
            earningModel.setSuccess("0");
            earningModel.setMessage("Please Check Internet Connection");
            getEarnig.setValue(earningModel);
        }
        return getEarnig;
    }


    private MutableLiveData<StatementModelClass> getStatements;

    public LiveData<StatementModelClass> statementModelClassLiveData(Activity activity, String summaryType, String driverId) {
        getStatements = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, "");
            Api api = ApiClient.getApiClient().create(Api.class);
            api.getStatements(summaryType, driverId).enqueue(new Callback<StatementModelClass>() {
                @Override
                public void onResponse(Call<StatementModelClass> call, Response<StatementModelClass> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        getStatements.setValue(response.body());
                    } else {
                        StatementModelClass statementModelClass = new StatementModelClass();
                        statementModelClass.setSuccess("0");
                        statementModelClass.setMessage("Server Error");
                        getStatements.setValue(statementModelClass);
                    }
                }

                @Override
                public void onFailure(Call<StatementModelClass> call, Throwable t) {

                    StatementModelClass statementModelClass = new StatementModelClass();
                    statementModelClass.setSuccess("0");
                    statementModelClass.setMessage("Server Error");
                    getStatements.setValue(statementModelClass);
                }
            });

        } else {

            StatementModelClass statementModelClass = new StatementModelClass();
            statementModelClass.setSuccess("0");
            statementModelClass.setMessage("Please Check Internet Connection");
            getStatements.setValue(statementModelClass);
        }

        return getStatements;
    }
}
