package com.omninos.saowaridriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Beant Singh on 26/03/19.
 */

public class VehicleTypePojo {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public class Detail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("basePrice")
        @Expose
        private String basePrice;
        @SerializedName("baseDistance")
        @Expose
        private String baseDistance;
        @SerializedName("unitTimePricing")
        @Expose
        private String unitTimePricing;
        @SerializedName("unitDistancePrice")
        @Expose
        private String unitDistancePrice;
        @SerializedName("seatCapacity")
        @Expose
        private String seatCapacity;
        @SerializedName("pricingLogic")
        @Expose
        private String pricingLogic;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("serviceImage1")
        @Expose
        private String serviceImage1;
        @SerializedName("serviceImage2")
        @Expose
        private String serviceImage2;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBasePrice() {
            return basePrice;
        }

        public void setBasePrice(String basePrice) {
            this.basePrice = basePrice;
        }

        public String getBaseDistance() {
            return baseDistance;
        }

        public void setBaseDistance(String baseDistance) {
            this.baseDistance = baseDistance;
        }

        public String getUnitTimePricing() {
            return unitTimePricing;
        }

        public void setUnitTimePricing(String unitTimePricing) {
            this.unitTimePricing = unitTimePricing;
        }

        public String getUnitDistancePrice() {
            return unitDistancePrice;
        }

        public void setUnitDistancePrice(String unitDistancePrice) {
            this.unitDistancePrice = unitDistancePrice;
        }

        public String getSeatCapacity() {
            return seatCapacity;
        }

        public void setSeatCapacity(String seatCapacity) {
            this.seatCapacity = seatCapacity;
        }

        public String getPricingLogic() {
            return pricingLogic;
        }

        public void setPricingLogic(String pricingLogic) {
            this.pricingLogic = pricingLogic;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getServiceImage1() {
            return serviceImage1;
        }

        public void setServiceImage1(String serviceImage1) {
            this.serviceImage1 = serviceImage1;
        }

        public String getServiceImage2() {
            return serviceImage2;
        }

        public void setServiceImage2(String serviceImage2) {
            this.serviceImage2 = serviceImage2;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

    }

}
