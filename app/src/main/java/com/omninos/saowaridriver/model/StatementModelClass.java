package com.omninos.saowaridriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StatementModelClass {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public class PaymentDetail {

        @SerializedName("payment")
        @Expose
        private String payment;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("jobTime")
        @Expose
        private String jobTime;

        public String getPayment() {
            return payment;
        }

        public void setPayment(String payment) {
            this.payment = payment;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getJobTime() {
            return jobTime;
        }

        public void setJobTime(String jobTime) {
            this.jobTime = jobTime;
        }

    }


    public class Detail {

        @SerializedName("jobCreatedDate")
        @Expose
        private String jobCreatedDate;
        @SerializedName("paymentDetails")
        @Expose
        private List<PaymentDetail> paymentDetails = null;

        public String getJobCreatedDate() {
            return jobCreatedDate;
        }

        public void setJobCreatedDate(String jobCreatedDate) {
            this.jobCreatedDate = jobCreatedDate;
        }

        public List<PaymentDetail> getPaymentDetails() {
            return paymentDetails;
        }

        public void setPaymentDetails(List<PaymentDetail> paymentDetails) {
            this.paymentDetails = paymentDetails;
        }

    }

}
