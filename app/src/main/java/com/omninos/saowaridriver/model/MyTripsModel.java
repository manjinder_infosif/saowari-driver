package com.omninos.saowaridriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyTripsModel {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public class Detail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("jobStatus")
        @Expose
        private String jobStatus;
        @SerializedName("rideOtp")
        @Expose
        private String rideOtp;
        @SerializedName("jobId")
        @Expose
        private String jobId;
        @SerializedName("vehicleId")
        @Expose
        private String vehicleId;
        @SerializedName("driverId")
        @Expose
        private String driverId;
        @SerializedName("userId")
        @Expose
        private String userId;
        @SerializedName("picAddress")
        @Expose
        private String picAddress;
        @SerializedName("picLat")
        @Expose
        private String picLat;
        @SerializedName("picLong")
        @Expose
        private String picLong;
        @SerializedName("dropAddress")
        @Expose
        private String dropAddress;
        @SerializedName("dropLat")
        @Expose
        private String dropLat;
        @SerializedName("dropLong")
        @Expose
        private String dropLong;
        @SerializedName("payment")
        @Expose
        private String payment;
        @SerializedName("paymentType")
        @Expose
        private String paymentType;
        @SerializedName("distance")
        @Expose
        private String distance;
        @SerializedName("rejectDriverId")
        @Expose
        private String rejectDriverId;
        @SerializedName("jobCreatedDate")
        @Expose
        private String jobCreatedDate;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("userName")
        @Expose
        private String userName;
        @SerializedName("userImage")
        @Expose
        private String userImage;
        @SerializedName("userPhone")
        @Expose
        private String userPhone;
        @SerializedName("userRating")
        @Expose
        private Float userRating;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getJobStatus() {
            return jobStatus;
        }

        public void setJobStatus(String jobStatus) {
            this.jobStatus = jobStatus;
        }

        public String getRideOtp() {
            return rideOtp;
        }

        public void setRideOtp(String rideOtp) {
            this.rideOtp = rideOtp;
        }

        public String getJobId() {
            return jobId;
        }

        public void setJobId(String jobId) {
            this.jobId = jobId;
        }

        public String getVehicleId() {
            return vehicleId;
        }

        public void setVehicleId(String vehicleId) {
            this.vehicleId = vehicleId;
        }

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getPicAddress() {
            return picAddress;
        }

        public void setPicAddress(String picAddress) {
            this.picAddress = picAddress;
        }

        public String getPicLat() {
            return picLat;
        }

        public void setPicLat(String picLat) {
            this.picLat = picLat;
        }

        public String getPicLong() {
            return picLong;
        }

        public void setPicLong(String picLong) {
            this.picLong = picLong;
        }

        public String getDropAddress() {
            return dropAddress;
        }

        public void setDropAddress(String dropAddress) {
            this.dropAddress = dropAddress;
        }

        public String getDropLat() {
            return dropLat;
        }

        public void setDropLat(String dropLat) {
            this.dropLat = dropLat;
        }

        public String getDropLong() {
            return dropLong;
        }

        public void setDropLong(String dropLong) {
            this.dropLong = dropLong;
        }

        public String getPayment() {
            return payment;
        }

        public void setPayment(String payment) {
            this.payment = payment;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getRejectDriverId() {
            return rejectDriverId;
        }

        public void setRejectDriverId(String rejectDriverId) {
            this.rejectDriverId = rejectDriverId;
        }

        public String getJobCreatedDate() {
            return jobCreatedDate;
        }

        public void setJobCreatedDate(String jobCreatedDate) {
            this.jobCreatedDate = jobCreatedDate;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserImage() {
            return userImage;
        }

        public void setUserImage(String userImage) {
            this.userImage = userImage;
        }

        public String getUserPhone() {
            return userPhone;
        }

        public void setUserPhone(String userPhone) {
            this.userPhone = userPhone;
        }

        public float getUserRating() {
            return userRating;
        }

        public void setUserRating(Float userRating) {
            this.userRating = userRating;
        }

    }

}
