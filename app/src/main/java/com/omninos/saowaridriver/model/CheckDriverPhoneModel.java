package com.omninos.saowaridriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckDriverPhoneModel {


    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private Details details;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public CheckDriverPhoneModel withSuccess(String success) {
        this.success = success;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CheckDriverPhoneModel withMessage(String message) {
        this.message = message;
        return this;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public CheckDriverPhoneModel withDetails(Details details) {
        this.details = details;
        return this;
    }

    public class Details {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("walletBalance")
        @Expose
        private String walletBalance;
        @SerializedName("onlineStatus")
        @Expose
        private String onlineStatus;
        @SerializedName("social_id")
        @Expose
        private String socialId;
        @SerializedName("appointmentStatus")
        @Expose
        private String appointmentStatus;
        @SerializedName("appointmentDate")
        @Expose
        private String appointmentDate;
        @SerializedName("appointmentTime")
        @Expose
        private String appointmentTime;
        @SerializedName("appointmentAddress")
        @Expose
        private String appointmentAddress;
        @SerializedName("otp")
        @Expose
        private String otp;
        @SerializedName("phoneVerifyStatus")
        @Expose
        private String phoneVerifyStatus;
        @SerializedName("driverName")
        @Expose
        private String driverName;
        @SerializedName("driverEmail")
        @Expose
        private String driverEmail;
        @SerializedName("driverPhoneNumber")
        @Expose
        private String driverPhoneNumber;
        @SerializedName("driverAddress")
        @Expose
        private String driverAddress;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("driverPassword")
        @Expose
        private String driverPassword;
        @SerializedName("driverImage")
        @Expose
        private String driverImage;
        @SerializedName("driverLicenseNumber")
        @Expose
        private String driverLicenseNumber;
        @SerializedName("driverVehicleType")
        @Expose
        private String driverVehicleType;
        @SerializedName("driverLicenceStartDate")
        @Expose
        private String driverLicenceStartDate;
        @SerializedName("driverLicenseExpiry")
        @Expose
        private String driverLicenseExpiry;
        @SerializedName("driverLicenseImage")
        @Expose
        private String driverLicenseImage;
        @SerializedName("driverJob")
        @Expose
        private String driverJob;
        @SerializedName("vehicleImage")
        @Expose
        private String vehicleImage;
        @SerializedName("vehicleBrand")
        @Expose
        private String vehicleBrand;
        @SerializedName("vehicleModel")
        @Expose
        private String vehicleModel;
        @SerializedName("vehicalYear")
        @Expose
        private String vehicalYear;
        @SerializedName("vehicleColor")
        @Expose
        private String vehicleColor;
        @SerializedName("vehicleInsurenceImage")
        @Expose
        private String vehicleInsurenceImage;
        @SerializedName("vehicleInsurenceNumber")
        @Expose
        private String vehicleInsurenceNumber;
        @SerializedName("vehicleInsurenceStartDate")
        @Expose
        private String vehicleInsurenceStartDate;
        @SerializedName("vehicleInsurenceExpiery")
        @Expose
        private String vehicleInsurenceExpiery;
        @SerializedName("vehicleCertificateImage")
        @Expose
        private String vehicleCertificateImage;
        @SerializedName("vehiclePermitImage")
        @Expose
        private String vehiclePermitImage;
        @SerializedName("vehiclePermitNumber")
        @Expose
        private String vehiclePermitNumber;
        @SerializedName("vehiclePermitStartDate")
        @Expose
        private String vehiclePermitStartDate;
        @SerializedName("vehiclePermitExpiryDate")
        @Expose
        private String vehiclePermitExpiryDate;
        @SerializedName("region")
        @Expose
        private String region;
        @SerializedName("about")
        @Expose
        private String about;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("category")
        @Expose
        private String category;
        @SerializedName("digit")
        @Expose
        private String digit;
        @SerializedName("reg_id")
        @Expose
        private String regId;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("referralCode")
        @Expose
        private String referralCode;
        @SerializedName("previousLongitude")
        @Expose
        private String previousLongitude;
        @SerializedName("previousLatitude")
        @Expose
        private String previousLatitude;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("login_type")
        @Expose
        private String loginType;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("vehicleRegistartionImage")
        @Expose
        private String vehicleRegistartionImage;
        @SerializedName("vehicleRegistartionNumber")
        @Expose
        private String vehicleRegistartionNumber;
        @SerializedName("vehicleRegistartionDate")
        @Expose
        private String vehicleRegistartionDate;
        @SerializedName("vehicleRegistartionValidDate")
        @Expose
        private String vehicleRegistartionValidDate;
        @SerializedName("vehiclePlateNumber")
        @Expose
        private String vehiclePlateNumber;
        @SerializedName("driverRating")
        @Expose
        private Double driverRating;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getWalletBalance() {
            return walletBalance;
        }

        public void setWalletBalance(String walletBalance) {
            this.walletBalance = walletBalance;
        }

        public String getOnlineStatus() {
            return onlineStatus;
        }

        public void setOnlineStatus(String onlineStatus) {
            this.onlineStatus = onlineStatus;
        }

        public String getSocialId() {
            return socialId;
        }

        public void setSocialId(String socialId) {
            this.socialId = socialId;
        }

        public String getAppointmentStatus() {
            return appointmentStatus;
        }

        public void setAppointmentStatus(String appointmentStatus) {
            this.appointmentStatus = appointmentStatus;
        }

        public String getAppointmentDate() {
            return appointmentDate;
        }

        public void setAppointmentDate(String appointmentDate) {
            this.appointmentDate = appointmentDate;
        }

        public String getAppointmentTime() {
            return appointmentTime;
        }

        public void setAppointmentTime(String appointmentTime) {
            this.appointmentTime = appointmentTime;
        }

        public String getAppointmentAddress() {
            return appointmentAddress;
        }

        public void setAppointmentAddress(String appointmentAddress) {
            this.appointmentAddress = appointmentAddress;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getPhoneVerifyStatus() {
            return phoneVerifyStatus;
        }

        public void setPhoneVerifyStatus(String phoneVerifyStatus) {
            this.phoneVerifyStatus = phoneVerifyStatus;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }

        public String getDriverEmail() {
            return driverEmail;
        }

        public void setDriverEmail(String driverEmail) {
            this.driverEmail = driverEmail;
        }

        public String getDriverPhoneNumber() {
            return driverPhoneNumber;
        }

        public void setDriverPhoneNumber(String driverPhoneNumber) {
            this.driverPhoneNumber = driverPhoneNumber;
        }

        public String getDriverAddress() {
            return driverAddress;
        }

        public void setDriverAddress(String driverAddress) {
            this.driverAddress = driverAddress;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDriverPassword() {
            return driverPassword;
        }

        public void setDriverPassword(String driverPassword) {
            this.driverPassword = driverPassword;
        }

        public String getDriverImage() {
            return driverImage;
        }

        public void setDriverImage(String driverImage) {
            this.driverImage = driverImage;
        }

        public String getDriverLicenseNumber() {
            return driverLicenseNumber;
        }

        public void setDriverLicenseNumber(String driverLicenseNumber) {
            this.driverLicenseNumber = driverLicenseNumber;
        }

        public String getDriverVehicleType() {
            return driverVehicleType;
        }

        public void setDriverVehicleType(String driverVehicleType) {
            this.driverVehicleType = driverVehicleType;
        }

        public String getDriverLicenceStartDate() {
            return driverLicenceStartDate;
        }

        public void setDriverLicenceStartDate(String driverLicenceStartDate) {
            this.driverLicenceStartDate = driverLicenceStartDate;
        }

        public String getDriverLicenseExpiry() {
            return driverLicenseExpiry;
        }

        public void setDriverLicenseExpiry(String driverLicenseExpiry) {
            this.driverLicenseExpiry = driverLicenseExpiry;
        }

        public String getDriverLicenseImage() {
            return driverLicenseImage;
        }

        public void setDriverLicenseImage(String driverLicenseImage) {
            this.driverLicenseImage = driverLicenseImage;
        }

        public String getDriverJob() {
            return driverJob;
        }

        public void setDriverJob(String driverJob) {
            this.driverJob = driverJob;
        }

        public String getVehicleImage() {
            return vehicleImage;
        }

        public void setVehicleImage(String vehicleImage) {
            this.vehicleImage = vehicleImage;
        }

        public String getVehicleBrand() {
            return vehicleBrand;
        }

        public void setVehicleBrand(String vehicleBrand) {
            this.vehicleBrand = vehicleBrand;
        }

        public String getVehicleModel() {
            return vehicleModel;
        }

        public void setVehicleModel(String vehicleModel) {
            this.vehicleModel = vehicleModel;
        }

        public String getVehicalYear() {
            return vehicalYear;
        }

        public void setVehicalYear(String vehicalYear) {
            this.vehicalYear = vehicalYear;
        }

        public String getVehicleColor() {
            return vehicleColor;
        }

        public void setVehicleColor(String vehicleColor) {
            this.vehicleColor = vehicleColor;
        }

        public String getVehicleInsurenceImage() {
            return vehicleInsurenceImage;
        }

        public void setVehicleInsurenceImage(String vehicleInsurenceImage) {
            this.vehicleInsurenceImage = vehicleInsurenceImage;
        }

        public String getVehicleInsurenceNumber() {
            return vehicleInsurenceNumber;
        }

        public void setVehicleInsurenceNumber(String vehicleInsurenceNumber) {
            this.vehicleInsurenceNumber = vehicleInsurenceNumber;
        }

        public String getVehicleInsurenceStartDate() {
            return vehicleInsurenceStartDate;
        }

        public void setVehicleInsurenceStartDate(String vehicleInsurenceStartDate) {
            this.vehicleInsurenceStartDate = vehicleInsurenceStartDate;
        }

        public String getVehicleInsurenceExpiery() {
            return vehicleInsurenceExpiery;
        }

        public void setVehicleInsurenceExpiery(String vehicleInsurenceExpiery) {
            this.vehicleInsurenceExpiery = vehicleInsurenceExpiery;
        }

        public String getVehicleCertificateImage() {
            return vehicleCertificateImage;
        }

        public void setVehicleCertificateImage(String vehicleCertificateImage) {
            this.vehicleCertificateImage = vehicleCertificateImage;
        }

        public String getVehiclePermitImage() {
            return vehiclePermitImage;
        }

        public void setVehiclePermitImage(String vehiclePermitImage) {
            this.vehiclePermitImage = vehiclePermitImage;
        }

        public String getVehiclePermitNumber() {
            return vehiclePermitNumber;
        }

        public void setVehiclePermitNumber(String vehiclePermitNumber) {
            this.vehiclePermitNumber = vehiclePermitNumber;
        }

        public String getVehiclePermitStartDate() {
            return vehiclePermitStartDate;
        }

        public void setVehiclePermitStartDate(String vehiclePermitStartDate) {
            this.vehiclePermitStartDate = vehiclePermitStartDate;
        }

        public String getVehiclePermitExpiryDate() {
            return vehiclePermitExpiryDate;
        }

        public void setVehiclePermitExpiryDate(String vehiclePermitExpiryDate) {
            this.vehiclePermitExpiryDate = vehiclePermitExpiryDate;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getAbout() {
            return about;
        }

        public void setAbout(String about) {
            this.about = about;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getDigit() {
            return digit;
        }

        public void setDigit(String digit) {
            this.digit = digit;
        }

        public String getRegId() {
            return regId;
        }

        public void setRegId(String regId) {
            this.regId = regId;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getReferralCode() {
            return referralCode;
        }

        public void setReferralCode(String referralCode) {
            this.referralCode = referralCode;
        }

        public String getPreviousLongitude() {
            return previousLongitude;
        }

        public void setPreviousLongitude(String previousLongitude) {
            this.previousLongitude = previousLongitude;
        }

        public String getPreviousLatitude() {
            return previousLatitude;
        }

        public void setPreviousLatitude(String previousLatitude) {
            this.previousLatitude = previousLatitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLoginType() {
            return loginType;
        }

        public void setLoginType(String loginType) {
            this.loginType = loginType;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getVehicleRegistartionImage() {
            return vehicleRegistartionImage;
        }

        public void setVehicleRegistartionImage(String vehicleRegistartionImage) {
            this.vehicleRegistartionImage = vehicleRegistartionImage;
        }

        public String getVehicleRegistartionNumber() {
            return vehicleRegistartionNumber;
        }

        public void setVehicleRegistartionNumber(String vehicleRegistartionNumber) {
            this.vehicleRegistartionNumber = vehicleRegistartionNumber;
        }

        public String getVehicleRegistartionDate() {
            return vehicleRegistartionDate;
        }

        public void setVehicleRegistartionDate(String vehicleRegistartionDate) {
            this.vehicleRegistartionDate = vehicleRegistartionDate;
        }

        public String getVehicleRegistartionValidDate() {
            return vehicleRegistartionValidDate;
        }

        public void setVehicleRegistartionValidDate(String vehicleRegistartionValidDate) {
            this.vehicleRegistartionValidDate = vehicleRegistartionValidDate;
        }

        public String getVehiclePlateNumber() {
            return vehiclePlateNumber;
        }

        public void setVehiclePlateNumber(String vehiclePlateNumber) {
            this.vehiclePlateNumber = vehiclePlateNumber;
        }

        public Double getDriverRating() {
            return driverRating;
        }

        public void setDriverRating(Double driverRating) {
            this.driverRating = driverRating;
        }

    }


}
