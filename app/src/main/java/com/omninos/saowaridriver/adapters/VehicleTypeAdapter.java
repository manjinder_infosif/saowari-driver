package com.omninos.saowaridriver.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.VehicleTypePojo;

import java.util.List;

public class VehicleTypeAdapter extends RecyclerView.Adapter<VehicleTypeAdapter.VehicleTypeViewHolder> {

    private Activity activity;
    private List<VehicleTypePojo.Detail> vehDetailList;
    private int currentPosition = 0;
    private int change = 0;

    private VehicleTypeCallback callback;

    public VehicleTypeAdapter(Activity activity, List<VehicleTypePojo.Detail> vehDetailList, VehicleTypeCallback callback) {
        this.activity = activity;
        this.vehDetailList = vehDetailList;
        this.callback = callback;
    }

    @NonNull
    @Override
    public VehicleTypeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity)
                .inflate(R.layout.item_vehicle_type, viewGroup, false);

        return new VehicleTypeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VehicleTypeViewHolder holder, final int i) {

        holder.maxPassengerText.setText(vehDetailList.get(i).getSeatCapacity() + " Max Persons");
        holder.vehDescription.setText(vehDetailList.get(i).getDescription());
        holder.vehTypeText.setText(vehDetailList.get(i).getTitle());

        Glide.with(activity).load(vehDetailList.get(i).getServiceImage1()).into(holder.hatchVehImage);

        holder.maxPassengerHatchCV.setVisibility(View.GONE);

        if (change == 0) {
            holder.maxPassengerText.setText(vehDetailList.get(i).getSeatCapacity() + " Max Persons");
            holder.vehDescription.setText(vehDetailList.get(i).getDescription());
            holder.vehTypeText.setText(vehDetailList.get(i).getTitle());

            Glide.with(activity).load(vehDetailList.get(i).getServiceImage1()).into(holder.hatchVehImage);
            holder.maxPassengerHatchCV.setVisibility(View.GONE);
        } else if (currentPosition == i) {
            holder.maxPassengerText.setText(vehDetailList.get(i).getSeatCapacity() + " Max Persons");
            holder.vehDescription.setText(vehDetailList.get(i).getDescription());
            holder.vehTypeText.setText(vehDetailList.get(i).getTitle());

            Glide.with(activity).load(vehDetailList.get(i).getServiceImage2()).into(holder.hatchVehImage);

            holder.maxPassengerHatchCV.setVisibility(View.VISIBLE);

            callback.selectedVehicleType(vehDetailList.get(i).getId());
        }

        holder.vehTypeOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                change = 1;
                currentPosition = i;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return vehDetailList.size();
    }

    public interface VehicleTypeCallback {

        void selectedVehicleType(String id);

    }

    public class VehicleTypeViewHolder extends RecyclerView.ViewHolder {

        CardView maxPassengerHatchCV, vehTypeOpen;
        TextView maxPassengerText, vehDescription, vehTypeText;
        ImageView hatchVehImage;

        public VehicleTypeViewHolder(@NonNull View itemView) {
            super(itemView);

            maxPassengerHatchCV = itemView.findViewById(R.id.maxPassengerHatchCV);
            maxPassengerText = itemView.findViewById(R.id.maxPassengerText);
            vehDescription = itemView.findViewById(R.id.vehDescription);
            vehTypeText = itemView.findViewById(R.id.vehTypeText);
            vehTypeOpen = itemView.findViewById(R.id.vehTypeOpen);

            hatchVehImage = itemView.findViewById(R.id.hatchVehImage);

        }
    }
}
