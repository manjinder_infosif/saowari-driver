package com.omninos.saowaridriver.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.StatementModelClass;

import java.util.ArrayList;
import java.util.List;

public class MonthlyStatementAdapter extends RecyclerView.Adapter<MonthlyStatementAdapter.MyViewHolder> {

    Context context;
    List<StatementModelClass.Detail> list;
    private List<String> changeList = new ArrayList<>();

    public MonthlyStatementAdapter(Context context, List<StatementModelClass.Detail> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_monthly_statement, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int i) {
        myViewHolder.monthlyDateHeading.setText(list.get(i).getJobCreatedDate());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        myViewHolder.monthHiddenStatementRC.setLayoutManager(linearLayoutManager);

        SubMonthlyStatementAdapter adapter = new SubMonthlyStatementAdapter(context, list.get(i).getPaymentDetails());
        myViewHolder.monthHiddenStatementRC.setAdapter(adapter);

        myViewHolder.monthlyDateHeading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (changeList.contains(String.valueOf(i))) {
                    myViewHolder.monthHiddenStatementRC.setVisibility(View.GONE);
                    changeList.remove(String.valueOf(i));
                } else {
                    myViewHolder.monthHiddenStatementRC.setVisibility(View.VISIBLE);
                    changeList.add(String.valueOf(i));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView monthlyDateHeading;
        private RecyclerView monthHiddenStatementRC;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            monthlyDateHeading = itemView.findViewById(R.id.monthlyDateHeading);
            monthHiddenStatementRC = itemView.findViewById(R.id.monthHiddenStatementRC);
        }
    }
}
