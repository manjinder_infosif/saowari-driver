package com.omninos.saowaridriver.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.MyTripsModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MyBookingAdapter extends RecyclerView.Adapter<MyBookingAdapter.MyViewHolder> {
    private Activity activity;
    private List<MyTripsModel.Detail> details;


    public MyBookingAdapter(Activity activity, List<MyTripsModel.Detail> details) {
        this.activity = activity;
        this.details = details;
    }

    @NonNull
    @Override
    public MyBookingAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_layout_mybooking, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyBookingAdapter.MyViewHolder holder, int position) {
        holder.bookingNo.setText("JobId : " + details.get(position).getJobId());
        holder.pickAddressBooking.setText(details.get(position).getPicAddress());
        holder.dropAddressBooking.setText(details.get(position).getDropAddress());
        holder.nameMyBooking.setText(details.get(position).getUserName());
        holder.numberMyBooking.setText(details.get(position).getUserPhone());

        Glide.with(activity).load(details.get(position).getUserImage()).into(holder.imageMyBooking);

        holder.ratingMyBooking.setRating(details.get(position).getUserRating());

        String date = details.get(position).getCreated();
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MM-yyyy HH:mm:a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.ENGLISH);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.ENGLISH);

        Date dated;
        String str = "";

        try {
            dated = inputFormat.parse(date);
            str = outputFormat.format(dated);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.bookingDate.setText(str);
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView bookingNo, bookingDate, pickAddressBooking, dropAddressBooking, nameMyBooking, numberMyBooking;
        ImageView imageMyBooking;
        RatingBar ratingMyBooking;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            bookingNo = itemView.findViewById(R.id.bookingNo);
            bookingDate = itemView.findViewById(R.id.bookingDate);
            pickAddressBooking = itemView.findViewById(R.id.pickAddressBooking);
            dropAddressBooking = itemView.findViewById(R.id.dropAddressBooking);

            imageMyBooking = itemView.findViewById(R.id.imageMyBooking);

            nameMyBooking = itemView.findViewById(R.id.nameMyBooking);
            numberMyBooking = itemView.findViewById(R.id.numberMyBooking);
            ratingMyBooking = itemView.findViewById(R.id.ratingMyBooking);

        }
    }
}
