package com.omninos.saowaridriver.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omninos.saowaridriver.Activities.ChatActivity;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.MessageInboxModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.MyViewHolder> {
    private Activity activity;
    private List<MessageInboxModel.MessageDetail> list;


    public InboxAdapter(Activity activity, List<MessageInboxModel.MessageDetail> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_layout_inbox, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {
        Glide.with(activity).load(list.get(i).getImage()).into(myViewHolder.userImage);
        myViewHolder.userName.setText(list.get(i).getName());
        myViewHolder.lastMessage.setText(list.get(i).getMessage());
        myViewHolder.textTime.setText(list.get(i).getTime());
        myViewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ChatActivity.class);
                if (list.get(i).getType().equalsIgnoreCase("driver")){
                    intent.putExtra("userId", list.get(i).getReciverId());
                }else if (list.get(i).getType().equalsIgnoreCase("user")){
                    intent.putExtra("userId", list.get(i).getSenderId());
                }

                intent.putExtra("Title", list.get(i).getName());
                intent.putExtra("status", list.get(i).getDriveStatus());
                activity.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout linearLayout;
        private CircleImageView userImage;
        private TextView userName, lastMessage, textTime;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.linear_layout_msg);
            userImage = itemView.findViewById(R.id.userImage);
            userName = itemView.findViewById(R.id.userName);
            lastMessage = itemView.findViewById(R.id.lastMessage);
            textTime = itemView.findViewById(R.id.textTime);

        }
    }
}