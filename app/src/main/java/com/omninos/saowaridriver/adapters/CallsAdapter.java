package com.omninos.saowaridriver.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.GetCallListModel;

import org.w3c.dom.Text;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CallsAdapter extends RecyclerView.Adapter<CallsAdapter.MyViewHolder> {

    private Activity activity;
    List<GetCallListModel.Detail> list;

    public CallsAdapter(Activity activity, List<GetCallListModel.Detail> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_layout_callls, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Glide.with(activity).load(list.get(i).getDriverImage()).into(myViewHolder.image);
        myViewHolder.dateAndTime.setText(list.get(i).getCallDate() + "," + list.get(i).getStartTime());
        myViewHolder.name.setText(list.get(i).getDriverName());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView image;
        private TextView name, dateAndTime;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            dateAndTime = itemView.findViewById(R.id.dateAndTime);
        }
    }
}
