package com.omninos.saowaridriver.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.GetConversionModel;

import java.util.List;

public class PersonalChatAdapter extends RecyclerView.Adapter<PersonalChatAdapter.MyViewHolder> {
    private Activity activity;
    private List<GetConversionModel.MessageDetail> list;

    public PersonalChatAdapter(Activity activity, List<GetConversionModel.MessageDetail> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_layout_personal_chat, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        if (list.get(i).getType().equalsIgnoreCase("Driver")) {
            myViewHolder.recieveMsgRL.setVisibility(View.GONE);
            myViewHolder.sendingMsgRL.setVisibility(View.VISIBLE);
            myViewHolder.tv_input_message.setText(list.get(i).getMessage());
        } else {
            myViewHolder.recieveMsgRL.setVisibility(View.VISIBLE);
            myViewHolder.sendingMsgRL.setVisibility(View.GONE);
            myViewHolder.tv_output_message.setText(list.get(i).getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout recieveMsgRL, sendingMsgRL;
        private TextView tv_output_message, tv_input_message;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            recieveMsgRL = itemView.findViewById(R.id.recieveMsgRL);
            sendingMsgRL = itemView.findViewById(R.id.sendingMsgRL);
            tv_output_message = itemView.findViewById(R.id.tv_output_message);
            tv_input_message = itemView.findViewById(R.id.tv_input_message);

        }
    }
}
