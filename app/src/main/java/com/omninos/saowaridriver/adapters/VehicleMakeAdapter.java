package com.omninos.saowaridriver.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.DummyPojo;

import java.util.List;

public class VehicleMakeAdapter extends BaseAdapter {

    private Activity activity;
    private List<DummyPojo> dummyPojoList;

    public VehicleMakeAdapter(Activity activity, List<DummyPojo> dummyPojoList) {
        this.activity = activity;
        this.dummyPojoList = dummyPojoList;
    }

    @Override
    public int getCount() {
        return dummyPojoList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final MyViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.custom_spinner_items, parent, false);
            viewHolder = new MyViewHolder(convertView);
            convertView.setTag(viewHolder);
            viewHolder.tvMake.setText(dummyPojoList.get(position).getName());
        } else {
            viewHolder = (MyViewHolder) convertView.getTag();

        }
        return convertView;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMake;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMake = itemView.findViewById(R.id.tv_spinner_text);
        }
    }



}
