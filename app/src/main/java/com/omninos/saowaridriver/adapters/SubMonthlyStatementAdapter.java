package com.omninos.saowaridriver.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.model.StatementModelClass;

import java.util.List;

public class SubMonthlyStatementAdapter extends RecyclerView.Adapter<SubMonthlyStatementAdapter.MyViewholder> {

    Context context;
    List<StatementModelClass.PaymentDetail> list;

    public SubMonthlyStatementAdapter(Context context, List<StatementModelClass.PaymentDetail> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_sub_monthly_statement, viewGroup, false);
        return new MyViewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewholder myViewholder, int i) {
        myViewholder.timeMonthlyStatement.setText(list.get(i).getJobTime());
        myViewholder.amountMonthlyStatement.setText("৳" + list.get(i).getPayment());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewholder extends RecyclerView.ViewHolder {
        private TextView timeMonthlyStatement, amountMonthlyStatement;

        public MyViewholder(@NonNull View itemView) {
            super(itemView);
            amountMonthlyStatement = itemView.findViewById(R.id.amountMonthlyStatement);
            timeMonthlyStatement = itemView.findViewById(R.id.timeMonthlyStatement);
        }
    }
}
