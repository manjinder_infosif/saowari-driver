package com.omninos.saowaridriver.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static Retrofit retrofit;

    public static Retrofit getApiClient() {

        if (retrofit == null) {

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .build();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl("http://futurewingsvisa.in/saowariApplication/index.php/api/Users/")
//                    .baseUrl("http://3.130.252.86/newApp/index.php/api/Users/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }


    private static final String BASE_URL_DIRECTION = "https://maps.googleapis.com/";

    public static Retrofit retrofitRoute = null;

    public static Retrofit getClientRoute() {

        if (retrofitRoute == null) {
            retrofitRoute = new Retrofit.Builder()
                    .baseUrl(BASE_URL_DIRECTION)
                    .addConverterFactory(GsonConverterFactory.create())
                    //.client(httpCLient.build())
                    .build();
        }
        return retrofitRoute;
    }

}
