package com.omninos.saowaridriver.retrofit;

import com.omninos.saowaridriver.model.CheckDriverJobPojo;
import com.omninos.saowaridriver.model.CheckDriverPhoneModel;
import com.omninos.saowaridriver.model.VersionModel;
import com.omninos.saowaridriver.model.DirectionPojo;
import com.omninos.saowaridriver.model.EarningModel;
import com.omninos.saowaridriver.model.GetCallListModel;
import com.omninos.saowaridriver.model.GetConversionModel;
import com.omninos.saowaridriver.model.HelpAndContactusModel;
import com.omninos.saowaridriver.model.JobFlowPojo;
import com.omninos.saowaridriver.model.MessageInboxModel;
import com.omninos.saowaridriver.model.MyTripsModel;
import com.omninos.saowaridriver.model.PendingAmountModel;
import com.omninos.saowaridriver.model.RescheduleAppointmentPojo;
import com.omninos.saowaridriver.model.SendMessageModel;
import com.omninos.saowaridriver.model.StatementModelClass;
import com.omninos.saowaridriver.model.VehicleListModel;
import com.omninos.saowaridriver.model.VehicleRegisterModel;
import com.omninos.saowaridriver.model.VehicleTypePojo;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.QueryMap;

public interface Api {

    //checkDriver Phone number
    @FormUrlEncoded
    @POST("checkDriverPhoneNumber")
    Call<CheckDriverPhoneModel> checkphone(@Field("driverPhoneNumber") String driverPhoneNumber,
                                           @Field("latitude") String latitude,
                                           @Field("longitude") String longitude,
                                           @Field("reg_id") String reg_id,
                                           @Field("device_type") String device_type,
                                           @Field("login_type") String login_type);

    //driver register api
    @Multipart
    @POST("driverRegister")
    Call<CheckDriverPhoneModel> driverRegister(@Part("driverName") RequestBody driverName,
                                               @Part("driverPhoneNumber") RequestBody driverPhoneNumber,
                                               @Part("driverEmail") RequestBody driverEmail,
                                               @Part("dateOfBirth") RequestBody dateOfBirth,
                                               @Part("gender") RequestBody gender,
                                               @Part("device_type") RequestBody device_type,
                                               @Part("reg_id") RequestBody reg_id,
                                               @Part("latitude") RequestBody latitude,
                                               @Part("longitude") RequestBody longitude,
                                               @Part("login_type") RequestBody login_type,
                                               @Part MultipartBody.Part driverImage,
                                               @Part("driverJob") RequestBody driverJob);

    //make and model list
    @FormUrlEncoded
    @POST("brandDetails")
    Call<VehicleListModel> getMakeAndModelList(@Field("type") String type);

    //driver vehicle register
    @Multipart
    @POST("driverVehicleRegister")
    Call<VehicleRegisterModel> vehcileRegister(@Part("vehicleBrand") RequestBody vehicleBrand,
                                               @Part("vehicleModel") RequestBody vehicleModel,
                                               @Part("vehicalYear") RequestBody vehicalYear,
                                               @Part("vehicleColor") RequestBody vehicleColor,
                                               @Part("vehiclePlateNumber") RequestBody vehiclePlateNumber,
                                               @Part("driverLicenseNumber") RequestBody driverLicenseNumber,
                                               @Part("driverVehicleType") RequestBody driverVehicleType,
                                               @Part("driverLicenceStartDate") RequestBody driverLicenceStartDate,
                                               @Part("driverLicenseExpiry") RequestBody driverLicenseExpiry,
                                               @Part("vehicleInsurenceNumber") RequestBody vehicleInsurenceNumber,
                                               @Part("vehicleInsurenceStartDate") RequestBody vehicleInsurenceStartDate,
                                               @Part("vehicleInsurenceExpiery") RequestBody vehicleInsurenceExpiery,
                                               @Part("vehiclePermitStartDate") RequestBody vehiclePermitStartDate,
                                               @Part("vehiclePermitExpiryDate") RequestBody vehiclePermitExpiryDate,
                                               @Part("vehiclePermitNumber") RequestBody vehiclePermitNumber,
                                               @Part("vehicleRegistartionNumber") RequestBody vehicleRegistartionNumber,
                                               @Part("vehicleRegistartionDate") RequestBody vehicleRegistartionDate,
                                               @Part("vehicleRegistartionValidDate") RequestBody vehicleRegistartionValidDate,
                                               @Part("driverId") RequestBody driverId,
                                               @Part MultipartBody.Part driverLicenseImage,
                                               @Part MultipartBody.Part vehicleRegistartionImage,
                                               @Part MultipartBody.Part vehicleImage,
                                               @Part MultipartBody.Part vehicleInsurenceImage,
                                               @Part MultipartBody.Part vehiclePermitImage,
                                               @Part("type") RequestBody type);


    @GET("brandDetails")
    Call<VehicleTypePojo> vehicleDetails();

    @FormUrlEncoded
    @POST("driverOnlineOffline")
    Call<Map> onlineOfflineData(@Field("onlineStatus") String onlineStatus,
                                @Field("driverId") String driverId);

    @FormUrlEncoded
    @POST("updateDriverLatLong")
    Call<Map> updateLatLng(@Field("driverId") String driverId,
                           @Field("latitude") String latitude,
                           @Field("longitude") String longitude,
                           @Field("previousLatitude") String previousLatitude,
                           @Field("previousLongitude") String previousLongitude);


    @FormUrlEncoded
    @POST("driverAcceptRejectJob")
    Call<JobFlowPojo> driverAcceptRejectJob(@Field("jobStatus") String jobStatus,
                                            @Field("jobId") String jobId);


    @GET("maps/api/directions/json?")
    Call<DirectionPojo> getPolyLine(@QueryMap Map<String, String> data);


    @FormUrlEncoded
    @POST("checkDriverJob")
    Call<CheckDriverJobPojo> checkDriverJob(@Field("jobId") String jobId);

    @FormUrlEncoded
    @POST("driverHelpContact")
    Call<HelpAndContactusModel> HelpContact(@Field("type") String type,
                                            @Field("name") String name,
                                            @Field("phone") String phone,
                                            @Field("email") String email,
                                            @Field("message") String message,
                                            @Field("driverId") String driverId);

    @FormUrlEncoded
    @POST("driverTripsList")
    Call<MyTripsModel> myTrips(@Field("driverId") String driverId);

    @Multipart
    @POST("updateDriverProfile")
    Call<CheckDriverPhoneModel> updateProfile(@Part("driverName") RequestBody driverName,
                                              @Part("address") RequestBody address,
                                              @Part("driverPhoneNumber") RequestBody driverPhoneNumber,
                                              @Part("driverEmail") RequestBody driverEmail,
                                              @Part("driverId") RequestBody driverId,
                                              @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("driverEarnings")
    Call<EarningModel> earning(@Field("driverId") String driverId);

    @FormUrlEncoded
    @POST("ridesSummary")
    Call<StatementModelClass> getStatements(@Field("summaryType") String summaryType,
                                            @Field("driverId") String driverId);


    @FormUrlEncoded
    @POST("driverRescheduleAppointment")
    Call<RescheduleAppointmentPojo> rescheduleAppointment(@Field("driverId") String driverId);

    @FormUrlEncoded
    @POST("inboxMessage")
    Call<MessageInboxModel> getInbox(@Field("sender_id") String sender_id,
                                     @Field("type") String type);

    @FormUrlEncoded
    @POST("conversationMessage")
    Call<GetConversionModel> getConversion(@Field("sender_id") String sender_id,
                                           @Field("reciver_id") String reciver_id,
                                           @Field("type") String type);

    @FormUrlEncoded
    @POST("sendMessage")
    Call<SendMessageModel> sendMessage(@Field("sender_id") String sender_id,
                                       @Field("reciver_id") String reciver_id,
                                       @Field("message") String message,
                                       @Field("type") String type);

    @FormUrlEncoded
    @POST("driverLogOut")
    Call<Map> logOut(@Field("driverId") String userId);

    @FormUrlEncoded
    @POST("driverRatingUser")
    Call<Map> rating(@Field("jobId") String jobId,
                     @Field("driverId") String driverId,
                     @Field("userId") String userId,
                     @Field("comment") String comment,
                     @Field("rating") String rating);

    @FormUrlEncoded
    @POST("driverCallList")
    Call<GetCallListModel> getCall(@Field("driverId") String driverId);

    @FormUrlEncoded
    @POST("driverPayment")
    Call<PendingAmountModel> pending(@Field("driverId") String driverId,
                                     @Field("status") String status);


    @FormUrlEncoded
    @POST("checkVersion")
    Call<VersionModel> version(@Field("type") String type);

}
