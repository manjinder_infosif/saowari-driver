package com.omninos.saowaridriver.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.omninos.saowaridriver.Activities.AppointmentConfirmationActivity;
import com.omninos.saowaridriver.Activities.HomeActivity;
import com.omninos.saowaridriver.Activities.RideRequestActivity;
import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;


public class MyFirebaseMessagingService extends FirebaseMessagingService {


    public static final String NOTIFICATION_CHANNEL_ID = "10001";

    @SuppressLint("NewApi")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String subtitle = remoteMessage.getData().get("subtitle");

        if (subtitle != null) {

            if (subtitle.equalsIgnoreCase("Request")) {

                String userId = remoteMessage.getData().get("userId");
                String paymentType = remoteMessage.getData().get("paymentType");
                String picUpAddress = remoteMessage.getData().get("picUpAddress");
                String picLat = remoteMessage.getData().get("picLat");
                String picLong = remoteMessage.getData().get("picLong");
                String dropAddress = remoteMessage.getData().get("dropAddress");
                String dropLat = remoteMessage.getData().get("dropLat");
                String dropLong = remoteMessage.getData().get("dropLong");
                String payment = remoteMessage.getData().get("payment");

                String name = remoteMessage.getData().get("name");
                String image = remoteMessage.getData().get("image");
                String phone = remoteMessage.getData().get("phone");
                String rating = remoteMessage.getData().get("ratingOfCustomer");

                String jobId = remoteMessage.getData().get("jobId");

                String distance = remoteMessage.getData().get("distance");

                String title = remoteMessage.getData().get("title");
                String message = remoteMessage.getData().get("message");

                App.getAppPreference().SaveString(ConstantData.JOB_CUSTOMER_ID, userId);
                App.getAppPreference().SaveString(ConstantData.JOB_CUSTOMER_NAME, name);
                App.getAppPreference().SaveString(ConstantData.JOB_CUSTOMER_IMAGE, image);
                App.getAppPreference().SaveString(ConstantData.JOB_CUSTOMER_MOBILE, phone);
                App.getAppPreference().SaveString(ConstantData.JOB_CUSTOMER_RATING, rating);

                App.getAppPreference().SaveString(ConstantData.JOB_STATUS, subtitle);

                App.getAppPreference().SaveString(ConstantData.JOB_ID, jobId);
                App.getAppPreference().SaveString(ConstantData.JOB_TOTAL_DISTANCE, distance);
                App.getAppPreference().SaveString(ConstantData.JOB_TOTAL_AMOUNT, payment);
                App.getAppPreference().SaveString(ConstantData.PAYMENT_TYPE, paymentType);

                App.getAppPreference().SaveString(ConstantData.JOB_PICK_ADDRESS, picUpAddress);
                App.getAppPreference().SaveString(ConstantData.JOB_PICK_LAT, picLat);
                App.getAppPreference().SaveString(ConstantData.JOB_PICK_LONG, picLong);

                App.getAppPreference().SaveString(ConstantData.JOB_DROP_ADDRESS, dropAddress);
                App.getAppPreference().SaveString(ConstantData.JOB_DROP_LAT, dropLat);
                App.getAppPreference().SaveString(ConstantData.JOB_DROP_LONG, dropLong);

                showNotification(title, message, subtitle);

            } else if (subtitle.equalsIgnoreCase("appointmenStatus")) {

                String appointmentDate = remoteMessage.getData().get("appointmentDate");
                String appointmentTime = remoteMessage.getData().get("appointmentTime");
                String appointmentAddress = remoteMessage.getData().get("appointmentAddress");

                String title = remoteMessage.getData().get("title");
                String message = remoteMessage.getData().get("message");

                App.getAppPreference().SaveString(ConstantData.NOTIFICATION_STATUS, "appointmenStatus");
                App.getAppPreference().SaveString(ConstantData.APPOINTMENT_DATE, appointmentDate);
                App.getAppPreference().SaveString(ConstantData.APPOINTMENT_TIME, appointmentTime);
                App.getAppPreference().SaveString(ConstantData.APPOINTMENT_ADDRESS, appointmentAddress);

                showNotification(title, message, subtitle);

            } else if (subtitle.equalsIgnoreCase("userCancel")) {

                String title = remoteMessage.getData().get("title");
                String message = remoteMessage.getData().get("message");
                String reason = remoteMessage.getData().get("reason");

                showNotification(title, message + " " + reason, subtitle);

            }
        }
    }

    /**
     * Create and push the notification
     */

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void showNotification(String title, String message, String subtitle) {
        /*Creates an explicit intent for an Activity in your app**/
        Intent resultIntent = null;
        if (subtitle.equalsIgnoreCase("Request")) {
            resultIntent = new Intent(this, RideRequestActivity.class);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(resultIntent);
        } else if (subtitle.equalsIgnoreCase("appointmenStatus")) {
            App.getAppPreference().SaveString(ConstantData.IS_APPOINTMENT, "1");
            resultIntent = new Intent(this, AppointmentConfirmationActivity.class);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(resultIntent);
        } else if (subtitle.equalsIgnoreCase("userCancel")) {
            resultIntent = new Intent(this, HomeActivity.class);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(resultIntent);
            CommonUtils.clearJobDetails();
        }
//
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.logo);
        mBuilder.setContentTitle(title)
                .setAutoCancel(true)
                .setContentText(message)
                .setSound(soundUri)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
//                .setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(0 /* Req* Request Code */, mBuilder.build());

    }
}