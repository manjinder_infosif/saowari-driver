package com.omninos.saowaridriver.services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.omninos.saowaridriver.retrofit.Api;
import com.omninos.saowaridriver.retrofit.ApiClient;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.TrackerSettings;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dimpal on 06/03/19.
 */

public class LocationService extends Service
        implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, com.google.android.gms.location.LocationListener {

    public static final int LOCATION_INTERVAL = 10000;
    public static final int FASTEST_LOCATION_INTERVAL = 10000;
    public static final String ACTION_LOCATION_BROADCAST = LocationService.class.getName() +
            "LocationBroadcast";
    public static final String EXTRA_LATITUDE = "extra_latitude";
    public static final String EXTRA_LONGITUDE = "extra_longitude";
    private static final String TAG = LocationService.class.getSimpleName();
    LocationRequest mLocationRequest = new LocationRequest();
    private GoogleApiClient mLocationClient;

    private String latt = "0.0", longg = "0.0", preLat = "0.0", preLng = "0.0";


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mLocationClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest.setInterval(LOCATION_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_LOCATION_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationClient.connect();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent broadcastIntent = new Intent("uk.ac.shef.oak.ActivityRecognition.RestartSensor");
        sendBroadcast(broadcastIntent);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            Log.d(TAG, "== Error On onConnected() Permission not granted");
            //Permission not granted by user so cancel the further execution.
            return;
        }

        if (mLocationClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);
            Log.d(TAG, "Connected to Google API");
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Failed to connect to Google API");
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            Log.v("location_service", "location != null");
            Log.v("location_service", location.getLatitude() + "," + location.getLongitude());
            //Toast.makeText(this, location.getLatitude() + ",oye," + location.getLongitude(), Toast.LENGTH_SHORT).show();

//            latt = String.valueOf(location.getLatitude());
//            longg = String.valueOf(location.getLongitude());
//
            if (preLng.equalsIgnoreCase("0.0")) {
                preLng = latt;
                preLat = longg;
            }

            latt = String.valueOf(location.getLatitude());
            longg = String.valueOf(location.getLongitude());

            if (CommonUtils.isNetworkConnected(LocationService.this)) {

                Api api = ApiClient.getApiClient().create(Api.class);
                api.updateLatLng(App.getAppPreference().GetString(ConstantData.USERID), latt, longg, preLat, preLng).enqueue(new Callback<Map>() {
                    @Override
                    public void onResponse(Call<Map> call, Response<Map> response) {

                    }

                    @Override
                    public void onFailure(Call<Map> call, Throwable t) {

                    }
                });
                preLat = latt;
                preLng = longg;

            } else {

            }
            App.getAppPreference().SaveString(ConstantData.CURRENT_LAT, latt);
            App.getAppPreference().SaveString(ConstantData.CURRENT_LONG, longg);
            App.getSinltonPojo().setCurrentLatlng(new LatLng(location.getLatitude(), location.getLongitude()));

            App.getSinltonPojo().setCurrentLatlng(new LatLng(location.getLatitude(), location.getLongitude()));

            sendMessageToUI(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void sendMessageToUI(String lat, String lng) {
        Log.v(TAG, "Sending info...");

        Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
        intent.putExtra(EXTRA_LATITUDE, lat);
        intent.putExtra(EXTRA_LONGITUDE, lng);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


//    LocationTracker tracker;
//    String lastLat = "0.0", LastLng = "0.0";
//    Timer timer;
//
//    public LocationService(Context applicationContext) {
//        super();
//
//    }
//
//    public LocationService() {
//    }
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        super.onStartCommand(intent, flags, startId);
//
//        Log.d("in location Service", "On start command");
//        StartListning();
//        return START_STICKY;
//    }
//
//    private void initTimer() {
////
//        timer = new Timer();
//        timer.scheduleAtFixedRate(new TimerTask() {
//            @Override
//            public void run() {
//                StartListning();
//            }
//        }, 500, 500);
//    }
//
//
//    private void StartListning() {
//
//        Log.d("in location Service", "On  start listning");
//        tracker = new LocationTracker(
//                getApplicationContext(),
//                new TrackerSettings()
//                        .setUseGPS(true)
//                        .setUseNetwork(true)
//                        .setUsePassive(true)
//                        .setMetersBetweenUpdates(100)) {
//
//            @Override
//            public void onLocationFound(Location location) {
//                if (CommonUtils.isNetworkConnected(getApplicationContext())) {
//                    ApiClient.getApiClient();
//                    Api api = ApiClient.retrofit.create(Api.class);
//                    String latitude = location.getLatitude() + "";
//                    String longitude = location.getLongitude() + "";
//                    App.getSinltonPojo().setCurrentLatlng(new LatLng(location.getLatitude(),location.getLongitude()));
////                    App.getSinltonPojo().setLonitude(longitude);
//                    if (App.getAppPreference().getLoginDetail() != null) {
//                        String id = App.getAppPreference().getLoginDetail().getDetails().getId();
//                        api.updateLatLng(id, latitude, longitude, lastLat, LastLng).enqueue(new Callback<Map>() {
//                            @Override
//                            public void onResponse(Call<Map> call, retrofit2.Response<Map> response) {
//                                if (response.body() != null) {
//                                    Toast.makeText(LocationService.this, "Update", Toast.LENGTH_SHORT).show();
//                                } else {
//                                    Toast.makeText(LocationService.this, "Not Update", Toast.LENGTH_SHORT).show();
//                                }
//
//                            }
//
//                            @Override
//                            public void onFailure(Call<Map> call, Throwable t) {
////                                Toast.makeText(LocationService.this, t.toString(), Toast.LENGTH_SHORT).show();
//                            }
//                        });
//                    }
//
//                } else {
//
//                    Toast.makeText(LocationService.this, "Netowrk disconnected", Toast.LENGTH_SHORT).show();
//                }
//
//                lastLat = location.getLatitude() + "";
//                LastLng = location.getLongitude() + "";
////                App.getSinltonPojo().setLastLatitude(lastLat);
////                App.getSinltonPojo().setLastlongitude(LastLng);
//
//                // Do some stuff when a new GPS Location has been found
//            }
//
//            @Override
//            public void onTimeout() {
//
//            }
//        };
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//
//            return;
//        }
//        tracker.startListening();
//        Log.d("in location Service", "listning start");
//    }
//
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//
////        if (tracker.isListening()) {
////
////            tracker.stopListening();
////            Log.d("in location Service", "listning stop");
////        }
//
//    }
//
//
//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
}
