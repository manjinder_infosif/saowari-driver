package com.omninos.saowaridriver.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.view.View;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.kaopiz.kprogresshud.KProgressHUD;

public class CommonUtils {
    private static KProgressHUD progressDialog;

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static void showSnackbarAlert(View view, String message) {
        TSnackbar snackbar = TSnackbar.make(view, message, TSnackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.WHITE);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.BLACK);
        TextView textView = snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public static void showProgress(Activity activity, String message) {
        progressDialog = KProgressHUD.create(activity)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

    }


    public static void dismissProgress() {
        progressDialog.dismiss();
    }

    public static void clearJobDetails() {

        App.getAppPreference().SaveString(ConstantData.JOB_STATUS, "");

        App.getAppPreference().SaveString(ConstantData.JOB_ARRIVING_STATUS, "");
        App.getAppPreference().SaveString(ConstantData.JOB_CUSTOMER_ID, "");
        App.getAppPreference().SaveString(ConstantData.JOB_CUSTOMER_NAME, "");
        App.getAppPreference().SaveString(ConstantData.JOB_CUSTOMER_IMAGE, "");
        App.getAppPreference().SaveString(ConstantData.JOB_CUSTOMER_MOBILE, "");
        App.getAppPreference().SaveString(ConstantData.JOB_CUSTOMER_RATING, "");

        App.getAppPreference().SaveString(ConstantData.JOB_ID, "");
        App.getAppPreference().SaveString(ConstantData.JOB_TOTAL_DISTANCE, "");
        App.getAppPreference().SaveString(ConstantData.JOB_TOTAL_AMOUNT, "");
        App.getAppPreference().SaveString(ConstantData.PAYMENT_TYPE, "");


        App.getAppPreference().SaveString(ConstantData.JOB_PICK_ADDRESS, "");
        App.getAppPreference().SaveString(ConstantData.JOB_PICK_LAT, "");
        App.getAppPreference().SaveString(ConstantData.JOB_PICK_LONG, "");
        App.getAppPreference().SaveString(ConstantData.JOB_DROP_ADDRESS, "");
        App.getAppPreference().SaveString(ConstantData.JOB_DROP_LAT, "");
        App.getAppPreference().SaveString(ConstantData.JOB_DROP_LONG, "");


    }

}
