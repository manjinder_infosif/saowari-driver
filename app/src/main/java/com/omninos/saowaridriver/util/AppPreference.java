package com.omninos.saowaridriver.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.omninos.saowaridriver.model.CheckDriverPhoneModel;

public class AppPreference {

    private static AppPreference appPreference;
    private SharedPreferences sharedPreferences;


    private AppPreference(Context context) {

        sharedPreferences = context.getSharedPreferences("SaowarDriver", Context.MODE_PRIVATE);
    }

    public static AppPreference init(Context context) {
        if (appPreference == null) {
            appPreference = new AppPreference(context);
        }
        return appPreference;
    }

    public void SaveString(String key, String value) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String GetString(String key) {

        return sharedPreferences.getString(key, "");
    }

    public void Logout(Activity activity) {

        sharedPreferences.edit().clear().apply();
    }

    public void saveLoginDetail(CheckDriverPhoneModel userLoginModel) {

        Gson gson = new Gson();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ConstantData.LOG_IN_DATA, gson.toJson(userLoginModel));
        editor.apply();
    }

    //
    public CheckDriverPhoneModel getLoginDetail() {
        Gson gson = new Gson();
        return gson.fromJson(sharedPreferences.getString(ConstantData.LOG_IN_DATA, ""), CheckDriverPhoneModel.class);
    }

}
