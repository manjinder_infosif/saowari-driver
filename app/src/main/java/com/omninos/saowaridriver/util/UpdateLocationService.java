package com.omninos.saowaridriver.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.retrofit.Api;
import com.omninos.saowaridriver.retrofit.ApiClient;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Beant Singh on 26/03/19.
 */

public class UpdateLocationService extends Service {

    private Api apiInterface;

    private int checkFirst = 0;

    private Context context;

    private Timer timer;
    private TimerTask timerTask;

    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;
    private NotificationChannel notificationChannel;
    private String NOTIFICATION_CHANNEL_ID = "17";

    public double Lat = 0.0;
    public double Lan = 0.0;

    public UpdateLocationService(Context applicationContext) {
        super();
        this.context = applicationContext;
        Log.i("HERE", "here I am!");
    }

    public UpdateLocationService() {
    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 5000, 5000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                updateLocationApi(App.getAppPreference().GetString(ConstantData.USERID));
            }
        };
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void updateLocationApi(final String userId) {

        apiInterface = ApiClient.getApiClient().create(Api.class);

        if (checkFirst == 0) {
            App.getAppPreference().SaveString(ConstantData.MOVED_LAT,
                    App.getAppPreference().GetString(ConstantData.CURRENT_LAT));
            App.getAppPreference().SaveString(ConstantData.MOVED_LONG,
                    App.getAppPreference().GetString(ConstantData.CURRENT_LONG));

            Lat = Double.parseDouble(App.getAppPreference().GetString(ConstantData.CURRENT_LAT));
            Lan = Double.parseDouble(App.getAppPreference().GetString(ConstantData.CURRENT_LONG));

            checkFirst = 1;
        }
        if (!App.getAppPreference().GetString(ConstantData.CURRENT_LAT).isEmpty()) {
            if (HasMoved(Double.parseDouble(App.getAppPreference().GetString(ConstantData.CURRENT_LAT)),
                    Double.parseDouble(App.getAppPreference().GetString(ConstantData.CURRENT_LONG)))) {
                App.getAppPreference().SaveString(ConstantData.MOVED_LAT,
                        App.getAppPreference().GetString(ConstantData.CURRENT_LAT));
                App.getAppPreference().SaveString(ConstantData.MOVED_LONG,
                        App.getAppPreference().GetString(ConstantData.CURRENT_LONG));

                String latt = App.getAppPreference().GetString(ConstantData.MOVED_LAT);
                String longg = App.getAppPreference().GetString(ConstantData.MOVED_LONG);

                Log.d("YES", "MOVED");
                Log.d("YES", latt + "  " + longg);

//                if (internetCheck(getApplicationContext())) {
//                    apiInterface.updateLatLng(userId, latt, longg, String.valueOf(Lat), String.valueOf(Lan)).enqueue(new Callback<Map>() {
//                        @Override
//                        public void onResponse(@NonNull Call<Map> call, @NonNull Response<Map> response) {
//                            if (response.body() != null) {
//                                if (response.body().get("success").toString().equalsIgnoreCase("1")) {
//                                    Log.d("Update LatLng", "Success");
//                                } else {
//                                    Log.d("Update LatLng", "Fail");
//                                }
//                            } else {
//                                Log.d("Update LatLng", "Fail With Null Response");
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(@NonNull Call<Map> call, @NonNull Throwable t) {
//                            Log.d("Update LatLng", "Failure");
////                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                } else {
//                    new Handler(Looper.getMainLooper()).post(new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(getApplicationContext(), "Network Issue", Toast.LENGTH_SHORT).show();
//                        }
//                    });
////                Toast.makeText(getApplicationContext(), AppConstants.NETWORK_ISSUE, Toast.LENGTH_SHORT).show();
//                }

            } else {
                Log.d("NOT", "MOVED");
                Log.d("NOT", App.getAppPreference().GetString(ConstantData.MOVED_LAT)
                        + "  " + App.getAppPreference().GetString(ConstantData.MOVED_LONG));
            }

        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void startNotification() {

        mNotifyManager = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(this, null);
        mBuilder.setContentTitle("Online"/* + DateFormat.getDateTimeInstance().format(new Date())*/)
                .setSmallIcon(R.drawable.logo)
                .setPriority(Notification.PRIORITY_LOW)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_HIGH);
            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            mNotifyManager.createNotificationChannel(notificationChannel);
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            startForeground(17, mBuilder.build());
        } else {
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotifyManager.notify(17, mBuilder.build());
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startTimer();
        startNotification();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        Intent broadcastIntent = new Intent(this, LocationBroadcastReceiver.class);
        sendBroadcast(broadcastIntent);
        stoptimertask();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    public boolean HasMoved(double newLat, double newLan) {
        double significantDistance = 20;
        double currentDistance = 0;

        currentDistance = DistanceMilesSEP(Lat, Lan, newLat, newLan);
        currentDistance = currentDistance * _m2km * 1000;

        if (currentDistance < significantDistance) {
            return false;
        } else {
            Lat = newLat;
            Lan = newLan;
            return true;
        }
    }

    //To calculate distance of to gmap points

    private double _radiusEarthMiles = 3959;
    private double _radiusEarthKM = 6371;
    private double _m2km = 1.60934;
    private double _toRad = Math.PI / 180;
    private double _centralAngle;

    public double DistanceMilesSEP(double Lat1,
                                   double Lon1,
                                   double Lat2,
                                   double Lon2) {
        try {
            double _radLat1 = Lat1 * _toRad;
            double _radLat2 = Lat2 * _toRad;
            double _dLat = (_radLat2 - _radLat1);
            double _dLon = (Lon2 - Lon1) * _toRad;

            double _a = (_dLon) * Math.cos((_radLat1 + _radLat2) / 2);

            // central angle, aka arc segment angular distance
            _centralAngle = Math.sqrt(_a * _a + _dLat * _dLat);

            // great-circle (orthodromic) distance on Earth between 2 points
        } catch (Exception e) {
            e.printStackTrace();
        }
        return _radiusEarthMiles * _centralAngle;
    }


    public static boolean internetCheck(Context activity) {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable();
    }

}