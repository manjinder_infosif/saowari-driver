package com.omninos.saowaridriver.util;

public class ConstantData {

    public static final String SHOW_PROGRESS_MESSAGE = "Please Wait...";
    public static final String Fill_DETAILS = "Fill Mandatory Fields";
    public static final String USERID = "User_id";
    public static final String TOKEN = "Token";
    public static final String IMAGEPATH = "imagePath";
    public static final String Name = "name";
    public static final String Type = "Type";
    public static final String CURRENT_LAT = "current_lat";
    public static final String CURRENT_LONG = "current_long";
    public static final String PHONE_NUMBER = "phone_number";

    public static String EnableInternet = "1";
    public static String ONLINE_STATUS = "online_status";

    public static String LOG_IN_DATA = "login_data";


    public static final String MOVED_LAT = "moved_lat";
    public static final String MOVED_LONG = "moved_long";


    //vehicle details
    public static final String VEHICLE_TYPE = "vehicle_type";
    public static final String VEHICLE_IMAGE = "vehicle_image";
    public static final String VEHICLE_BRAND = "vehicle_brand";
    public static final String VEHICLE_MODEL = "vehicle_model";
    public static final String VEHICLE_MODEL_NAME = "vehicle_model_name";
    public static final String VEHICLE_YEAR = "vehicle_year";
    public static final String VEHICLE_COLOR = "vehicle_color";
    public static final String VEHICLE_PLATE = "vehicle_plate";

    public static final String VEHICAL_REGISTRARTION_IMAGE = "registrartionImage";

    public static final String VEHICALE_NEW_REGISTER_IMAGE = "NewImage";

    public static final String DRIVER_LICENSE_IMAGE = "driver_license_image";
    public static final String DRIVER_LICENSE_NUMBER = "driver_license_number";
    public static final String DRIVER_LICENSE_ISSUE_DATE = "driver_license_issue_date";
    public static final String DRIVER_VALID_VEHICLE_TYPE = "driver_valid_vehicle_type";
    public static final String DRIVER_LICENSE_EXPIRY_DATE = "driver_license_expiry_date";
    public static final String DRIVER_LICENSE_DETAILS = "driver_license_details";

    public static final String DRIVER_VEHICLE_INSURANCE_IMAGE = "driver_vehicle_insurance_image";
    public static final String DRIVER_VEHICLE_INSURANCE_NUMBER = "driver_vehicle_insurance_number";
    public static final String DRIVER_VEHICLE_INSURANCE_ISSUE_DATE = "driver_vehicle_insurance_issue_date";
    public static final String DRIVER_VEHICLE_INSURANCE_EXPIRY_DATE = "driver_vehicle_insurance_expiry_date";
    public static final String DRIVER_VEHICLE_INSURANCE_DETAILS = "driver_vehicle_insurance_details";

    public static final String DRIVER_VEHICLE_PERMIT_IMAGE = "driver_vehicle_permit_image";
    public static final String DRIVER_VEHICLE_PERMIT_NUMBER = "driver_vehicle_permit_number";
    public static final String DRIVER_VEHICLE_PERMIT_ISSUE_DATE = "driver_vehicle_permit_issue_date";
    public static final String DRIVER_VEHICLE_PERMIT_EXPIRY_DATE = "driver_vehicle_permit_expiry_date";
    public static final String DRIVER_VEHICLE_PERMIT_DETAILS = "driver_vehicle_permit_details";

    public static final String DRIVER_VEHICLE_REGISTRATION_IMAGE = "driver_vehicle_registration_image";
    public static final String DRIVER_VEHICLE_REGISTRATION_NUMBER = "driver_vehicle_registration_number";
    public static final String DRIVER_VEHICLE_REGISTRATION_DATE = "driver_vehicle_registration_date";
    public static final String DRIVER_VEHICLE_REGISTRATION_VALID_DATE = "driver_vehicle_registration_valid_date";
    public static final String DRIVER_VEHICLE_REGISTRATION_DETAILS = "driver_vehicle_registration_details";


    public static final String JOB_STATUS = "job_status";

    public static final String JOB_ARRIVING_STATUS = "job_arriving_status";
    public static final String IS_APPOINTMENT = "is_appointment";

    public static final String JOB_CUSTOMER_ID = "job_customer_id";
    public static final String JOB_CUSTOMER_NAME = "job_customer_name";
    public static final String JOB_CUSTOMER_IMAGE = "job_customer_image";
    public static final String JOB_CUSTOMER_MOBILE = "job_customer_mobile";
    public static final String JOB_CUSTOMER_RATING = "job_customer_rating";

    public static final String JOB_ID = "job_id";
    public static final String JOB_TOTAL_DISTANCE = "job_total_distance";
    public static final String JOB_TOTAL_AMOUNT = "job_total_amount";
    public static final String PAYMENT_TYPE = "payment_type";

    public static final String JOB_PICK_ADDRESS = "job_pick_address";
    public static final String JOB_PICK_LAT = "job_pick_lat";
    public static final String JOB_PICK_LONG = "job_pick_long";

    public static final String JOB_DROP_ADDRESS = "job_drop_address";
    public static final String JOB_DROP_LAT = "job_drop_lat";
    public static final String JOB_DROP_LONG = "job_drop_long";

    public static final String NOTIFICATION_STATUS = "notification_status";

    public static final String APPOINTMENT_DATE = "appointment_date";
    public static final String APPOINTMENT_TIME = "appointment_time";
    public static final String APPOINTMENT_ADDRESS = "appointment_address";
}
