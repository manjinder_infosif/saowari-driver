package com.omninos.saowaridriver.util;


import com.google.android.gms.maps.model.LatLng;

public class SingltonPojo {

    LatLng CurrentLatlng;
    String Mobilenumber,PickUpPoint,SouceLat,SourceLng,DropPoint,DestiLat,DestiLng;

    public String getDropPoint() {
        return DropPoint;
    }

    public void setDropPoint(String dropPoint) {
        DropPoint = dropPoint;
    }

    public String getDestiLat() {
        return DestiLat;
    }

    public void setDestiLat(String destiLat) {
        DestiLat = destiLat;
    }

    public String getDestiLng() {
        return DestiLng;
    }

    public void setDestiLng(String destiLng) {
        DestiLng = destiLng;
    }

    public String getPickUpPoint() {
        return PickUpPoint;
    }

    public void setPickUpPoint(String pickUpPoint) {
        PickUpPoint = pickUpPoint;
    }

    public String getSouceLat() {
        return SouceLat;
    }

    public void setSouceLat(String souceLat) {
        SouceLat = souceLat;
    }

    public String getSourceLng() {
        return SourceLng;
    }

    public void setSourceLng(String sourceLng) {
        SourceLng = sourceLng;
    }

    public LatLng getCurrentLatlng() {
        return CurrentLatlng;
    }

    public void setCurrentLatlng(LatLng currentLatlng) {
        CurrentLatlng = currentLatlng;
    }

    public String getMobilenumber() {
        return Mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        Mobilenumber = mobilenumber;
    }
}
