package com.omninos.saowaridriver.util;


import android.app.Application;
import android.content.Context;

public class App extends Application {
    private static App instance;

    private Context context;
    private static AppPreference appPreference;
    private static SingltonPojo sinltonPojo;

    public static App getContext() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        instance = this;
        appPreference = AppPreference.init(context);
        sinltonPojo = new SingltonPojo();
    }

    public static App getInstance() {
        return instance;
    }

    public static SingltonPojo getSinltonPojo() {
        return sinltonPojo;
    }


    public static AppPreference getAppPreference() {
        return appPreference;
    }
}

