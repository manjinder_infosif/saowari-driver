package com.omninos.saowaridriver.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.adapters.CallsAdapter;
import com.omninos.saowaridriver.model.GetCallListModel;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.MessegeViewModel;

import retrofit2.CallAdapter;

public class CallsFragment extends Fragment {
    private RecyclerView recyclerView;
    private MessegeViewModel viewModel;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calls, container, false);

        viewModel = ViewModelProviders.of(this).get(MessegeViewModel.class);

        findIds(view);

        getList();
        return view;
    }

    private void getList() {
        viewModel.getCallListModelLiveData(getActivity(), App.getAppPreference().GetString(ConstantData.USERID)).observe(getActivity(), new Observer<GetCallListModel>() {
            @Override
            public void onChanged(@Nullable GetCallListModel getCallListModel) {
                if (getCallListModel.getSuccess().equalsIgnoreCase("1")) {
                    if (getCallListModel.getDetails() != null) {
                        recyclerView.setAdapter(new CallsAdapter(getActivity(),getCallListModel.getDetails()));
//                        adapter = new CallAdapter(getActivity(), getCallListModel.getDetails());
//                        callsRC.setAdapter(adapter);
                    }
                } else {

                }
            }
        });
    }

    private void findIds(View view) {
        recyclerView = view.findViewById(R.id.recyclerview_calls);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    }
}
