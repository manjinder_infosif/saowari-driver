package com.omninos.saowaridriver.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.omninos.saowaridriver.R;
import com.omninos.saowaridriver.adapters.InboxAdapter;
import com.omninos.saowaridriver.model.MessageInboxModel;
import com.omninos.saowaridriver.util.App;
import com.omninos.saowaridriver.util.CommonUtils;
import com.omninos.saowaridriver.util.ConstantData;
import com.omninos.saowaridriver.viewmodels.MessegeViewModel;

public class ChatsFragment extends Fragment {
    private RecyclerView recyclerView;
    private MessegeViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chats, container, false);
        viewModel = ViewModelProviders.of(this).get(MessegeViewModel.class);

        findIds(view);
        return view;
    }

    private void findIds(View view) {
        recyclerView = view.findViewById(R.id.recyclerview_chats);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getList();

    }

    private void getList() {
        viewModel.messageInboxModelLiveData(getActivity(), App.getAppPreference().GetString(ConstantData.USERID)).observe(getActivity(), new Observer<MessageInboxModel>() {
            @Override
            public void onChanged(@Nullable MessageInboxModel messageInboxModel) {
                if (messageInboxModel.getSuccess().equalsIgnoreCase("1")){
                    if (messageInboxModel.getMessageDetails()!=null){
                        recyclerView.setAdapter(new InboxAdapter(getActivity(),messageInboxModel.getMessageDetails()));
                    }
                }else {
                    CommonUtils.showSnackbarAlert(recyclerView,messageInboxModel.getMessage());
                }
            }
        });
    }

}
